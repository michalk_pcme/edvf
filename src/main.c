/**
  ******************************************************************************
  * @file		main.c
  * @author		Michal Kalbarczyk
  * @date		11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/

/* ----------------------- Includes ---------------------------------------*/
#include <measResGuard.h>
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "systemInit.h"

#include "timestamps.h"
#include "timeoutTimers.h"
//#include "acEngine.h"
#include "acDmaAux.h"

#include "spiPeripheral.h"
#include "eepromDriver.h"
#include "regularMeasurements.h"
#include "dacDriver.h"
#include "adcDriver.h"
#include "gainSwitches.h"
#include "measResGuard.h"



/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
/* ----------------------- Public Functions -------------------------------*/

int main(void)
{
	//Initialise MCU
	//Reset of all peripherals, Initializes the Flash interface and the Systick.
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	SystemHardware_Config();

	//Init RTOS independent services.
	initTimestamps();
	initTimeoutTimers();

	//Init RTOS based services.
	spInitSpiInstance();
	//edInitEepromDriver();
	acInitAuxDmaComms();

	mrgInitMeasurementResourceGuard();
	adcdInitAdcDriver();
	ddInitDacDriver();
	gsInitGainSwitches();

	rmInitRegularMeasurementsDriver();

	//Start scheduler.
	osKernelStart();

	//We should never get here as control is now taken by the scheduler
	while (1)
	{
		errorLoop();
	}
}


#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif


/* ----------------------- Private Functions ------------------------------*/

