/**
  ******************************************************************************
  * @file      	systemDefinitions.c
  * @author		Michal Kalbarczyk
  * @date		11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	Definitions of symbols and functions used system wide.
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"

#include "stm32l4xx_hal.h"
#include "stdlib.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
volatile int disIntCtr = 0;


/* ----------------------- Private Prototypes -----------------------------*/
/* ----------------------- Public Functions -------------------------------*/

/**
 * @brief ???
 */
void errorLoop(void)
{
    while(1)
        asm("nop");
}


/**
 * @brief ???
 */
inline void assertPtr(void * ptr)
{
    if(0 == ptr)
        errorLoop();
}


/**
  * @brief    Multiple entrance disable interrupt function.
  * @details  Enables nested enables/disables of interrupts without worrying
  * 		  which entrance should really enable them by counting number of dis/en
  * 		  attempts.
  * @note     does not affect diode array driving
  * @param    None
  * @retval   None
  */
void disableInterrupts(void)
{
#ifdef CEEDLING_TEST
	asm("nop");
#else
	#ifdef INT_MANAGE_BASEPRI_RAISE
	//interrupt priorities 2 and higher gets disabled with this value put as basepri
	//2 << (8 - __NVIC_PRIO_BITS)
	__ASM volatile ("cpsid i");
	__ASM volatile ("MSR basepri, %0" : : "r" (0x20) : "memory");
	__ASM volatile ("isb");
	__ASM volatile ("dsb");
	__ASM volatile ("cpsie i");
	#else
	__ASM volatile ("cpsid i");
	__ASM volatile ("isb");
	__ASM volatile ("dsb");
	#endif

	disIntCtr++;
#endif
}


/**
  * @brief    Multiple entrance enable interrupt function.
  * @details  Enables nested enables/disables of interrupts without worrying
  * 		  which entrance should really enable them by counting number of
  * 		  dis/en attempts.
  * @note     does not affect diode array driving
  * @param    None
  * @retval   None
  */
void enableInterrupts(void)
{
#ifdef CEEDLING_TEST
	asm("nop");
#else
	disIntCtr--;

	if(!(disIntCtr>0)){
	#ifdef INT_MANAGE_BASEPRI_RAISE
		__ASM volatile ("MSR basepri, %0" : : "r" (0x0) : "memory");
	#else
		__ASM volatile ("cpsie i");
	#endif
	}
#endif
}



/**
  * @brief  Interrupt safe free() function implementation.
  * @param  ptr: pointer to heap memory that should be released
  * @retval None
  */
void safeFree(void * ptr)
{
	DIS_INT
	vPortFree(ptr);
	//free(ptr);
	EN_INT;
}


/**
  * @brief  Interrupt safe malloc() function implementation.
  * @param  size: size of the heap memory that should be allocated
  * @retval void* pointer to allocated heap memory
  */
void * safeMalloc(uint32_t size)
{
	void * ptr;

	DIS_INT
	ptr = pvPortMalloc(size);
	//ptr = malloc(size);
	EN_INT;

	return ptr;
}


//DEFINITIONS TO TRIGGER SVC interrupt to put main thread into privileged mode
//-----------------------------------------------------------------------
#if defined ( __CC_ARM   )
 __ASM void __SVC(void)
 {
   SVC 0x01
   BX R14
 }
#elif defined ( __ICCARM__ )
 static __INLINE  void __SVC()                     { __ASM ("svc 0x01");}
#elif defined   (  __GNUC__  )
 inline void __SVC()                      { __asm volatile ("svc 0x01");}
 #elif defined ( __TASKING__ )
 static __INLINE  void __SVC()                     { __ASM ("svc 0x01");}
#endif


 /* ----------------------- Private Functions ------------------------------*/

