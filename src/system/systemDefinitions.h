/**
  ******************************************************************************
  * @file      	systemDefinitions.h
  * @author		Michal Kalbarczyk
  * @date		11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	Definitions of symbols and functions used system wide.
*/


#ifndef _SYSTEM_DEFINITIONS_H_
#define _SYSTEM_DEFINITIONS_H_


/* ----------------------- Includes ---------------------------------------*/
#include "stm32l4xx_hal.h"
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"

#ifdef CEEDLING_TEST
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#endif


/* ----------------------- Public Defines ---------------------------------*/

/**
 * @brief Enables interrupt disable/enable management through raise of a BASEPRI
 * 		  level otherwise all the interrupts get disabled.
 */
//#define INT_MANAGE_BASEPRI_RAISE

//#define SYSTEM_CLOCK_8MHZ
#define SYSTEM_CLOCK_8_192MHZ
//#define SYSTEM_CLOCK_16MHZ
//#define SYSTEM_CLOCK_80MHZ


#ifdef SYSTEM_CLOCK_8MHZ
#define SYS_CLK					8000000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif

#ifdef SYSTEM_CLOCK_8_192MHZ
#define SYS_CLK					8192000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif

#ifdef SYSTEM_CLOCK_16MHZ
#define SYS_CLK					16000000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif

#ifdef SYSTEM_CLOCK_80MHZ
#define SYS_CLK					80000000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif


#define VERSION_NBR_MAJOR                   0
#define VERSION_NBR_MINOR                   0
#define VERSION_NBR_TRIVIAL                 0

#define MAX_DELAY                           portMAX_DELAY

#define ASSERT_PTR(ptr)                     assertPtr(ptr)
#define ERROR_LOOP                          errorLoop()

#define DINT                                disableGlobalInterrupts()
#define EINT                                enableGlobalInterrupts()

#define SAFE_MALLOC(size)                   safeMalloc(size)
#define SAFE_FREE(ptr)                      safeFree(ptr)

#ifdef CEEDLING_TEST
#define STATIC
#else
#define STATIC					static
#endif


//Default system variables values
// ...


//########## RTOS TASKS PRIORITIES (higher number - higher priority) ################
#define SERVICES_REFRESH_PERIOD             100     //[ms]

#define REGULAR_SERVICES_TASK_PRIORITY      2

#define MODBUS_TASK_PRIORITY                REGULAR_SERVICES_TASK_PRIORITY
#define MODBUS_SUB_TASK_PRIORITY            (MODBUS_TASK_PRIORITY + 1)
#define AUX_COMMS_TASK_PRIORITY             REGULAR_SERVICES_TASK_PRIORITY
#define AUX_DMA_COMMS_TASK_PRIORITY         REGULAR_SERVICES_TASK_PRIORITY
#define ADC_DRIVER_TASK_PRIORITY            (REGULAR_SERVICES_TASK_PRIORITY + 2)
#define EEPROM_DRIVER_TASK_PRIORITY			REGULAR_SERVICES_TASK_PRIORITY
#define REGULAR_MEASUREMENTS_TASK_PRIORITY	(REGULAR_SERVICES_TASK_PRIORITY + 1)


//########## RTOS MANAGED ISR PRIORITIES (higher number - lower priority) ###########
//MODBUS
#define UART2_ISR_PRIORITY                  (configMAX_SYSCALL_INTERRUPT_PRIORITY - 1)

//ADC
#define ADC_DRDY_ISR_PRIORITY				(configMAX_SYSCALL_INTERRUPT_PRIORITY - 1)

//aux comms
#define UART1_ISR_PRIORITY                	(configMAX_SYSCALL_INTERRUPT_PRIORITY -1)



//########## NON-RTOS MANAGED ISR PRIORITIES (higher number - lower priority) #######
//timeout timers
#define TIM3_UPDATE_ISR_PRIORITY            (configMAX_SYSCALL_INTERRUPT_PRIORITY + 1)

#define DAC_ISR_PRIORITY            		(configMAX_SYSCALL_INTERRUPT_PRIORITY + 1)


/* ----------------------- Public Type Definitions ------------------------*/
#ifndef CEEDLING_TEST
typedef enum {true = 1, false = 0} bool;
#endif


/* ----------------------- Public Macros ----------------------------------*/
#define DIS_INT disableInterrupts();
#define EN_INT  enableInterrupts();


/* ----------------------- Public Prototypes ------------------------------*/
void errorLoop(void);
void assertPtr(void * ptr);
void disableInterrupts(void);
void enableInterrupts(void);
void safeFree(void*);
void * safeMalloc(uint32_t);

//extern inline void __SVC(void);


#endif /* _SYSTEM_DEFINITIONS_H_ */
