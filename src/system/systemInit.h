/**
  ******************************************************************************
  * @file      	systemInit.h
  * @author    	Michal Kalbarczyk
  * @date      	11-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	This file contains system initialisation functions that should
  * 	be run before initialisation of any modules or services.
*/


#ifndef _SYSTEM_INIT_H_
#define _SYSTEM_INIT_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"


/* ----------------------- Public Defines ---------------------------------*/
#define LD_RED_Pin 				GPIO_PIN_2
#define LD_RED_GPIO_Port 		GPIOC
#define LD_GREEN_Pin 			GPIO_PIN_3
#define LD_GREEN_GPIO_Port 		GPIOC
#define DCIn1_Pin 				GPIO_PIN_1
#define DCIn1_GPIO_Port 		GPIOA
#define ADDR_4_Pin 				GPIO_PIN_5
#define ADDR_4_GPIO_Port 		GPIOA
#define ADDR_3_Pin 				GPIO_PIN_6
#define ADDR_3_GPIO_Port 		GPIOA
#define ADDR_2_Pin 				GPIO_PIN_7
#define ADDR_2_GPIO_Port 		GPIOA
#define ADDR_1_Pin 				GPIO_PIN_4
#define ADDR_1_GPIO_Port 		GPIOC
#define ADDR_EN_Pin 			GPIO_PIN_5
#define ADDR_EN_GPIO_Port 		GPIOC
#define E2PROM_CS_Pin 			GPIO_PIN_11
#define E2PROM_CS_GPIO_Port 	GPIOB
#define DRDY_Pin 				GPIO_PIN_12
#define DRDY_GPIO_Port 			GPIOB
#define DRDY_EXTI_IRQn 			EXTI15_10_IRQn
#define ADC_RESET_Pin 			GPIO_PIN_6
#define ADC_RESET_GPIO_Port 	GPIOC
#define ADC_CS_Pin 				GPIO_PIN_10
#define ADC_CS_GPIO_Port 		GPIOC
#define GAIN_1_Pin 				GPIO_PIN_11
#define GAIN_1_GPIO_Port 		GPIOC
#define GAIN_0_Pin 				GPIO_PIN_12
#define GAIN_0_GPIO_Port 		GPIOC
#define ROD_STATE_Pin 			GPIO_PIN_2
#define ROD_STATE_GPIO_Port 	GPIOD
#define OP_SELECT_Pin 			GPIO_PIN_5
#define OP_SELECT_GPIO_Port 	GPIOB
#define GAIN5L_Pin 				GPIO_PIN_8
#define GAIN5L_GPIO_Port 		GPIOB
#define GAIN5H_Pin 				GPIO_PIN_9
#define GAIN5H_GPIO_Port 		GPIOB


/* ----------------------- Public Type Definitions ------------------------*/
/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void SystemClock_Config(void);
void MX_GPIO_Init(void);
void SystemHardware_Config(void);


#endif  /*_SYSTEM_INIT_H_*/
