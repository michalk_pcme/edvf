//**************************************************************************
//
// Date             
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************
#ifndef _MBPORTUSART2_H_
#define _MBPORTUSART2_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include "mbport.h"
    
    
/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Global variables -------------------------------*/
extern serHrdwHandle_t uart2HrdwHandle;


/* ----------------------- Global Prototypes ------------------------------*/



#ifdef __cplusplus
}
#endif

#endif  /*_MBPORTUSART2_H_*/
