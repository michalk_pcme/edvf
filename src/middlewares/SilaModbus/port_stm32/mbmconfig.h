/* 
 * MODBUS Slave Library: Configuration
 * Copyright (c) 2007 Christian Walter <wolti@sil.at>
 * All rights reserved.
 *
 * $Id: mbmconfig.h,v 1.1 2008-04-06 07:46:23 cwalter Exp $
 */

#ifndef _MBMCONFIG_H
#define _MBMCONFIG_H

#ifdef __cplusplus
PR_BEGIN_EXTERN_C
#endif

/* ----------------------- Defines ------------------------------------------*/

#ifdef __cplusplus
PR_END_EXTERN_C
#endif


#define MBM_ASCII_ENABLED                       ( 1 )
#define MBM_RTU_ENABLED                         ( 1 )
#define MBM_TCP_ENABLED                         ( 0 )
#define MBM_DEFAULT_RESPONSE_TIMEOUT            ( 500 )
#define MBM_SERIAL_RTU_MAX_INSTANCES            ( 1 )
#define MBM_SERIAL_ASCII_MAX_INSTANCES          ( 1 )
#define MBM_TCP_MAX_INSTANCES                   ( 0 )
        
#define MBM_SERIAL_API_VERSION                  ( 2 )
#define MBM_ASCII_WAITAFTERSEND_ENABLED	        ( 1 )
#define MBM_ASCII_BACKOF_TIME_MS                ( 10 ) 

#define MBM_RTU_WAITAFTERSEND_ENABLED	        ( 1 )
#define MBM_SERIAL_APIV2_RTU_DYNAMIC_TIMEOUT_MS( ulBaudRate ) \
	usMPSerialTimeout( ulBaudRate )



#endif
