//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

#ifndef _MB_PORT_H
#define _MB_PORT_H

#include <assert.h>
#include <FreeRTOS.h>

#include "systemDefinitions.h"
#include "../mbslave/include/common/mbtypes.h"

#ifdef __cplusplus
extern          "C"
{
#endif

    
/* ----------------------- Defines ------------------------------------------*/
#define INLINE                              inline
#define STATIC                              static

#define PR_BEGIN_EXTERN_C                   extern "C" {
#define	PR_END_EXTERN_C                     }


#define MBP_ASSERT( x )                     ( ( x ) ? ( void )0 : vMBPAssert( ) )
    
#define MBP_ENTER_CRITICAL_SECTION( )         vMBPEnterCritical( )
#define MBP_EXIT_CRITICAL_SECTION( )          vMBPExitCritical( )
#define MBP_ENTER_CRITICAL_INIT( )            vMBPEnterCriticalInit( )
#define MBP_EXIT_CRITICAL_INIT( )             vMBPExitCriticalInit( )

#ifndef TRUE
#define TRUE                                ( BOOL )1
#endif

#ifndef FALSE
#define FALSE                               ( BOOL )0
#endif

#define MBP_EVENTHDL_INVALID                NULL
#define MBP_TIMERHDL_INVALID                NULL
#define MBP_SERIALHDL_INVALID               NULL
#define MBP_TCPHDL_INVALID                  NULL
#define MBP_TCPHDL_CLIENT_INVALID           NULL


//#define MBP_USE_SOFTWARE_PARITY_BIT

#define MBP_TASK_PRIORITY                   MODBUS_SUB_TASK_PRIORITY  
    

/**
 * @brief These numbers refer to the positions of particular UART
 * 		  instances in xDrvSerialHdls[] matrix!
 */
typedef enum{
	USART_USART2_IDX    = 0,
    MAX_NBR_OF_UARTS    = 1
}usartHrdHandNames_t;


#define DRV_SERIAL_MAX_INSTANCES			( MAX_NBR_OF_UARTS )
#define MBP_FORCE_SERV2PROTOTYPES           ( 1 )


    
/* ----------------------- Function prototypes ------------------------------*/
/* ----------------------- Type definitions ---------------------------------*/
typedef void *xMBPEventHandle;
typedef void *xMBPTimerHandle;
typedef void *xMBPSerialHandle;
typedef void *xMBPTCPHandle;
typedef void *xMBPTCPClientHandle;

typedef char BOOL;

typedef char BYTE;
typedef unsigned char UBYTE;

typedef unsigned char UCHAR;
typedef char CHAR;

typedef unsigned short USHORT;
typedef short SHORT;

typedef unsigned long ULONG;
typedef long LONG;


typedef struct
{        
    ULONG ulBaudRate;
    UCHAR ucDataBits;
    UCHAR ucStopBits;
    eMBSerialParity eParity;
    
    void  (*initHardware)(ULONG ulBaudRate, UCHAR ucDataBits, UCHAR ucStopBits, eMBSerialParity eParity);
    void  (*closeHardware)(void);
    void  (*initInterrupts)(void);
    
    void  (*rxEnable)(BOOL);
    void  (*rxInterruptEnable)(BOOL);
    void  (*rxInterruptClear)(void);
    BOOL  (*rxHandleErrors)(void);
    
    void  (*txEnable)(BOOL);
    void  (*txInterruptEnable)(BOOL);
    void  (*txInterruptClear)(void);  
    
    UBYTE (*getChar)(BOOL*);
    void  (*putChar)(UBYTE);    
}serHrdwHandle_t;


/* ----------------------- Function prototypes ------------------------------*/
void vMBPInit( void );
void vMBPEnterCritical( void );
void vMBPExitCritical( void );
void vMBPAssert();
USHORT usMPSerialTimeout( ULONG ulBaudRate );


#ifdef __cplusplus
}
#endif

#endif
