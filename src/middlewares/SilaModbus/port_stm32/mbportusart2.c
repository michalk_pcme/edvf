//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

/* ----------------------- System includes --------------------------------*/
#include "stm32l4xx_hal.h"
#include "mbport.h"
#include <FreeRTOS.h>


/* ----------------------- Application includes ---------------------------*/
/* ----------------------- Defines ----------------------------------------*/
#define DEBUG_BUFFER


/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
static UART_HandleTypeDef huart2;

#ifdef DEBUG_BUFFER
	#define DBG_BUFF_SIZE			48

	static uint8_t buffer[DBG_BUFF_SIZE];
	static uint32_t bufItr = 0;
#endif


/* ----------------------- Local  Prototypes ------------------------------*/
static void usart2HardwareInit(ULONG ulBaudRate, UCHAR ucDataBits, UCHAR ucStopBits, eMBSerialParity eParity);
static void usart2HardwareClose(void);
static void usart2InterruptInit(void);
   
static void usart2RXEnable(BOOL);
static void usart2RXInterruptEnable(BOOL);
static void usart2RXInterruptClear(void);
static BOOL usart2RXHandleErrors(void);
    
static void usart2TXEnable(BOOL);
static void usart2TXInterruptEnable(BOOL);
static void usart2TXInterruptClear(void);
    
//static UBYTE usart2GetChar(BOOL);
static UBYTE usart2GetChar(BOOL * dataValid);
static void usart2PutChar(UBYTE);


/* ----------------------- Global Variables -------------------------------*/
serHrdwHandle_t uart2HrdwHandle = 
{    
    0,
    0,
    0,
    0,
    
    &usart2HardwareInit,
    &usart2HardwareClose,
    &usart2InterruptInit,
    
    &usart2RXEnable,
    &usart2RXInterruptEnable,
    &usart2RXInterruptClear,
    &usart2RXHandleErrors,
    
    &usart2TXEnable,
    &usart2TXInterruptEnable,
    &usart2TXInterruptClear,  
    
    &usart2GetChar,
    &usart2PutChar
};


/* ----------------------- Global Functions -------------------------------*/
static void usart2HardwareInit(ULONG ulBaudRate, UCHAR ucDataBits, UCHAR ucStopBits, eMBSerialParity eParity)
{
	uint16_t temp;

	//fill up common settings for all modes of operation
	huart2.Instance 			= USART2;
	huart2.Init.Mode 			= UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl 		= UART_HWCONTROL_NONE;
	huart2.Init.OverSampling 	= UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling 	= UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

	//baud rate
	huart2.Init.BaudRate = ulBaudRate;

	//data bits (word length comprises data bits and a parity bit)
	temp = ucDataBits;
	if(MB_PAR_NONE != eParity)
		temp += 1;	//there is parity bit selected, therefore increase the word length by one to accommodate for it

	switch(temp){
		case 7: huart2.Init.WordLength = UART_WORDLENGTH_7B; break;
		case 8: huart2.Init.WordLength = UART_WORDLENGTH_8B; break;
		case 9: huart2.Init.WordLength = UART_WORDLENGTH_9B; break;
		default: errorLoop(); break;
	}

	//parity
	switch(eParity){
		case MB_PAR_ODD:  huart2.Init.Parity = UART_PARITY_ODD; break;
		case MB_PAR_EVEN: huart2.Init.Parity = UART_PARITY_EVEN; break;
		case MB_PAR_NONE: huart2.Init.Parity = UART_PARITY_NONE; break;
		default: errorLoop(); break;
	}

	//stop bits
	switch(ucStopBits){
		case 1: huart2.Init.StopBits = UART_STOPBITS_1; break;
		case 2: huart2.Init.StopBits = UART_STOPBITS_2; break;
		default: errorLoop(); break;
	}

	//configure UART through HAL (GPIOs are configured through HAL_UART_MspInit() )
    if (HAL_UART_Init(&huart2) != HAL_OK)
    	errorLoop();
    
    //fill up uart settings
    uart2HrdwHandle.ulBaudRate = ulBaudRate;
    uart2HrdwHandle.ucDataBits = ucDataBits;
    uart2HrdwHandle.ucStopBits = ucStopBits;
    uart2HrdwHandle.eParity    = eParity;


    //disable transmitter
    usart2TXEnable(false);

    //disable receiver
    usart2RXEnable(false);
    
    asm("nop");

    //reset receive error flags -> done at HAL_UART_Init()
    //usart2RXHandleErrors();

    //TODO open hardware -> done at HAL_UART_Init()
}



static void usart2HardwareClose(void)
{
	if (HAL_UART_DeInit(&huart2) != HAL_OK)
		errorLoop();
}


/**
 * @brief Configures RX, TX and Fault interrupts to cleared and disabled condition.
 */
static void usart2InterruptInit(void)
{
	//This is done in HAL_UART_MspInit() function through HAL_UART_Init().
}


/**
 * @brief Enables/disables receiver and sets direction pin (master only).
 */
static void usart2RXEnable(BOOL enable)
{
    if(TRUE == enable){
    	//set direction pin to RX (master only)

        //enable receiver
    	SET_BIT(huart2.Instance->CR1, USART_CR1_RE);
    }
    else{
        //disable receiver
    	CLEAR_BIT(huart2.Instance->CR1, USART_CR1_RE);
    }
}


/**
 * @brief Enables/disables receiver interrupts. Clears flags on disable.
 */
static void usart2RXInterruptEnable(BOOL enable)
{
    if(TRUE == enable){
    	//TODO clear flags

        //enable ISR
    	SET_BIT(huart2.Instance->CR1, (USART_CR1_RXNEIE));
    }
    else{
        //disable ISR
    	CLEAR_BIT(huart2.Instance->CR1, (USART_CR1_RXNEIE));
    }
}


/**
 * @brief   Clear the flag after receiver data available interrupt.
 * @warning Do not read the data here!
 */
static void usart2RXInterruptClear(void)
{
	//This should be cleared by reading the data register
	//but optionally can be done through the registers.
	huart2.Instance->RQR = USART_RQR_RXFRQ;
}


/**
 * @brief  Checks if any RX errors have appeared and clears them.
 * @return TRUE - no errors, FALSE - error detected
 */
static BOOL usart2RXHandleErrors(void)
{
	volatile uint32_t flags;
	BOOL status;

	//read the flags
	flags = huart2.Instance->ISR;

	//check the flags
	status = false;
	if(0 != (flags & (USART_ISR_PE | USART_ISR_FE | USART_ISR_ORE | USART_ISR_NE)))
	{
		status = true;

		//clear the flags
		huart2.Instance->ICR = (USART_ICR_PECF | USART_ICR_FECF | USART_ICR_ORECF | USART_ICR_NCF);
	}

	return status;
}


/**
 * @brief Enables/disables transmitter and sets direction pin (master only).
 */
static void usart2TXEnable(BOOL enable)
{
    if(TRUE == enable){
        //set direction pin to transmit (master only)
    	//enable transmitter
    	SET_BIT(huart2.Instance->CR1, USART_CR1_TE);
    }
    else{
        //disable transmitter
    	CLEAR_BIT(huart2.Instance->CR1, USART_CR1_TE);
    }
}


/**
 * @brief Enables/disables transmitter interrupts. Clears flags on disable.
 */
static void usart2TXInterruptEnable(BOOL enable)
{
	if(TRUE == enable){
	    //TODO clear flags

	    //enable ISR
		SET_BIT(huart2.Instance->CR1, (USART_CR1_TXEIE));
	}
	else{
		//disable ISR
		CLEAR_BIT(huart2.Instance->CR1, (USART_CR1_TXEIE | USART_CR1_TCIE));
	}
}


/**
 * @brief Clear the flag after transmitter buffer empty interrupt.
 */
static void usart2TXInterruptClear(void)
{
	huart2.Instance->RQR = USART_RQR_TXFRQ;
}


/**
 * @brief Puts character into transmit buffer.
 */
static void usart2PutChar(UBYTE data)
{
	huart2.Instance->TDR = data;
}


/**
 * @brief  Returns data from a RX buffer regardless if there was anything in it.
 * @param  dataValid: [OUT] true - no errors detected,
 * 							false - errors detected during transmission
 * @return data from the reception buffer
 */
static UBYTE usart2GetChar(BOOL * dataValid)
{
	volatile uint32_t flags;
	UBYTE data;

	//read the flags
	flags = huart2.Instance->ISR;
	data  = huart2.Instance->RDR;

	if(7 == uart2HrdwHandle.ucDataBits)
		data &= 0x7F;

	*dataValid = true;

	//check the flags
	if(0 != (flags & (USART_ISR_PE | USART_ISR_FE | USART_ISR_ORE | USART_ISR_NE)))
		*dataValid = false;

#ifdef DEBUG_BUFFER
	buffer[bufItr++] = data;
	if(DBG_BUFF_SIZE == bufItr)
		bufItr = 0;
#endif

	return data;
}



/* ----------------------- Local  Functions -------------------------------*/
