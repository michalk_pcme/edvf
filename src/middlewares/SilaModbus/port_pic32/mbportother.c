//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes ----------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"


/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"


/* ----------------------- Defines ------------------------------------------*/
#define MBP_LOCK_TIMEOUT                        ( 10000 / portTICK_RATE_MS )


/* ----------------------- Type definitions ---------------------------------*/

/* ----------------------- Static variables ---------------------------------*/
STATIC xSemaphoreHandle xCritSection;

/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Function prototypes ------------------------------*/

/* ----------------------- Start implementation -----------------------------*/

void vMBPInit( void )
{
    xCritSection = xSemaphoreCreateRecursiveMutex(  );
    MBP_ASSERT( NULL != xCritSection );
}


void vMBPEnterCritical( void )
{
    while(TRUE != xSemaphoreTakeRecursive( xCritSection, MBP_LOCK_TIMEOUT ))
    {
        asm("nop");
    }
}


void vMBPExitCritical( void )
{
    signed portBASE_TYPE xResult;

    xResult = xSemaphoreGiveRecursive( xCritSection );
    MBP_ASSERT( pdTRUE == xResult );
}


void vMBPAssert()
{
    __builtin_disable_interrupts();
    //Let the watchdog trigger a reset here.
    for( ;; );
}


