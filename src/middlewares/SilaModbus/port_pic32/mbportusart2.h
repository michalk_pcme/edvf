//**************************************************************************
//
// Date             
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************
#ifndef _MBPORTUSART2_H_
#define _MBPORTUSART2_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include "mbport.h"
    
    
/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void usart2HardwareInit(void);
void usart2HardwareClose(void);
void usart2InterruptInit(void);
   
void usart2RXInterruptEnable(void);
void usart2RXInterruptDisable(void);
void usart2RXInterruptClear(void);  
    
void usart2TXInterruptEnable(void);
void usart2TXInterruptDisable(void);
void usart2TXInterruptClear(void);
    
UBYTE usart2GetChar(void);
void usart2PutChar(UBYTE);


#ifdef __cplusplus
}
#endif

#endif  /*_MBPORTUSART2_H_*/
