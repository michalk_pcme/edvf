/* 
 * MODBUS Library: PIC32 port
 * Copyright (c) 2010 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbporttimer.c,v 1.2 2011-05-01 21:00:01 embedded-solutions.cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>


/* ----------------------- Platform includes --------------------------------*/
#include "xc.h"
#include "mbport.h"


/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"


/* ----------------------- Defines ------------------------------------------*/
#define TIMER_TASK_PRIORITY             ( MBP_TASK_PRIORITY )
#define TIMER_TASK_STACKSIZE            ( 128 )
#define TIMER_TICKRATE_MS               ( 10 / portTICK_RATE_MS )
#define TIMER_DEFAULT_TIMEOUT           ( 1 )
#define MAX_TIMER_HDLS                  ( 4 )
#define IDX_INVALID                     ( 255 )

#define RESET_HDL( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
    ( x )->bIsRunning = FALSE; \
    /*( x )->xNTimeoutTicks = 0;*/ \
    /*( x )->xNExpiryTimeTicks = 0;*/ \
    ( x )->xMBMHdl = MB_HDL_INVALID; \
    ( x )->pbMBPTimerExpiredFN = NULL; \
    ( x )->rtosTimerHandle = NULL; \
} while( 0 );

#define DEBUG_INIT
#define DEBUG_TIMEREXPIRED_ON 
#define DEBUG_TIMEREXPIRED_OFF
#define DEBUG_TIMERSTART_ON
#define DEBUG_TIMERSTART_OFF


/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    BOOL            bIsRunning;
    //portTickType    xNTimeoutTicks;
    //portTickType    xNExpiryTimeTicks;
    xMBHandle       xMBMHdl;
    pbMBPTimerExpiredCB pbMBPTimerExpiredFN;
    TimerHandle_t   rtosTimerHandle;
} xTimerInternalHandle;


/* ----------------------- Static variables ---------------------------------*/
STATIC xTimerInternalHandle arxTimerHdls[MAX_TIMER_HDLS];
STATIC BOOL     bIsInitalized = FALSE;


/* ----------------------- Static functions ---------------------------------*/
void vMBPTimerTask( void *pvParameters );
void eMBPTimerCallback(TimerHandle_t xTimer);


/* ----------------------- Start implementation -----------------------------*/


eMBErrorCode eMBPTimerInit( xMBPTimerHandle * xTimerHdl, USHORT usTimeOut1ms,
               pbMBPTimerExpiredCB pbMBPTimerExpiredFN, xMBHandle xHdl )
{
    eMBErrorCode    eStatus = MB_EPORTERR;
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( ( NULL != xTimerHdl ) && ( NULL != pbMBPTimerExpiredFN ) && ( MB_HDL_INVALID != xHdl ) )
    {
        if( !bIsInitalized )
        {
            DEBUG_INIT;
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                RESET_HDL(&arxTimerHdls[ubIdx]);                
            }
            
            bIsInitalized = TRUE;
        }
        
        if( bIsInitalized )
        {
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                if( IDX_INVALID == arxTimerHdls[ubIdx].ubIdx )
                {
                    break;
                }
            }
            if( MAX_TIMER_HDLS != ubIdx )
            {
                //create a new software timer
                arxTimerHdls[ubIdx].rtosTimerHandle = xTimerCreate(
                    "",                             // Text name for the software timer - not used by FreeRTOS.
                    usTimeOut1ms/portTICK_PERIOD_MS,// The software timer's period in ticks.
                    pdFALSE,                        // Setting uxAutoRealod to false creates an single-shot timer.
                    &arxTimerHdls[ubIdx],           // set pointer to the scale driver as a timer ID
                    &eMBPTimerCallback              // The callback function to be used by the software timer being created. 
                    );
                
                //check if it got created
                if(0 != arxTimerHdls[ubIdx].rtosTimerHandle){
                    arxTimerHdls[ubIdx].ubIdx = ubIdx;
                    arxTimerHdls[ubIdx].bIsRunning = FALSE;
                    //arxTimerHdls[ubIdx].xNTimeoutTicks = MB_INTDIV_CEIL( ( portTickType ) usTimeOut1ms, portTICK_RATE_MS );
                    //arxTimerHdls[ubIdx].xNExpiryTimeTicks = 0;
                    arxTimerHdls[ubIdx].xMBMHdl = xHdl;
                    arxTimerHdls[ubIdx].pbMBPTimerExpiredFN = pbMBPTimerExpiredFN;

                    *xTimerHdl = &arxTimerHdls[ubIdx];
                    
                    eStatus = MB_ENOERR;
                }
                else{
                    //nope, return an error
                    eStatus = MB_ENORES;
                }                
            }
            else
            {
                eStatus = MB_ENORES;
            }
        }
    }
    else
    {
        eStatus = MB_EINVAL;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


void vMBPTimerClose( xMBPTimerHandle xTimerHdl )
{
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        xTimerDelete(pxTimerIntHdl->rtosTimerHandle, MAX_DELAY);
        RESET_HDL( pxTimerIntHdl );
    }
}


eMBErrorCode eMBPTimerSetTimeout( xMBPTimerHandle xTimerHdl, USHORT usTimeOut1ms )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) && ( usTimeOut1ms > 0 ) )
    {
        xTimerChangePeriod(pxTimerIntHdl->rtosTimerHandle, usTimeOut1ms, MAX_DELAY);
        //pxTimerIntHdl->xNTimeoutTicks = MB_INTDIV_CEIL( ( portTickType ) usTimeOut1ms, portTICK_RATE_MS );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


eMBErrorCode eMBPTimerStart( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;

    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    DEBUG_TIMERSTART_ON;
    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = TRUE;
        xTimerStart(pxTimerIntHdl->rtosTimerHandle, MAX_DELAY);
        //pxTimerIntHdl->xNExpiryTimeTicks = xTaskGetTickCount(  );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    DEBUG_TIMERSTART_OFF;
    return eStatus;
}


eMBErrorCode eMBPTimerStop( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = FALSE;
        xTimerStop(pxTimerIntHdl->rtosTimerHandle, MAX_DELAY);
        //pxTimerIntHdl->xNExpiryTimeTicks = 0;
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


/**
 * @brief Callback from FreeRTOS elapsed software timer.
 * @note  Is being called from separate timers thread managed by FreeRTOS.
 * @param xTimer
 */
void eMBPTimerCallback(TimerHandle_t xTimer)
{
    //get the handle to local modbus "timers"
    xTimerInternalHandle *pxTimerIntHdl = pvTimerGetTimerID(xTimer);
    
    //trigger callback related with particular timer to notify modbus stack
    pxTimerIntHdl->pbMBPTimerExpiredFN(pxTimerIntHdl->xMBMHdl);
    
    pxTimerIntHdl->bIsRunning = FALSE;
}

/*
void vMBPTimerTask( void *pvParameters )
{
    UBYTE           ubIdx;
    xTimerInternalHandle *pxTmrHdl;
    portTickType    xLastWakeTime;
    portTickType    xCurrentTime;

    xLastWakeTime = xTaskGetTickCount(  );
    for( ;; )
    {
        vTaskDelayUntil( &xLastWakeTime, ( portTickType ) TIMER_TICKRATE_MS );
        xCurrentTime = xTaskGetTickCount(  );
        MBP_ENTER_CRITICAL_SECTION(  );
        for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
        {
            pxTmrHdl = &arxTimerHdls[ubIdx];
            if( ( IDX_INVALID != pxTmrHdl->ubIdx ) && ( pxTmrHdl->bIsRunning ) )
            {
                if( ( xCurrentTime - pxTmrHdl->xNExpiryTimeTicks ) >= pxTmrHdl->xNTimeoutTicks )
                {
                    DEBUG_TIMEREXPIRED_ON;
                    pxTmrHdl->bIsRunning = FALSE;
                    if( NULL != pxTmrHdl->pbMBPTimerExpiredFN )
                    {
                        ( void )pxTmrHdl->pbMBPTimerExpiredFN( pxTmrHdl->xMBMHdl );
                    }
                    DEBUG_TIMEREXPIRED_OFF;                    
                }
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
}
*/


//#####################################################################
//#####################################################################
//#####################################################################
//#####################################################################
//#####################################################################
//#####################################################################
//#####################################################################

/*
eMBErrorCode eMBPTimerInit( xMBPTimerHandle * xTimerHdl, USHORT usTimeOut1ms,
               pbMBPTimerExpiredCB pbMBPTimerExpiredFN, xMBHandle xHdl )
{
    eMBErrorCode    eStatus = MB_EPORTERR;
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( ( NULL != xTimerHdl ) && ( NULL != pbMBPTimerExpiredFN ) && ( MB_HDL_INVALID != xHdl ) )
    {
        if( !bIsInitalized )
        {
            DEBUG_INIT;
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                RESET_HDL( &arxTimerHdls[ubIdx] );
            }
#if defined( WITH_LWIP )            
            if( NULL == sys_thread_new( "MBP-TIMER", vMBPTimerTask, NULL, TIMER_TASK_STACKSIZE, TIMER_TASK_PRIORITY ) )
            {
                eStatus = MB_EPORTERR;
            }
#else          
            if( pdPASS != xTaskCreate( vMBPTimerTask, "MBP-TIMER", TIMER_TASK_STACKSIZE, NULL, TIMER_TASK_PRIORITY, NULL ) )
            {
                eStatus = MB_EPORTERR;
            }
#endif            
            else
            {
                bIsInitalized = TRUE;
            }
        }
        if( bIsInitalized )
        {
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                if( IDX_INVALID == arxTimerHdls[ubIdx].ubIdx )
                {
                    break;
                }
            }
            if( MAX_TIMER_HDLS != ubIdx )
            {
                arxTimerHdls[ubIdx].ubIdx = ubIdx;
                arxTimerHdls[ubIdx].bIsRunning = FALSE;
                arxTimerHdls[ubIdx].xNTimeoutTicks = MB_INTDIV_CEIL( ( portTickType ) usTimeOut1ms, portTICK_RATE_MS );
                arxTimerHdls[ubIdx].xNExpiryTimeTicks = 0;
                arxTimerHdls[ubIdx].xMBMHdl = xHdl;
                arxTimerHdls[ubIdx].pbMBPTimerExpiredFN = pbMBPTimerExpiredFN;

                *xTimerHdl = &arxTimerHdls[ubIdx];
                eStatus = MB_ENOERR;
            }
            else
            {
                eStatus = MB_ENORES;
            }
        }
    }
    else
    {
        eStatus = MB_EINVAL;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


void vMBPTimerClose( xMBPTimerHandle xTimerHdl )
{
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        RESET_HDL( pxTimerIntHdl );
    }
}


eMBErrorCode eMBPTimerSetTimeout( xMBPTimerHandle xTimerHdl, USHORT usTimeOut1ms )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) && ( usTimeOut1ms > 0 ) )
    {
        pxTimerIntHdl->xNTimeoutTicks = MB_INTDIV_CEIL( ( portTickType ) usTimeOut1ms, portTICK_RATE_MS );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


eMBErrorCode eMBPTimerStart( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;

    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    DEBUG_TIMERSTART_ON;
    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = TRUE;
        pxTimerIntHdl->xNExpiryTimeTicks = xTaskGetTickCount(  );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    DEBUG_TIMERSTART_OFF;
    return eStatus;
}


eMBErrorCode eMBPTimerStop( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = FALSE;
        pxTimerIntHdl->xNExpiryTimeTicks = 0;
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


void vMBPTimerTask( void *pvParameters )
{
    UBYTE           ubIdx;
    xTimerInternalHandle *pxTmrHdl;
    portTickType    xLastWakeTime;
    portTickType    xCurrentTime;

    xLastWakeTime = xTaskGetTickCount(  );
    for( ;; )
    {
        vTaskDelayUntil( &xLastWakeTime, ( portTickType ) TIMER_TICKRATE_MS );
        xCurrentTime = xTaskGetTickCount(  );
        MBP_ENTER_CRITICAL_SECTION(  );
        for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
        {
            pxTmrHdl = &arxTimerHdls[ubIdx];
            if( ( IDX_INVALID != pxTmrHdl->ubIdx ) && ( pxTmrHdl->bIsRunning ) )
            {
                if( ( xCurrentTime - pxTmrHdl->xNExpiryTimeTicks ) >= pxTmrHdl->xNTimeoutTicks )
                {
                    DEBUG_TIMEREXPIRED_ON;
                    pxTmrHdl->bIsRunning = FALSE;
                    if( NULL != pxTmrHdl->pbMBPTimerExpiredFN )
                    {
                        ( void )pxTmrHdl->pbMBPTimerExpiredFN( pxTmrHdl->xMBMHdl );
                    }
                    DEBUG_TIMEREXPIRED_OFF;                    
                }
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
}
*/


