//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

/* ----------------------- System includes --------------------------------*/
#include "xc.h"
#include "mbport.h"
#include <FreeRTOS.h>


/* ----------------------- Application includes ---------------------------*/
/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
/* ----------------------- Global Functions -------------------------------*/
void usart2HardwareInit(void)
{

}


void usart2HardwareClose(void)
{
    U2MODEbits.ON = 0;
}


void usart2InterruptInit(void)
{

}


void usart2RXInterruptEnable(void)
{
    IFS4bits.U2RXIF = 0;
    IEC4bits.U2RXIE = 1;
}


void usart2RXInterruptDisable(void)
{
    IEC4bits.U2RXIE = 0;
}


void usart2TXInterruptEnable(void)
{
    IFS4bits.U2TXIF = 0;
    IEC4bits.U2TXIE = 1;
}


void usart2TXInterruptDisable(void)
{
    IEC4bits.U2TXIE = 0;
}


void usart2RXInterruptClear(void)
{
    IFS4bits.U2RXIF = 0;
}


void usart2TXInterruptClear(void)
{
    IFS4bits.U2TXIF = 0;
}


void usart2PutChar(UBYTE data)
{
    U2TXREG = data;
}


UBYTE usart2GetChar(void)
{
    return U2RXREG;
}


/* ----------------------- Local  Functions -------------------------------*/
