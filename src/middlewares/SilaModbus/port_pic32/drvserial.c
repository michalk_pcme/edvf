//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"
#include "mbportusart4.h"

/* ----------------------- MODBUS -------------------------------------------*/
#include "common/mbtypes.h"
#include "common/mbframe.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"

#include "drvserial.h"

/* ----------------------- Hardware mapping - Must come first ---------------*/
/* ----------------------- Defines ------------------------------------------*/
#define DRV_SERIAL_IS_VALID( ubPort, xHdls )	\
	( ( ubPort < DRV_SERIAL_MAX_INSTANCES ) && ( xHdls[ ubPort ].bIsInitialized  ) )

#define HDL_RESET( x )						do { \
	( x )->bIsInitialized = FALSE; \
	( x )->xSemHdl = NULL; \
	( x )->usEventMask = 0; \
} while( 0 )


/* ----------------------- Type definitions ---------------------------------*/

typedef enum{
    TX_ISR,
    RX_ISR
}uartIsrTypes;


typedef struct
{
    BOOL             bIsInitialized;
    xSemaphoreHandle xSemHdl;
    USHORT           usEventMask;
    USHORT           activeOperations;
    serHrdwHandle_t* hardHdl; 
} xDrvSerialHandle;


/* ----------------------- Static functions ---------------------------------*/
STATIC portBASE_TYPE xSerialIntHandler(UBYTE ubIdx, uartIsrTypes isrType);

STATIC BOOL evenParityCheck(BYTE data);
STATIC BYTE oddParityInjector(BYTE data);
STATIC BYTE evenParityInjector(BYTE data);


/* ----------------------- Static variables ---------------------------------*/
STATIC xDrvSerialHandle xDrvSerialHdls[DRV_SERIAL_MAX_INSTANCES] =
{
    {0,0,0,0,&uart4HrdwHandle}
};


/* ----------------------- Start implementation -----------------------------*/
eMBErrorCode eDrvSerialInit( UBYTE ubPort, ULONG ulBaudRate, UBYTE ucDataBits, eMBSerialParity eParity, UBYTE ucStopBits )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    
    MBP_ENTER_CRITICAL_SECTION(  );
    
    if( ( ubPort < DRV_SERIAL_MAX_INSTANCES ) && ( !xDrvSerialHdls[ubPort].bIsInitialized ) )
    {
        HDL_RESET( &xDrvSerialHdls[ubPort] );
        xDrvSerialHdls[ubPort].bIsInitialized = TRUE;

        vSemaphoreCreateBinary( xDrvSerialHdls[ubPort].xSemHdl );
        if( NULL == xDrvSerialHdls[ubPort].xSemHdl )
        {
            eStatus = MB_ENORES;
        }
        else
        {
            portENTER_CRITICAL(  );
            xDrvSerialHdls[ubPort].hardHdl->initHardware(ulBaudRate, ucDataBits, ucStopBits, eParity);
            xDrvSerialHdls[ubPort].hardHdl->initInterrupts();
            portEXIT_CRITICAL(  );
            
            eStatus = MB_ENOERR;
        }
        
        if(eStatus != MB_ENOERR){
            if( NULL != xDrvSerialHdls[ubPort].xSemHdl )
                vQueueDelete( xDrvSerialHdls[ubPort].xSemHdl );
                        
            HDL_RESET( &xDrvSerialHdls[ubPort] );
        }
    }
    
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


eMBErrorCode eDrvSerialClose( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    
    MBP_ENTER_CRITICAL_SECTION(  );
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {        
        portENTER_CRITICAL(  );
        xDrvSerialHdls[ubPort].hardHdl->closeHardware();
        portEXIT_CRITICAL(  );
        
        vQueueDelete( xDrvSerialHdls[ubPort].xSemHdl );
        HDL_RESET( &xDrvSerialHdls[ubPort] );
        eStatus = MB_ENOERR;
        
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


//this function is waiting for a semaphore from a TX/RX interrupt to wake it up
eMBErrorCode eDrvSerialWaitEvent( UBYTE ubPort, USHORT * pusEvents, USHORT usTimeOut )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        //this semaphore gets woken up by the RX interrupt to trigger reception of each character separately
        //transmission is being triggered from eDrvSerialTransmitEnable()
        //TX interrupt is waking up only in its own transmission loop (eDrvSerialTransmit()).
        if( pdTRUE == xSemaphoreTake( xDrvSerialHdls[ubPort].xSemHdl, usTimeOut / portTICK_RATE_MS ) )
        {            
            portENTER_CRITICAL(  );
            
            *pusEvents = xDrvSerialHdls[ubPort].usEventMask;
            
            if((xDrvSerialHdls[ubPort].activeOperations & DRV_SERIAL_RX_ACTIVE) > 0)
                *pusEvents |= DRV_SERIAL_EVENT_RXRDY;
            
            if((xDrvSerialHdls[ubPort].activeOperations & DRV_SERIAL_TX_ACTIVE) > 0)
                *pusEvents |= DRV_SERIAL_EVENT_TXRDY;
                
            xDrvSerialHdls[ubPort].usEventMask = 0;
            portEXIT_CRITICAL(  );            
        }
        else
        {
            *pusEvents = 0;
        }
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialAbort( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        portENTER_CRITICAL(  );
        xDrvSerialHdls[ubPort].usEventMask |= DRV_SERIAL_EVENT_ABORT;
        portEXIT_CRITICAL(  );
        ( void )xSemaphoreGive( xDrvSerialHdls[ubPort].xSemHdl );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialTransmitEnable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {        
        //enable driver for transmission without triggering interrupts
        xDrvSerialHdls[ubPort].hardHdl->txEnable(TRUE);
        xDrvSerialHdls[ubPort].activeOperations |= DRV_SERIAL_TX_ACTIVE;
         
        //wake up vMBPSerialHandlerTask for transmission (through eDrvSerialWaitEvent())
        (void)xSemaphoreGive(xDrvSerialHdls[ubPort].xSemHdl);
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialTransmitDisable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {        
        portENTER_CRITICAL(  );
        xDrvSerialHdls[ubPort].hardHdl->txEnable(FALSE);
        xDrvSerialHdls[ubPort].activeOperations &= (USHORT)(~DRV_SERIAL_TX_ACTIVE);
        portEXIT_CRITICAL(  );
        
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


//return number of bytes that can be sent - what for???
eMBErrorCode eDrvSerialTransmitFree( UBYTE ubPort, USHORT * pusNFreeBytes )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( ( NULL != pusNFreeBytes ) && DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        // We can transmit an infinite number of bytes
        *pusNFreeBytes = 0xFFFFU;
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialTransmit( UBYTE ubPort, const UBYTE * pubBuffer, USHORT usLength )
{
    USHORT          usIdx;
    UBYTE           ubValue;
    eMBErrorCode    eStatus = MB_EINVAL;
    if( ( NULL != pubBuffer ) && DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        for( usIdx = 0; usIdx < usLength; usIdx++)
        {
            //get next byte for transmission
            ubValue = pubBuffer[usIdx];
            
            //software parity calculation for 7 bit data
            if(7 == xDrvSerialHdls[ubPort].hardHdl->ucDataBits){
                switch(xDrvSerialHdls[ubPort].hardHdl->eParity){
                    case MB_PAR_ODD:
                        ubValue = oddParityInjector(ubValue); break;
                    case MB_PAR_EVEN: 
                        ubValue = evenParityInjector(ubValue); break;
                    case MB_PAR_NONE: 
                        ubValue |= 0x80;
                        break;
                }
            }
            
            //enable interrupts to get right moment for sending of another byte of data
            xDrvSerialHdls[ubPort].hardHdl->txInterruptEnable(TRUE);
            
            //now wait till TX gets ready for transmission
            while(pdFALSE == xSemaphoreTake( xDrvSerialHdls[ubPort].xSemHdl, 0xFFFF))
                asm("nop");
            
            //put the value into transmission buffer
            xDrvSerialHdls[ubPort].hardHdl->putChar(ubValue);
        }
        
        //enable interrupts to wait for notification of last byte being sent
        xDrvSerialHdls[ubPort].hardHdl->txInterruptEnable(TRUE);
        
        //wait till the last byte gets sent
        while(pdFALSE == xSemaphoreTake( xDrvSerialHdls[ubPort].xSemHdl, 0xFFFF))
            asm("nop");
        
        //disable transmission interrupts
        xDrvSerialHdls[ubPort].hardHdl->txInterruptEnable(FALSE);
        xDrvSerialHdls[ubPort].hardHdl->txInterruptClear();

        // We can transmit more data - Signal immediately
        ( void )xSemaphoreGive( xDrvSerialHdls[ubPort].xSemHdl );

        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialReceiveEnable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        xDrvSerialHdls[ubPort].hardHdl->rxInterruptClear();
        xDrvSerialHdls[ubPort].hardHdl->rxEnable(TRUE);
        xDrvSerialHdls[ubPort].activeOperations |= DRV_SERIAL_RX_ACTIVE;
        xDrvSerialHdls[ubPort].hardHdl->rxInterruptEnable(TRUE);
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialReceiveDisable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        portENTER_CRITICAL(  );
        xDrvSerialHdls[ubPort].hardHdl->rxEnable(FALSE);
        xDrvSerialHdls[ubPort].hardHdl->rxInterruptEnable(FALSE);
        xDrvSerialHdls[ubPort].hardHdl->rxInterruptClear();
        xDrvSerialHdls[ubPort].activeOperations &= (USHORT)(~DRV_SERIAL_RX_ACTIVE);
        portEXIT_CRITICAL(  );
        
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


//collect just one byte from RX register and pass it into the buffer
eMBErrorCode eDrvSerialReceive( UBYTE ubPort, UBYTE * pubBuffer, USHORT usLengthMax, USHORT * pusNBytesReceived )
{
    UBYTE data;
    BOOL validData;
    
    eMBErrorCode    eStatus = MB_EINVAL;
    if( ( NULL != pubBuffer ) && ( NULL != pusNBytesReceived ) && DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {        
        *pusNBytesReceived = 0;
        
        //collect data from the RX register
        if(7 == xDrvSerialHdls[ubPort].hardHdl->ucDataBits){
            data = xDrvSerialHdls[ubPort].hardHdl->getChar(&validData);
            if(TRUE == validData){
                //most significant bit is a parity bit, therefore remove it from the value
                pubBuffer[*pusNBytesReceived] = (BYTE)(0x7F & data);
                *pusNBytesReceived = 1;
            }
        }
        else{
            data = xDrvSerialHdls[ubPort].hardHdl->getChar(&validData);
            if(TRUE == validData){
                pubBuffer[*pusNBytesReceived] = data;
                *pusNBytesReceived = 1;
            }
        }
        
        //enable rx interrupts
        xDrvSerialHdls[ubPort].hardHdl->rxInterruptEnable(TRUE);
        
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


eMBErrorCode eDrvSerialReceiveReset( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    BOOL temp;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        xDrvSerialHdls[ubPort].hardHdl->getChar(&temp);
        xDrvSerialHdls[ubPort].hardHdl->rxHandleErrors();
        eStatus = MB_ENOERR;
    }
    return eStatus;
}


//this actual interrupt handler called from assembly interrupt wrapper
void vUart4TxInterruptHandler(void)
{
    portBASE_TYPE   xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = xSerialIntHandler(USART_USART4_IDX, TX_ISR);
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}


//this actual interrupt handler called from assembly interrupt wrapper
void vUart4RxInterruptHandler(void)
{
    portBASE_TYPE   xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = xSerialIntHandler(USART_USART4_IDX, RX_ISR);
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}


STATIC portBASE_TYPE xSerialIntHandler(UBYTE ubIdx, uartIsrTypes isrType)
{
    signed portBASE_TYPE xHigherPriorityTaskWoken;
       
    switch(isrType){
        case TX_ISR:
            //disable TX interrupts so they do not get triggered again due to empty TX buffer
            xDrvSerialHdls[ubIdx].hardHdl->txInterruptEnable(FALSE);  //my line of code
            xDrvSerialHdls[ubIdx].hardHdl->txInterruptClear();
            
            //wake up eDrvSerialTransmit()
            xSemaphoreGiveFromISR( xDrvSerialHdls[ubIdx].xSemHdl, &xHigherPriorityTaskWoken );
            break;
            
        case RX_ISR:
            //disable interrupts so they do not get triggered again as RX data still sits in the buffer
            xDrvSerialHdls[ubIdx].hardHdl->rxInterruptEnable(FALSE);
            xDrvSerialHdls[ubIdx].hardHdl->rxHandleErrors();
            xDrvSerialHdls[ubIdx].hardHdl->rxInterruptClear();
            
            //wake up eDrvSerialWaitEvent()
            xSemaphoreGiveFromISR( xDrvSerialHdls[ubIdx].xSemHdl, &xHigherPriorityTaskWoken );
            break;
            
        default:
            break;
    }

    return xHigherPriorityTaskWoken;
}


STATIC BOOL evenParityCheck(BYTE data)
{
    char BITAND;
    char PARITY;
    char BT;

    PARITY = 0;
    BITAND = 1;

    for (BT = 0; BT < 7; BT++) {
	
        if (data & BITAND) {
            PARITY++;
        }

        BITAND <<= 1;
    }
  
    if ((PARITY & 1) == 0) {
        return FALSE;
    }
    
    return TRUE;
}


STATIC BYTE oddParityInjector(BYTE data)
{     
    if (FALSE == evenParityCheck(data)) {
        data |= 0x80;
    }
    
    return data;
}


STATIC BYTE evenParityInjector(BYTE data)
{    
    if (TRUE == evenParityCheck(data)) {
        data |= 0x80;
    }
    
    return data;
}


