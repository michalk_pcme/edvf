//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

/* ----------------------- System includes --------------------------------*/
#include "xc.h"
#include "mbport.h"
//#include "system_config.h"


/* ----------------------- Application includes ---------------------------*/
/* ----------------------- Defines ----------------------------------------*/
#define RS485_DIR           LATBbits.LATB8
#define TRIS_RS485_Dir      TRISBbits.TRISB8


/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
static void usart4HardwareInit(ULONG ulBaudRate, UCHAR ucDataBits, UCHAR ucStopBits, eMBSerialParity eParity);
static void usart4HardwareClose(void);
static void usart4InterruptInit(void);
   
static void usart4RXEnable(BOOL);
static void usart4RXInterruptEnable(BOOL);
static void usart4RXInterruptClear(void);
static BOOL usart4RXHandleErrors(void);
    
static void usart4TXEnable(BOOL);
static void usart4TXInterruptEnable(BOOL);
static void usart4TXInterruptClear(void);
    
//static UBYTE usart4GetChar(BOOL);
static UBYTE usart4GetChar(BOOL * dataValid);
static void usart4PutChar(UBYTE);


/* ----------------------- Global Variables -------------------------------*/
serHrdwHandle_t uart4HrdwHandle = 
{    
    0,
    0,
    0,
    0,
    
    &usart4HardwareInit,
    &usart4HardwareClose,
    &usart4InterruptInit,
    
    &usart4RXEnable,
    &usart4RXInterruptEnable,
    &usart4RXInterruptClear,
    &usart4RXHandleErrors,
    
    &usart4TXEnable,
    &usart4TXInterruptEnable,
    &usart4TXInterruptClear,  
    
    &usart4GetChar,
    &usart4PutChar
};


/* ----------------------- Global Functions -------------------------------*/
static void usart4HardwareInit(ULONG ulBaudRate, UCHAR ucDataBits, UCHAR ucStopBits, eMBSerialParity eParity)
{
    ULONG baud;
    
    //configure pins (assume that analog option is disabled))
    TRIS_RS485_Dir = 0;
    RS485_DIR = 1; //1 PIC TX  to Slave
    
    U4RXR = 7;
    RPB7R = 2;
    
    //configure baud rate
    baud  = ((PBCLK2_FREQ/ulBaudRate)/16)-1;
    U4BRG = baud;
            
    //U4BRG = 321;    //19200 BAUD
    //U4BRG = 160;    //38400 BAUD
    //U4BRG = 106;    //57600 BAUD
    //U4BRG = 53;     //115200 BAUD
    
    //reset peripheral
    U4STA  = 0;
    U4MODE = 0;
    
    //configure peripheral according to the mode
    switch(ucDataBits){
        case 7:
            U4MODEbits.STSEL = 0;    //1 stop bit
            U4MODEbits.PDSEL = 0;    //8 bit data, no parity
            //parity in ASCII mode is software generated and injected
            //to every sent byte!
            break;
            
        case 8:
            switch(eParity){
                case MB_PAR_ODD:  
                    U4MODEbits.PDSEL = 2;    //8 bit data, odd parity
                    U4MODEbits.STSEL = 0;    //1 stop bit
                    break;
                    
                case MB_PAR_EVEN: 
                    U4MODEbits.PDSEL = 1;    //8 bit data, even parity
                    U4MODEbits.STSEL = 0;    //1 stop bit
                    break;
                    
                case MB_PAR_NONE: 
                    U4MODEbits.PDSEL = 0;    //8 bit data, no parity
                    U4MODEbits.STSEL = 1;    //2 stop bit
                    break;
            }
            break;
    }
    
    //fill up uart settings
    uart4HrdwHandle.ulBaudRate = ulBaudRate;
    uart4HrdwHandle.ucDataBits = ucDataBits;
    uart4HrdwHandle.ucStopBits = ucStopBits;
    uart4HrdwHandle.eParity    = eParity;
        
    U4STAbits.UTXEN = 0;    //disable transmitter
    U4STAbits.URXEN = 0;    //disable receiver
    
    U4STAbits.OERR = 0;     //reset receive overflow status bit

    U4MODEbits.BRGH = 0;    //standard mode 16x baud clock
    U4MODEbits.ON = 1;
}


static void usart4HardwareClose(void)
{
    U4MODEbits.ON = 0;
}


static void usart4InterruptInit(void)
{
    //interrupt is generated when all characters have been transmitted
    U4STASET = 0X00004000; //U4STAbits.UTXSEL( bit 15,14) =01; 

    //USARTX TX interrupt
    IPC43bits.U4TXIS = 0;
    IPC43bits.U4TXIP = UART_ISR_PRIORITY;
    IFS5bits.U4TXIF = 0;
    IEC5bits.U4TXIE = 0;
    
    //USARTX RX interrupt
    IPC42bits.U4RXIS = 0;
    IPC42bits.U4RXIP = UART_ISR_PRIORITY;   
    IFS5bits.U4RXIF = 0;
    IEC5bits.U4RXIE = 0;
    
    //USARTX FAULT interrupt
    IPC42bits.U4EIS = 0;
    IPC42bits.U4EIP = UART_ISR_PRIORITY;
    IFS5bits.U4EIF = 0;
    IEC5bits.U4EIE = 0;
}


static void usart4RXEnable(BOOL enable)
{
    if(TRUE == enable){
        RS485_DIR = 0;
        U4STAbits.URXEN = 1;    //enable receiver
    }
    else{
        //RS485_DIR = 1;
        U4STAbits.URXEN = 0;    //disable receiver
    }
}


static void usart4RXInterruptEnable(BOOL enable)
{
    volatile BYTE temp;
    
    (void)(temp);
    
    if(TRUE == enable){
        //dummy read to clean the register
        temp = U4RXREG;
        
        IFS5bits.U4RXIF = 0;
        IEC5bits.U4RXIE = 1;
    }
    else{
        IEC5bits.U4RXIE = 0;
    }
}


static void usart4RXInterruptClear(void)
{
    IFS5bits.U4RXIF = 0;
}


//returns TRUE - no errors, FALSE - error detected
static BOOL usart4RXHandleErrors(void)
{
    volatile unsigned int staReg;
    
    //check for errors
    staReg = U4STA;
    if(0 != (staReg & (unsigned int)0x000E)){
       //clear errors
        U4STAbits.OERR = 0;
        
        return FALSE;
    }
    
    return TRUE;
}


static void usart4TXEnable(BOOL enable)
{
    if(TRUE == enable){
        RS485_DIR = 1;
        U4STAbits.UTXEN = 1;
    }
    else{
        U4STAbits.UTXEN = 0;
    }
}


static void usart4TXInterruptEnable(BOOL enable)
{
    if(TRUE == enable){
        IFS5bits.U4TXIF = 0;
        IEC5bits.U4TXIE = 1;
    }
    else{
        IEC5bits.U4TXIE = 0;
    }    
}


static void usart4TXInterruptClear(void)
{
    IFS5bits.U4TXIF = 0;
}


static void usart4PutChar(UBYTE data)
{
    U4TXREG = data;
}


//Returns data from a RX buffer regardles if there was anything in it.
//dataValid specifies if the character comes from a reception or if it
//is a random stuff in the register.
static UBYTE usart4GetChar(BOOL * dataValid)
{
    if(U4STAbits.URXDA == 0)
        *dataValid = FALSE;
    else
        *dataValid = TRUE;
    
    return U4RXREG;    
}

/*
static UBYTE usart4GetChar(BOOL override)
{
    if(TRUE == override)
        return U4RXREG;
    else{
        while(U4STAbits.URXDA == 0)
            asm("nop");
    
        return U4RXREG;
    }    
}
*/

/* ----------------------- Local  Functions -------------------------------*/



