//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include "xc.h"
#include <sys/attribs.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>


/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"


/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

#include "drvserial.h"

#include "mbportusart4.h"
#include "mbportusart2.h"


/* ----------------------- Defines ------------------------------------------*/
#define MBP_SERIAL_TASK_PRIORITY        	( MBP_TASK_PRIORITY )
#define MBP_SERIAL_TASK_STACKSIZE           ( 128 )
#define MBP_SERIAL_BUFFER_SIZE	            ( 16 )

#define IDX_INVALID                         ( 255 )

#define HDL_RESET( x )						do { \
	( x )->ubIdx = IDX_INVALID; \
	( x )->xMBHdl = FALSE; \
	( x )->bIsRunning = FALSE; \
	( x )->bIsBroken = FALSE; \
	( x )->pbMBPTransmitterEmptyFN = NULL; \
	( x )->pvMBPReceiveFN = NULL; \
} while( 0 )


/* ----------------------- Function prototypes ------------------------------*/
/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    xMBHandle       xMBHdl;
    BOOL            bIsRunning;
    BOOL            bIsBroken;
    pbMBPSerialTransmitterEmptyAPIV2CB pbMBPTransmitterEmptyFN;
    pvMBPSerialReceiverAPIV2CB pvMBPReceiveFN;
} xSerialHandle;


/* ----------------------- Static functions ---------------------------------*/
STATIC void     vMBPSerialHandlerTask( void *pvArg );


/* ----------------------- Static variables ---------------------------------*/
STATIC BOOL     bIsInitalized = FALSE;
STATIC xSerialHandle xSerialHdls[DRV_SERIAL_MAX_INSTANCES];


/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode eMBPSerialInit( xMBPSerialHandle * pxSerialHdl, UCHAR ucPort, ULONG ulBaudRate,
                UCHAR ucDataBits, eMBSerialParity eParity, UCHAR ucStopBits, xMBHandle xMBMHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( !bIsInitalized )
    {
        for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xSerialHdls ); ubIdx++ )
        {
            HDL_RESET( &xSerialHdls[ubIdx] );
        }
        bIsInitalized = TRUE;
    }

    if( ( ucPort < DRV_SERIAL_MAX_INSTANCES ) && ( IDX_INVALID == xSerialHdls[ucPort].ubIdx ) )
    {
        HDL_RESET( &xSerialHdls[ucPort] );
        xSerialHdls[ucPort].xMBHdl = xMBMHdl;
        xSerialHdls[ucPort].ubIdx = ucPort;
        xSerialHdls[ucPort].bIsRunning = TRUE;

        if( MB_ENOERR != eDrvSerialInit( ucPort, ulBaudRate, ucDataBits, eParity, ucStopBits ) )
        {
            eStatus = MB_EPORTERR;
        }
        else if( pdPASS !=
                 xTaskCreate( vMBPSerialHandlerTask, "MBP-SER", MBP_SERIAL_TASK_STACKSIZE, &xSerialHdls[ucPort],
                              MBP_SERIAL_TASK_PRIORITY, NULL ) )
        {
            eStatus = MB_EPORTERR;
        }
        else
        {
            *pxSerialHdl = &xSerialHdls[ucPort];
            eStatus = MB_ENOERR;
        }

        if( MB_ENOERR != eStatus )
        {
            HDL_RESET( &xSerialHdls[ucPort] );
        }
    }
    else
    {
        eStatus = MB_ENORES;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


eMBErrorCode eMBPSerialClose( xMBPSerialHandle xSerialHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xSerialHandle  *pxSerialIntHdl = xSerialHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( ( NULL != pxSerialIntHdl ) && MB_IS_VALID_HDL( pxSerialIntHdl, xSerialHdls ) )
    {
        pxSerialIntHdl->bIsRunning = FALSE;
        ( void )eDrvSerialAbort( pxSerialIntHdl->ubIdx );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


eMBErrorCode eMBPSerialTxEnable( xMBPSerialHandle xSerialHdl, pbMBPSerialTransmitterEmptyCB pbMBPTransmitterEmptyFN )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xSerialHandle  *pxSerialIntHdl = xSerialHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxSerialIntHdl, xSerialHdls ) )
    {
        if( pxSerialIntHdl->bIsBroken )
        {
            eStatus = MB_EIO;
        }
        else
        {
            if( NULL != pbMBPTransmitterEmptyFN )
            {
                //attach transmitter callback
                pxSerialIntHdl->pbMBPTransmitterEmptyFN = pbMBPTransmitterEmptyFN;
                
                //enable hardware for transmission (this will trigger vMBPSerialHandlerTask)
                eStatus = eDrvSerialTransmitEnable( pxSerialIntHdl->ubIdx );
            }
            else
            {
                //no transmission callback function attached, therefore close transmission
                eStatus = eDrvSerialTransmitDisable( pxSerialIntHdl->ubIdx );
                pxSerialIntHdl->pbMBPTransmitterEmptyFN = NULL;
            }
        }
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


//calculates timeout depending on baud rate and FIFO settings
USHORT usMPSerialTimeout( ULONG ulBaudRate )
{
    USHORT          usTimeoutMS;
    /* Timeout is size of FIFO (assume maximum fill) multiplied by
     * 11 bit times divided by baudrate in milliseconds.
     */
    usTimeoutMS = MB_INTDIV_CEIL( 1000UL * 11UL * 16UL, ulBaudRate );
    if( usTimeoutMS <= 2 )
    {
        usTimeoutMS = 2;
    }
    return usTimeoutMS;
}


eMBErrorCode eMBPSerialRxEnable( xMBPSerialHandle xSerialHdl, pvMBPSerialReceiverCB pvMBPReceiveFN )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xSerialHandle  *pxSerialIntHdl = xSerialHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxSerialIntHdl, xSerialHdls ) )
    {
        if( pxSerialIntHdl->bIsBroken )
        {
            eStatus = MB_EIO;
        }
        else
        {
            if( NULL != pvMBPReceiveFN )
            {
                //reset the hardware
                (void)eDrvSerialReceiveReset( pxSerialIntHdl->ubIdx );
                
                //attach reception callback
                pxSerialIntHdl->pvMBPReceiveFN = pvMBPReceiveFN;
                
                //enable receiver (it will start using RX interrupts to trigger vMBPSerialHandlerTask)
                eStatus = eDrvSerialReceiveEnable( pxSerialIntHdl->ubIdx );
            }
            else
            {   
                //no reception callback function attached, therefore close reception
                eStatus = eDrvSerialReceiveDisable( pxSerialIntHdl->ubIdx );
                pxSerialIntHdl->pvMBPReceiveFN = NULL;
            }
        }
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


STATIC void vMBPSerialHandlerTask( void *pvArg )
{
    xSerialHandle  *pxSerialIntHdl = pvArg;
    UBYTE           arubBuffer[16];
    USHORT          usCnt = 0;
    USHORT          usByteMax;
    BOOL            bIsRunning;
    USHORT          usEvents;
    eMBErrorCode    eStatus;
    do
    {
        //wait till TX/RX interrupt wakes up the task
        eStatus = eDrvSerialWaitEvent( pxSerialIntHdl->ubIdx, &usEvents, 5000 );
        MBP_ASSERT( MB_ENOERR == eStatus );
        
        if( ( usEvents & DRV_SERIAL_EVENT_RXRDY ) > 0 )
        {
            //RX transmission is ongoing one and needs processing
            
            //get received data from a driver
            eStatus = eDrvSerialReceive( pxSerialIntHdl->ubIdx, arubBuffer, MB_UTILS_NARRSIZE( arubBuffer ), &usCnt );
            
            MBP_ENTER_CRITICAL_SECTION(  );
            if( MB_ENOERR == eStatus )
            {
                if( NULL != pxSerialIntHdl->pvMBPReceiveFN )
                {
                    // push received chunk of data into modbus library
                    pxSerialIntHdl->pvMBPReceiveFN( pxSerialIntHdl->xMBHdl, arubBuffer, usCnt );
                }
            }
            else
            {
                pxSerialIntHdl->bIsBroken = TRUE;
            }
            MBP_EXIT_CRITICAL_SECTION(  );
        }
        
        
        if( ( usEvents & DRV_SERIAL_EVENT_TXRDY ) > 0 )
        {
            //TX transmission is ongoing now and needs processing
            
            //check how much data can be sent in one go
            eStatus = eDrvSerialTransmitFree( pxSerialIntHdl->ubIdx, &usByteMax );
            
            if( MB_ENOERR == eStatus )
            {
                MBP_ENTER_CRITICAL_SECTION(  );
                if( ( usByteMax > 0 ) && ( NULL != pxSerialIntHdl->pbMBPTransmitterEmptyFN ) )
                {
                    // Make sure that we do not overrun our buffer. 
                    usByteMax = usByteMax > MB_UTILS_NARRSIZE( arubBuffer ) ? MB_UTILS_NARRSIZE( arubBuffer ) : usByteMax;
                    
                    // Fetch the data from the stack...
                    if( !pxSerialIntHdl->pbMBPTransmitterEmptyFN( pxSerialIntHdl->xMBHdl, arubBuffer, usByteMax, &usCnt ) )
                    {
                        // ... there was no data for transmission
                        pxSerialIntHdl->pbMBPTransmitterEmptyFN = NULL;
                        MBP_EXIT_CRITICAL_SECTION(  );
                        if( MB_ENOERR != eDrvSerialTransmitDisable( pxSerialIntHdl->ubIdx ) )
                        {
                            MBP_ENTER_CRITICAL_SECTION(  );
                            pxSerialIntHdl->bIsBroken = TRUE;
                            MBP_EXIT_CRITICAL_SECTION(  );
                        }
                    }
                    else
                    {
                        // ... and transmit data in a blocking way in one go
                        MBP_EXIT_CRITICAL_SECTION(  );
                        if( MB_ENOERR != eDrvSerialTransmit( pxSerialIntHdl->ubIdx, arubBuffer, usCnt ) )
                        {
                            MBP_ENTER_CRITICAL_SECTION(  );
                            pxSerialIntHdl->bIsBroken = TRUE;
                            MBP_EXIT_CRITICAL_SECTION(  );
                        }
                    }
                }
                else
                {
                    MBP_EXIT_CRITICAL_SECTION(  );
                }
            }
            else
            {
                pxSerialIntHdl->bIsBroken = TRUE;
            }

        }
        MBP_ENTER_CRITICAL_SECTION(  );
        bIsRunning = pxSerialIntHdl->bIsRunning;
        MBP_EXIT_CRITICAL_SECTION(  );
    }
    while( bIsRunning );

    eDrvSerialClose( pxSerialIntHdl->ubIdx );
    HDL_RESET( pxSerialIntHdl );
    vTaskDelete( NULL );
}


