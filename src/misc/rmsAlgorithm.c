//**************************************************************************
//
// Date				06.12.2017
//
// Product			Electrodynamic sensor
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include <math.h>

/* ----------------------- Application includes ---------------------------*/
#include "rmsAlgorithm.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
static void rmsReSetBlock(rmsSqBlock_t* window);


/* ----------------------- Global Functions -------------------------------*/

/**
 * @brief This function should be called only once at the beginning
 * 		  of the life of the driver.
 */
void rmsPreSetDriver(rmsDriver_t* driver)
{
	if(0 == driver) return;
	
	driver->sqBlocks = 0;
}


/**
 * @brief This function should be called at the end of the life of
 * 		  the driver or just to reset it for new configuration.
 */
void rmsReSetDriver(rmsDriver_t* driver)
{
	if(0 == driver) return;
	
	//free the allocated memory
	if(0 != driver->sqBlocks)
	{
		safeFree(driver->sqBlocks);
		driver->sqBlocks = 0;
	}
}


/**
 * @brief This function initialises driver accordingly to configuration
 * 	      structure.
 */
bool rmsConfigureDriver(rmsDriver_t* driver, rmsConfig_t* newConfig)
{
	uint32_t i;

	if(0 == driver) 	return false;
	if(0 == newConfig) 	return false;
	
	if(0 == newConfig->nbrOfBlocks) return false;
	if(0 == newConfig->nbrOfSamplesPerBlock) return false;

	//assign memory for the driver's RMS windows
	driver->sqBlocks = safeMalloc(newConfig->nbrOfBlocks * sizeof(rmsSqBlock_t));
	if(0 == driver->sqBlocks) return false;
	
	//copy configuration
	driver->config = *newConfig;
	
	//configure the rest of the driver
	driver->activeBlock 	= 0;
	driver->totalSampCtr 	= 0;
	driver->maxSampCtr 		= newConfig->nbrOfSamplesPerBlock * newConfig->nbrOfBlocks;
	driver->squareSums 		= 0;
	driver->rmsValue 		= 0;
	
	for(i=0; i<newConfig->nbrOfBlocks; i++)
		rmsReSetBlock(&driver->sqBlocks[i]);

	return true;
}


/**
 * @brief This function adds a new sample to the RMS driver.
 */
void rmsAddNewSampleOptimised(rmsDriver_t* driver, float sample)
{
	rmsSqBlock_t * window;

	//retrieve window
	window = &driver->sqBlocks[driver->activeBlock];

	//add new sample to window
	window->value += sample*sample;
	window->ctr++;
	driver->totalSampCtr++;

	//check if whole window has been collected
	if(window->ctr == driver->config.nbrOfSamplesPerBlock)
	{
		//append collected new sum of squares
		driver->squareSums += window->value;

		//change the active window to the next one in a circular buffer manner (point to the oldest one)
		driver->activeBlock++;
		if(driver->activeBlock == driver->config.nbrOfBlocks)
			driver->activeBlock = 0;

		window = &driver->sqBlocks[driver->activeBlock];

		//if all the required samples have been collected, then calculate RMS value
		if(driver->totalSampCtr == driver->maxSampCtr)
		{
			//calculate new RMS value from all the windows/samples
			driver->rmsValue = (float)sqrt(driver->squareSums/(float)driver->totalSampCtr);

			//new window has been collected, driver->set totalSampCtr to such a value,
			//that next RMS is going to get calculated at the end of the next window
			driver->totalSampCtr -= driver->config.nbrOfSamplesPerBlock;

			//subtract the oldest sum to get rid of the irrelevant samples in RMS calculations
			driver->squareSums -= window->value;
		}

		//reset the oldest window to prepare for the new collection of data
		rmsReSetBlock(window);
	}
}


float rmsGetRMSValue(rmsDriver_t* driver)
{
	return driver->rmsValue;
}


/* ----------------------- Local  Functions -------------------------------*/

static void rmsReSetBlock(rmsSqBlock_t* window)
{
	if(0 == window) return;

	window->ctr 	= 0;
	window->value 	= 0;
}


