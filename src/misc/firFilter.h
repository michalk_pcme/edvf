//**************************************************************************
//
// Date				08.12.2017
//
// Product			Electrodynamic sensor
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


#ifndef _FIR_FILTER_H_
#define _FIR_FILTER_H_

/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include <systemDefinitions.h>


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/

typedef struct
{
	uint16_t nbrOfCoefs;		/*!< number of filter coefficients*/
	float*   firCoefs;			/*!< pointer to buffer with filter coefficients*/
}firConfig_t;


typedef struct
{
	float* 	 samples;
	uint16_t size;
	uint16_t nextSamplePos;
}firBuffer_t;


typedef struct
{
	firBuffer_t buffer;
	firConfig_t config;
	float result;
}firDriver_t;


/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/

void firPreSetDriver(firDriver_t* driver);
void firReSetDriver(firDriver_t* driver);
bool firConfigureDriver(firDriver_t* driver, firConfig_t* newConfig);
void firAddNewSample(firDriver_t* driver, float sample);
float firGetFIRValue(firDriver_t* driver);


#endif   /* _FIR_FILTER_H_ */
















