//**************************************************************************
//
// Date				06.12.2017
//
// Product			Electrodynamic sensor
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


#ifndef _RMS_ALGORITHM_H_
#define _RMS_ALGORITHM_H_

/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include <systemDefinitions.h>


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/

typedef struct
{
	uint32_t nbrOfSamplesPerBlock;
	uint32_t nbrOfBlocks;
}rmsConfig_t;


typedef struct
{
	float value;		/*!< summed values*/
	uint32_t ctr;		/*!< counter of added samples*/
}rmsSqBlock_t;


typedef struct
{
	rmsConfig_t config;
	
	rmsSqBlock_t* sqBlocks;
	uint32_t activeBlock;
	uint32_t totalSampCtr;		/*!< total number of collected values in all blocks*/
	uint32_t maxSampCtr;		/*!< maximum number of samples needed for RMS calculation*/
	
	float squareSums;
	float rmsValue;
}rmsDriver_t;


/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void  rmsPreSetDriver(rmsDriver_t* driver);
void  rmsReSetDriver(rmsDriver_t* driver);
bool  rmsConfigureDriver(rmsDriver_t* driver, rmsConfig_t* newConfig);
void  rmsAddNewSampleOptimised(rmsDriver_t* driver, float sample);
float rmsGetRMSValue(rmsDriver_t* driver);

#endif   /* _RMS_ALGORITHM_H_ */


