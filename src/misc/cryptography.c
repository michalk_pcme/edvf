//**************************************************************************
//
// Date             21.09.2016
//
// Product          Low Cost Controller
//
// Or Module
//
// Details          Module has a software implementation of CRC-32 (0x04C11DB7)
//                  calculations.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Application includes ---------------------------*/
#include "cryptography.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
static inline uint32_t crc32poly(uint32_t);

static uint32_t unlockCodeGenerator(uint32_t, uint32_t, uint32_t);
static bool compare(uint16_t, uint32_t, uint64_t);
static uint8_t generate8bitXorCrc(uint8_t*, uint16_t);
static bool generateDeviceNumber(uint64_t*, uint8_t, uint32_t);
//static bool generateDeviceNumber(uint64_t*);


/* ----------------------- Global Functions -------------------------------*/

/**
 * @brief	Initialises hardware and software of the CRC module.
 * @note	Should be called at the very beginning of system initialisation.
 * @param	None
 * @retval	None
 */
void initCrcModule(void){
    
}


/**
 * @brief	De-initialises hardware of the CRC module.
 * @param	None
 * @retval	None
 */
void deInitCrcModule(void){
    
}


/**
 * @brief	Calculates CRC-32 value of the input data.
 * @param	data: pointer to data buffer which CRC should be calculated
 * @param	size: size of data in bytes
 * @retval	uint32_t CRC value
 */
uint32_t softCrc(const uint8_t *buffer, uint32_t size){

	uint32_t i;
	uint32_t temp;
	uint32_t crc = 0xFFFFFFFF;

	//support for 32bit aligned part of data
	i = size >> 2;

	while(i--){
		crc = crc ^ *((uint32_t *)buffer);
		buffer += 4;
		crc = crc32poly(crc);
	}

	//calculate remaining bytes that are not aligned to 32bits
	i = size & 3;
	while(i--){
		temp = *buffer;
		crc ^= (temp << (24));
		buffer++;
		crc = crc32poly(crc);
	}

	return crc;
}


//procedure returns option code that matches given unlock code
uint16_t getOptionCode(uint32_t unlockCode, uint8_t prodID, uint32_t deviceUID)
{
    uint16_t i;
    uint16_t optionCode;
    uint64_t deviceNum;
    
    //generate device number
    if(false == generateDeviceNumber(&deviceNum, prodID, deviceUID)) return 0;
    
    //browse through every bit in option code
    optionCode = 1;
    for(i=0; i<sizeof(uint16_t)*8; i++){
        
        if(true == compare(optionCode, unlockCode, deviceNum)){
            //we have got a winner!
            return optionCode;
        }
        
        //keep trying for different optionCode
        optionCode = (uint16_t)(optionCode << 11);
    }
    
    return 0;
}


/* ----------------------- Local  Functions -------------------------------*/

static inline uint32_t crc32poly(uint32_t crc){

	uint16_t j=32;

	while(j--){
	    if (crc & 0x80000000){
	    	crc = (crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32/PIC
	    }
	    else{
	    	crc = (crc << 1);
	    }
	}

	return crc;
}


static uint32_t unlockCodeGenerator(uint32_t a, uint32_t b, uint32_t c)
{
  a=a-b;  a=a-c;  a=a^(c >> 13);
  b=b-c;  b=b-a;  b=b^(a << 8);
  c=c-a;  c=c-b;  c=c^(b >> 13);
  a=a-b;  a=a-c;  a=a^(c >> 12);
  b=b-c;  b=b-a;  b=b^(a << 16);
  c=c-a;  c=c-b;  c=c^(b >> 5);
  a=a-b;  a=a-c;  a=a^(c >> 3);
  b=b-c;  b=b-a;  b=b^(a << 10);
  c=c-a;  c=c-b;  c=c^(b >> 15);
  return c;
}


static bool compare(uint16_t optionCode, uint32_t unlockCode, uint64_t deviceNum)
{
  
    uint32_t a;
    uint32_t b;
    uint32_t c;
    uint32_t genCode;

    a = (uint32_t)(deviceNum >> 16);
    b = (uint32_t)(deviceNum & 0xFFFFFFFF);
  
    c = optionCode;
    c = (c << 4) + optionCode;

    genCode = unlockCodeGenerator(a, b, c);
    
    return (unlockCode == genCode);
}


static uint8_t generate8bitXorCrc(uint8_t * data, uint16_t size)
{
    uint16_t i;
    uint8_t res;
    
    //safety checks
    if(0 == data) return 0;
    
    //calculate XOR value
    res = data[0];
    for(i=1; i<size; i++){
        res ^= data[i];
    }
    
    return res;
}


static bool generateDeviceNumber(uint64_t * deviceNum, uint8_t prodID, uint32_t deviceUID)
{
    uint8_t  crcVal;
    uint64_t res;
    
    //safety checks
    if(0 == deviceNum) return false;
    
    //calculate CRC over UID
    crcVal = generate8bitXorCrc((uint8_t*)&deviceUID, sizeof(uint32_t));
    
    //create device number
    res = 0;
    
    res |= prodID;  res <<= 8;
    res |= crcVal;     res <<= 32;
    res |= deviceUID;
    
    //write results
    *deviceNum = res;
    
    return true;
}

