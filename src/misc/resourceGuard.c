/**
  ******************************************************************************
  * @file      	resourceGuard.c
  * @author    	Michal Kalbarczyk
  * @date      	14-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"

#include <FreeRTOS.h>
#include <queue.h>
#include <semphr.h>

#include "resourceGuard.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
/* ----------------------- Public Functions -------------------------------*/
/* ----------------------- Private Functions ------------------------------*/

void  rgDriverConstructor(rgDriver_t* driver)
{
	vSemaphoreCreateBinary(driver->registerSemaphore);
	vSemaphoreCreateBinary(driver->ownershipSemaphore);
	vSemaphoreCreateBinary(driver->masterAccessSemaphore);
	driver->registeredMaster = 0;
	driver->releaseCallback  = 0;
}


bool rgRegisterNewMaster(rgDriver_t* driver, void* master, rgReleaseClb_t releaseCallback, TickType_t ticksToWait)
{
	rgReleaseClb_t 	loc_releaseCallback;

	//sanity checks
	if(0 == releaseCallback) return false;
	if(0 == driver) return false;
	if(0 == master) return false;

	//try to get exclusive access to this code
	if(pdFALSE == xSemaphoreTake(driver->registerSemaphore, ticksToWait))
		return false;

	//accessing master fields, make it secure in case master is in process of releasing ownership
	if(pdFALSE == xSemaphoreTake(driver->masterAccessSemaphore, ticksToWait))
		goto err;

	//make local copy of the master callback field, so the callback is still valid, even if master
	//releases its ownership while executing this function, and that access to master fields stays
	//always exclusive
	loc_releaseCallback  = driver->releaseCallback;

	//make it possible to access master fields from rgUnregisterMaster()
	xSemaphoreGive(driver->masterAccessSemaphore);

	//check if there is already a master registered
	if(pdFALSE == xSemaphoreTake(driver->ownershipSemaphore, 0))
	{
		//there is already a master registered, notify him to release its ownership
		if(pdFALSE == loc_releaseCallback(ticksToWait))
			goto err;

		//wait for the master to get released
		if(pdFALSE == xSemaphoreTake(driver->ownershipSemaphore, ticksToWait))
			goto err;
	}

	//at this stage no code will be able to access master fields,
	//therefore no need to take driver->masterAccessSemaphore

	//there is no master registered at this stage, therefore register new one
	driver->registeredMaster = master;
	driver->releaseCallback  = releaseCallback;

	//release access to this function
	xSemaphoreGive(driver->registerSemaphore);

	return true;


err:
	xSemaphoreGive(driver->registerSemaphore);
	return false;

}


void  rgUnregisterMaster(rgDriver_t* driver, void* master)
{
	xSemaphoreTake(driver->masterAccessSemaphore, MAX_DELAY);

	//check if a valid master tries to unregister
	if(driver->registeredMaster != master)
	{
		xSemaphoreGive(driver->masterAccessSemaphore);
		return;
	}

	//unregister the master
	driver->registeredMaster = 0;
	driver->releaseCallback = 0;

	xSemaphoreGive(driver->ownershipSemaphore);

	xSemaphoreGive(driver->masterAccessSemaphore);
}


bool rgCheckMasterHandle(rgDriver_t* driver, void* master)
{
	bool status = false;

	xSemaphoreTake(driver->masterAccessSemaphore, MAX_DELAY);

	if(driver->registeredMaster == master)
		status = true;

	xSemaphoreGive(driver->masterAccessSemaphore);

	return status;
}


