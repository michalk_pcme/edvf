//**************************************************************************
//
// Date             21.09.2016
//
// Product          Low Cost Controller
//
// Or Module
//
// Details          Module has a software implementation of CRC-32 (0x04C11DB7)
//                  calculations.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************
#ifndef _CRYPTOGRAPHY_H_
#define _CRYPTOGRAPHY_H_


/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void initCrcModule(void);
void deInitCrcModule(void);
uint32_t softCrc(const uint8_t*, uint32_t);

uint16_t getOptionCode(uint32_t, uint8_t, uint32_t);


#endif /* _CRYPTOGRAPHY_H_ */
