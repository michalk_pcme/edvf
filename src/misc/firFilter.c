//**************************************************************************
//
// Date				08.12.2017
//
// Product			Electrodynamic sensor
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include <string.h>
#include <math.h>

/* ----------------------- Application includes ---------------------------*/
#include "firFilter.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
/* ----------------------- Global Functions -------------------------------*/

void firPreSetDriver(firDriver_t* driver)
{
	if(0 == driver) return;

	driver->buffer.samples = 0;
}


void firReSetDriver(firDriver_t* driver)
{
	if(0 == driver) return;

	if(0 != driver->buffer.samples)
		safeFree(driver->buffer.samples);
}


bool firConfigureDriver(firDriver_t* driver, firConfig_t* newConfig)
{
	if(0 == driver) 	return false;
	if(0 == newConfig) 	return false;

	//configure buffer
	driver->buffer.samples = safeMalloc(newConfig->nbrOfCoefs * sizeof(float));
	if(0 == driver->buffer.samples) return false;

	memset(driver->buffer.samples, 0, newConfig->nbrOfCoefs * sizeof(float));

	driver->buffer.nextSamplePos = 0;
	driver->buffer.size = newConfig->nbrOfCoefs;

	//copy configuration details
	driver->config = *newConfig;

	//initialise other fields of a driver
	driver->result = 0;

	return true;
}


void firAddNewSample(firDriver_t* driver, float sample)
{
	int i;
	float * bufSample;
	float * bufStart;
	float * bufEnd;

	//add new sample to the buffer
	driver->buffer.samples[driver->buffer.nextSamplePos] = sample;
	bufSample = &driver->buffer.samples[driver->buffer.nextSamplePos]; 	//capture the position of the latest sample before counters update
	driver->buffer.nextSamplePos++;
	if(driver->buffer.nextSamplePos == driver->buffer.size)
		driver->buffer.nextSamplePos = 0;

	//calculate new filter value
	driver->result = 0;
	bufStart  = &driver->buffer.samples[0];
	bufEnd 	  = &driver->buffer.samples[driver->buffer.size-1];

	for(i=driver->config.nbrOfCoefs-1; i>=0; i--)
	{
		driver->result += driver->config.firCoefs[i] * *bufSample;

		bufSample--;
		if(bufSample < bufStart)
		{
			bufSample = bufEnd;
		}
	}
}


float firGetFIRValue(firDriver_t* driver)
{
	return driver->result;
}



/* ----------------------- Local  Functions -------------------------------*/









