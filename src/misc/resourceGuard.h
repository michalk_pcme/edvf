/**
  ******************************************************************************
  * @file      	resourceGuard.h
  * @author    	Michal Kalbarczyk
  * @date      	14-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


#ifndef MISC_RESOURCEGUARD_H_
#define MISC_RESOURCEGUARD_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"

#include <FreeRTOS.h>
#include <queue.h>
#include <semphr.h>


/* ----------------------- Public Defines ---------------------------------*/
/* ----------------------- Public Type Definitions ------------------------*/

/**
 * @brief Callback called to notify current owner to release its ownership
 * 		  over the resource.
 */
typedef BaseType_t(*rgReleaseClb_t)(uint32_t ticksToWait);


typedef struct
{
	SemaphoreHandle_t registerSemaphore;
	SemaphoreHandle_t ownershipSemaphore;
	SemaphoreHandle_t masterAccessSemaphore;

	void* registeredMaster;
	rgReleaseClb_t releaseCallback;
}rgDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void rgDriverConstructor(rgDriver_t* driver);
bool rgRegisterNewMaster(rgDriver_t* driver, void* master, rgReleaseClb_t releaseCallback, TickType_t ticksToWait);
void rgUnregisterMaster(rgDriver_t* driver, void* master);
bool rgCheckMasterHandle(rgDriver_t* driver, void* master);


#endif /* MISC_RESOURCEGUARD_H_ */
