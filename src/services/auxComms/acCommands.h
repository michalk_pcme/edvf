/**
  ******************************************************************************
  * @file      	acCommands.h
  * @author    	Michal Kalbarczyk
  * @date      	11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	Simple serial protocol.
*/


#ifndef _AC_COMMANDS_H_
#define _AC_COMMANDS_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"

//#include "acPort.h"


/* ----------------------- Public Defines ---------------------------------*/
/**Defines number of recognised commands*/
#define COMMANDS_AMOUNT 19


/* ----------------------- Public Type Definitions ------------------------*/
typedef enum{
    //protocol specific IDs from 0 to 9 are reserved
    //DO NOT USE ANY OF THOSE COMMAND IDS in code
    COMM_NOTHING 					= 0,

    COMM_TRANSMISSION_ERROR  		= 1,
    COMM_UNKNOWN_COMMAND 	 		= 2,

    //USER'S COMMAND ID SPACE
    //those IDs can be used in a code
    COMM_TEMPLATE 					= 10,
    
	//RMS algorithm check
	COMM_START_WRMS_CALCULATIONS	= 11,
	COMM_WRMS_DATA					= 12,
	COMM_FINISHED_WRMS_CALCULATIONS	= 13,

	//FIR algorithm check
	COMM_START_FIR_CALCULATIONS		= 21,
	COMM_FIR_DATA					= 22,
	COMM_FINISHED_FIR_CALCULATIONS	= 23,

	//ADC driver
	COMM_ADC_SET_REGISTERS			= 30,
	COMM_ADC_REGISTERS_SET			= 31,
	COMM_ADC_REGISTERS_NOT_SET		= 32,
	COMM_ADC_READ_REGISTERS			= 33,
	COMM_ADC_REGISTERS_DATA			= 34,
	COMM_ADC_REGISTERS_READ_ERROR	= 35,
	COMM_ADC_START_SENDING_ADC_SAMPLES	= 36,
	COMM_ADC_STOP_SENDING_ADC_SAMPLES	= 37,
	COMM_ADC_SAMPLE_DATA			= 38,
	COMM_ADC_SEND_COMMAND			= 39,
	COMM_ADC_COMMAND_SENT			= 40,
	COMM_ADC_COMMAND_NOT_RECOGNISED = 41,

	//GAIN SWITCHES
	COMM_GS_READ_SWITCHES_STATES	= 50,
	COMM_GS_SWITCHES_STATES_DATA	= 51,
	COMM_GS_WRITE_SWITCHES_STATES	= 52,
	COMM_GS_SWITCHES_SET			= 53,

	//DAC driver
	COMM_DD_READ_SETTINGS			= 60,
	COMM_DD_SETTINGS_DATA			= 61,
	COMM_DD_START_DAC				= 62,
	COMM_DD_STOP_DAC				= 63,
	COMM_DD_DAC_STARTED				= 64,
	COMM_DD_DAC_STOPPED				= 65,

	//regular measurements
	COMM_RM_START_MODULE			= 70,
	COMM_RM_MODULE_STARTED			= 71,
	COMM_RM_STOP_MODULE				= 72,
	COMM_RM_MODULE_STOPPED			= 73,
	COMM_RM_START_SENDING_MEAS_DATA	= 74,
	COMM_RM_MEAS_DATA				= 75,
	COMM_RM_STOP_SENDING_MEAS_DATA	= 76,
	COMM_RM_SET_RMS_PARAMETERS		= 77,
	COMM_RM_RMS_PARAMETERS_SET		= 78,
	COMM_RM_ZERO_CROSSINGS_DATA		= 79

}commandsIds;


typedef struct
{
    uint32_t  valA;
    uint8_t   valB;
    float     valC;
}__attribute__ ((packed)) templatePar_t;


typedef struct
{
	float data;
}__attribute__ ((packed)) wrmsDataParams_t;


typedef struct
{
	float data;
}__attribute__ ((packed)) firDataParams_t;



/** @brief	Specifies command structure with its related function. */
typedef struct
{
	commandsIds bin_alias;
	void (*function_ptr)(uint8_t*);
}Command_Stc;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
bool accTriggerCommand(commandsIds cmdId, uint8_t* paramPtr);


/* ----------------------- Global variables -------------------------------*/
//extern Command_Stc USART_Commands[COMMANDS_AMOUNT];


#endif /* _AC_COMMANDS_H_ */

