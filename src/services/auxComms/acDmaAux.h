/**
  ******************************************************************************
  * @file      	acDmaAux.h
  * @author    	Michal Kalbarczyk
  * @date      	09-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


#ifndef SERVICES_AUXCOMMS_ACDMAAUX_H_
#define SERVICES_AUXCOMMS_ACDMAAUX_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <semphr.h>
#include <queue.h>

#include "acCommands.h"


/* ----------------------- Public Defines ---------------------------------*/
#define AC_AUX_DMA_TASK_SIZE    		1024
#define AC_AUX_DMA_TX_QUEUE_SIZE		8
#define AC_AUX_DMA_RX_QUEUE_SIZE		8

#define AC_AUX_DMA_MAX_MESSAGE_LENGTH	128

#define AC_AUX_DMA_RX_BUF_SIZE 			128

#define AC_AUX_DMA_TX_QUEUE_SIZE		8

#define AC_AUX_DMA_PROTOCOL_OVERHEAD	4 //bytes (start + length + command + CRC)


/* ----------------------- Public Type Definitions ------------------------*/

typedef struct
{
	uint8_t* paramPtr;				/*!< Points to beginning of the memory that should be sent as parameter*/
	uint8_t  paramSize;				/*!< Size of parameters in bytes*/
	commandsIds command;			/*!< Specifies command for binary transmission that should be
									 	 embedded into message*/
}acMessage_t;


typedef struct
{
	uint8_t* buffer;
	uint16_t size;
}acTxQueueItem_t;


typedef struct
{
	uint8_t none;
}acRxQueueItem_t;


typedef struct
{
	uint8_t none;
}acTransmitter_t;


typedef struct
{
	uint8_t none;
}acReceiver_t;


typedef struct
{
	QueueHandle_t txQueue;
	QueueHandle_t rxQueue;
	SemaphoreHandle_t txFinishedSemaphore;
	SemaphoreHandle_t rxFinishedSemaphore;

	UART_HandleTypeDef huart;
	DMA_HandleTypeDef  hdma_usart1_rx;
	DMA_HandleTypeDef  hdma_usart1_tx;

	uint8_t* activeDmaBuffer;						/*!< pointer to the active RX buffer*/
	uint8_t dmaRxBuffer1[AC_AUX_DMA_RX_BUF_SIZE];
	uint8_t dmaRxBuffer2[AC_AUX_DMA_RX_BUF_SIZE];

	acTxQueueItem_t activeTxItem;
}acDmaAuxDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void acInitAuxDmaComms(void);
void acDmaAuxTxTask(void);
void acDmaAuxRxTask(void);

void acDmaAuxTxIsr(void);
void acDmaAuxRxIsr(void);
void acDmaAuxUartIsr(void);

void acAuxDma_HAL_UART_MspInit(UART_HandleTypeDef* huart);
void acAuxDma_HAL_UART_MspDeInit(UART_HandleTypeDef* huart);

bool addShortMessageToQueue(commandsIds cmd, uint32_t ticksToWait);
bool addMessageToQueue(acMessage_t * newMessage, uint32_t ticksToWait);


#ifdef CEEDLING_TEST
extern STATIC acDmaAuxDriver_t acDriver;
#endif

#endif /* SERVICES_AUXCOMMS_ACDMAAUX_H_ */
