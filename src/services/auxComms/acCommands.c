/**
  ******************************************************************************
  * @file      	acCommands.c
  * @author    	Michal Kalbarczyk
  * @date      	11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	Simple serial protocol.
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "FreeRTOS.h"

#include "acCommands.h"
//#include "acEngine.h"
//#include "acPort.h"
#include "acDmaAux.h"

#include "adcDriver.h"
#include "adcRegisters.h"

#include "gainSwitches.h"
#include "dacDriver.h"

#include "regularMeasurements.h"

#include "timestamps.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
//STATIC BaseType_t releaseAdcMasterCallback(uint32_t tickToWait);

STATIC void templateCommand(uint8_t* paramPtr);
STATIC void startWrmsCalculations(uint8_t* paramPtr);
STATIC void startFirCalculations(uint8_t* paramPtr);

STATIC void adcSetRegisters(uint8_t* paramPtr);
STATIC void adcReadRegisters(uint8_t* paramPtr);
STATIC void adcStartSendingAdcSamples(uint8_t* paramPtr);
STATIC void adcStopSendingAdcSamples(uint8_t* paramPtr);
STATIC void adcSendCommand(uint8_t* paramPtr);

STATIC void commGsReadSwitchesStates(uint8_t* paramPtr);
STATIC void commGsWriteSwitchesStates(uint8_t* paramPtr);

STATIC void commDdStartDac(uint8_t* paramPtr);
STATIC void commDdStopDac(uint8_t* paramPtr);
STATIC void commDdReadSettings(uint8_t* paramPtr);

STATIC void commRmStartModule(uint8_t* paramPtr);
STATIC void commRmStopModule(uint8_t* paramPtr);
STATIC void commRmStartSendingMeasData(uint8_t* paramPtr);
STATIC void commRmStopSendingMeasData(uint8_t* paramPtr);
STATIC void commRmSetRmsParameters(uint8_t* paramPtr);


/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/

/**
 * @brief	matrix that stores commands numbers for binary protocol,
 *  		names for text protocol and commands function pointers.
 * 
 * @warning REMEMBER TO CHANGE COMMANDS_AMOUNT TO MATCH NUMBER OF
 *          COMMANDS IN MATRIX!
 */
STATIC Command_Stc USART_Commands[COMMANDS_AMOUNT] = {
	{COMM_TEMPLATE,                     	&templateCommand},
	{COMM_START_WRMS_CALCULATIONS,      	&startWrmsCalculations},
	{COMM_START_FIR_CALCULATIONS,       	&startFirCalculations},

	{COMM_ADC_SET_REGISTERS,       			&adcSetRegisters},
	{COMM_ADC_READ_REGISTERS,       		&adcReadRegisters},
	{COMM_ADC_START_SENDING_ADC_SAMPLES,	&adcStartSendingAdcSamples},
	{COMM_ADC_STOP_SENDING_ADC_SAMPLES,		&adcStopSendingAdcSamples},
	{COMM_ADC_SEND_COMMAND,					&adcSendCommand},

	{COMM_GS_READ_SWITCHES_STATES,			&commGsReadSwitchesStates},
	{COMM_GS_WRITE_SWITCHES_STATES,			&commGsWriteSwitchesStates},

	{COMM_DD_START_DAC,						&commDdStartDac},
	{COMM_DD_STOP_DAC,						&commDdStopDac},
	{COMM_DD_READ_SETTINGS,					&commDdReadSettings},

	{COMM_RM_START_MODULE,					&commRmStartModule},
	{COMM_RM_STOP_MODULE,					&commRmStopModule},
	{COMM_RM_START_SENDING_MEAS_DATA,		&commRmStartSendingMeasData},
	{COMM_RM_STOP_SENDING_MEAS_DATA,		&commRmStopSendingMeasData},
	{COMM_RM_SET_RMS_PARAMETERS,			&commRmSetRmsParameters}
};


/* ----------------------- Public Functions -------------------------------*/
bool accTriggerCommand(commandsIds cmdId, uint8_t* paramPtr)
{
	int i;
	bool commandTriggered;

	commandTriggered = false;
	for(i=0; i<COMMANDS_AMOUNT; i++)
	{
		if(cmdId == USART_Commands[i].bin_alias)
		{
			//found the command, therefore trigger it
			USART_Commands[i].function_ptr(paramPtr);

			commandTriggered = true;
		}
	}

	return commandTriggered;
}


/* ----------------------- Private Functions ------------------------------*/

/**
 * @brief Dummy function to release ADC Driver from AUX master whenever AUX
 * 		  is interacting with ADC Driver interface.
 */
#if 0
STATIC BaseType_t releaseAdcMasterCallback(uint32_t tickToWait)
{
	(void)(tickToWait);
	return pdTRUE;
}
#endif


/**
  * @brief	Template command.
  * @param  paramPtr: 	 pointer to command parameters in serial communication
  * 					 receive buffer.
  */
STATIC void templateCommand(uint8_t* paramPtr)
{
    volatile templatePar_t * temp = (templatePar_t*)paramPtr;
    
    //suppress compiler's warning
    (void)(temp);
    
    //generate own template message as a reply
    templatePar_t templateVal;
    templateVal.valA = 0xDEADBEEF;
    templateVal.valB = 0xAB;
    templateVal.valC = 10.5;

    acMessage_t item;
    item.paramPtr  = (unsigned char*)&templateVal;
    item.paramSize = sizeof(templatePar_t);
    item.command   = COMM_TEMPLATE;
    addMessageToQueue(&item, MAX_DELAY);
}


STATIC void startWrmsCalculations(uint8_t* paramPtr)
{
	(void)(paramPtr);

	//startWrsmUartTest();
	//startWrsmOptimisedUartTest();
	//startFakeWrsmUartTest();
}


STATIC void startFirCalculations(uint8_t* paramPtr)
{
	(void)(paramPtr);

	//startFirUartTest();
}


STATIC void adcSetRegisters(uint8_t* paramPtr)
{
	adcrRegisters_t* registers;

	registers = (adcrRegisters_t*)paramPtr;

	//acquire access to ADC driver
	//adcdRegisterMasterOverAdcDriver((void*)&token, &releaseAdcMasterCallback, MAX_DELAY);

	//make an inquiry
	adcdWriteAdcRegistersFromAux(0, registers, MAX_DELAY);

	startCollectingTimestamps(0);

	//return ADC access
	//adcdUnRegisterMasterOverAdcDriver((void*)&token);
}


STATIC void adcReadRegisters(uint8_t* paramPtr)
{
	(void)(paramPtr);

	//acquire access to ADC driver
	//adcdRegisterMasterOverAdcDriver((void*)&token, &releaseAdcMasterCallback, MAX_DELAY);

	adcdReadAdcRegistersToAux(0, MAX_DELAY);

	//return ADC access
	//adcdUnRegisterMasterOverAdcDriver((void*)&token);
}


STATIC void adcStartSendingAdcSamples(uint8_t* paramPtr)
{
	(void)(paramPtr);

	//acquire access to ADC driver
	//adcdRegisterMasterOverAdcDriver((void*)&token, &releaseAdcMasterCallback, MAX_DELAY);

	adcdEnableSendingAdcSamplesToAux(0, true, MAX_DELAY);

	//return ADC access
	//adcdUnRegisterMasterOverAdcDriver((void*)&token);
}


STATIC void adcStopSendingAdcSamples(uint8_t* paramPtr)
{
	(void)(paramPtr);

	//acquire access to ADC driver
	//adcdRegisterMasterOverAdcDriver((void*)&token, &releaseAdcMasterCallback, MAX_DELAY);

	adcdEnableSendingAdcSamplesToAux(0, false, MAX_DELAY);

	//return ADC access
	//adcdUnRegisterMasterOverAdcDriver((void*)&token);
}


STATIC void adcSendCommand(uint8_t* paramPtr)
{
	adcrAdcCmdPar_t* params;

	params = (adcrAdcCmdPar_t*)paramPtr;

	//acquire access to ADC driver
	//adcdRegisterMasterOverAdcDriver((void*)&token, &releaseAdcMasterCallback, MAX_DELAY);

	adcdSendAdcCommandFromAux(0, params->command, MAX_DELAY);

	//return ADC access
	//adcdUnRegisterMasterOverAdcDriver((void*)&token);
}


STATIC void commGsReadSwitchesStates(uint8_t* paramPtr)
{
	gsDataParams_t returnParams;
	uint16_t switchesStates;

	(void)(paramPtr);

	//retrieve states of switches
	gsGetSwitches(&switchesStates);

	//assemble return message parameters
	returnParams.data = switchesStates;

	//send a reply
	acMessage_t item;
	item.paramPtr  = (unsigned char*)&returnParams;
	item.paramSize = sizeof(gsDataParams_t);
	item.command   = COMM_GS_SWITCHES_STATES_DATA;

	addMessageToQueue(&item, MAX_DELAY);
}


STATIC void commGsWriteSwitchesStates(uint8_t* paramPtr)
{
	gsDataParams_t* switchesStates;

	switchesStates = (gsDataParams_t*)paramPtr;

	gsSetSwitchesAux(switchesStates->data);

	addShortMessageToQueue(COMM_GS_SWITCHES_SET, MAX_DELAY);
}


STATIC void commDdStartDac(uint8_t* paramPtr)
{
	(void)(paramPtr);

#if 0
	ddConfig_t config;

	config.mode = DD_MODE_SINE_WAVE;
	config.params.sineWave.frequency	= 1000;
	config.params.sineWave.dcOffset 	= 1000;
	config.params.sineWave.p_pAmplitude = 2000;
	ddStartDac((ddConfig_t*)&config);
#else
	ddStartDacAux((ddConfig_t*)paramPtr);
#endif
}


STATIC void commDdStopDac(uint8_t* paramPtr)
{
	(void)(paramPtr);

	ddStopDacAux();
}


STATIC void commDdReadSettings(uint8_t* paramPtr)
{
	(void)(paramPtr);

	ddSendSettingsToAux();
}




STATIC void commRmStartModule(uint8_t* paramPtr)
{
	(void)(paramPtr);

	rmStartModule(MAX_DELAY);
}


STATIC void commRmStopModule(uint8_t* paramPtr)
{
	(void)(paramPtr);

	rmStopModule(MAX_DELAY);
}


STATIC void commRmStartSendingMeasData(uint8_t* paramPtr)
{
	(void)(paramPtr);

	rmStartSendingMeasData(MAX_DELAY);
}


STATIC void commRmStopSendingMeasData(uint8_t* paramPtr)
{
	(void)(paramPtr);

	rmStopSendingMeasData(MAX_DELAY);
}


STATIC void commRmSetRmsParameters(uint8_t* paramPtr)
{
	rmSetRmsParameters((rmRmsParams_t*)paramPtr, MAX_DELAY);
}


