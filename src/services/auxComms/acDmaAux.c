/**
  ******************************************************************************
  * @file      	acDmaAux.c
  * @author    	Michal Kalbarczyk
  * @date      	09-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <semphr.h>
#include <queue.h>
#include <task.h>

#include <string.h>

#include "acDmaAux.h"
#include "acCommands.h"

#include "stm32l4xx_hal_uart.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
STATIC acDmaAuxDriver_t acDriver;


/* ----------------------- Private Prototypes -----------------------------*/
STATIC void acAuxDmaHardwareInit(void);
STATIC void sendCommandUnknownMsg(void);
STATIC void sendTransmissionErrorMsg(void);
STATIC void acTransmitMessageOverDma(void);
STATIC void acProcessFinishedDmaRx(void);
STATIC bool acDmaAuxProcessRxBuffer(uint8_t* buffer, uint16_t bufferSize);
STATIC void acDmaAuxConfirmRx(void);


/* ----------------------- Public Functions -------------------------------*/
void acInitAuxDmaComms(void)
{
    //initialise tasks
    xTaskCreate((TaskFunction_t) acDmaAuxTxTask,
                "xxx",
                AC_AUX_DMA_TASK_SIZE, NULL, AUX_DMA_COMMS_TASK_PRIORITY, NULL);

    xTaskCreate((TaskFunction_t) acDmaAuxRxTask,
                "xxx",
                AC_AUX_DMA_TASK_SIZE, NULL, AUX_DMA_COMMS_TASK_PRIORITY, NULL);

    acDriver.txQueue = xQueueCreate(AC_AUX_DMA_TX_QUEUE_SIZE, sizeof(acTxQueueItem_t));
    acDriver.rxQueue = xQueueCreate(AC_AUX_DMA_RX_QUEUE_SIZE, sizeof(acRxQueueItem_t));

    vSemaphoreCreateBinary(acDriver.txFinishedSemaphore);
    xSemaphoreTake(acDriver.txFinishedSemaphore, MAX_DELAY);

    vSemaphoreCreateBinary(acDriver.rxFinishedSemaphore);
    xSemaphoreTake(acDriver.rxFinishedSemaphore, MAX_DELAY);

    acAuxDmaHardwareInit();

    acDriver.activeDmaBuffer = &acDriver.dmaRxBuffer1[0];
}


void acDmaAuxTxTask(void)
{
	while(1)
	{
		if(pdPASS == xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		{
			acTransmitMessageOverDma();
		}
	}
}


void acDmaAuxRxTask(void)
{
	//set up DMA for reception of UART data
	HAL_UART_Receive_DMA(&acDriver.huart, acDriver.activeDmaBuffer, AC_AUX_DMA_RX_BUF_SIZE);

	while(1)
	{
		if(pdTRUE == xSemaphoreTake(acDriver.rxFinishedSemaphore, MAX_DELAY))
		{
			acProcessFinishedDmaRx();
		}
	}
}


void acDmaAuxTxIsr(void)
{
	HAL_DMA_IRQHandler(&acDriver.hdma_usart1_tx);
}


void acDmaAuxRxIsr(void)
{
	HAL_DMA_IRQHandler(&acDriver.hdma_usart1_rx);
}


void acDmaAuxUartIsr(void)
{
	uint32_t isrflags = READ_REG(acDriver.huart.Instance->ISR);

	//check for IDLE flag (idle terminates RX transmission)
	if(isrflags & USART_ISR_IDLE)
	{
		//IDLE detected
		__HAL_UART_CLEAR_FLAG(&acDriver.huart, USART_ICR_IDLECF);

		acDmaAuxConfirmRx();
	}

	HAL_UART_IRQHandler(&acDriver.huart);
}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	(void)(huart);

	portBASE_TYPE   xHigherPriorityTaskWoken;

	xSemaphoreGiveFromISR(acDriver.txFinishedSemaphore, &xHigherPriorityTaskWoken);

#ifdef CEEDLING_TEST
	xHigherPriorityTaskWoken = 0;
#endif

	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	(void)(huart);

	acDmaAuxConfirmRx();
}


void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	(void)(huart);

	acDmaAuxConfirmRx();
}


bool addShortMessageToQueue(commandsIds cmd, uint32_t ticksToWait)
{
	acMessage_t newMessage;

	newMessage.command   = cmd;
	newMessage.paramPtr  = 0;
	newMessage.paramSize = 0;

	return addMessageToQueue(&newMessage, ticksToWait);
}


bool addMessageToQueue(acMessage_t * newMessage, uint32_t ticksToWait)
{
	acTxQueueItem_t txItem;
	uint8_t crcVal;
	int i;

	memset(&txItem, 0, sizeof(acTxQueueItem_t));

	if((newMessage->paramSize + AC_AUX_DMA_PROTOCOL_OVERHEAD) > AC_AUX_DMA_MAX_MESSAGE_LENGTH)
		goto err;

	//assign memory for new DMA buffer frame
	txItem.size 	= (uint16_t)(newMessage->paramSize + AC_AUX_DMA_PROTOCOL_OVERHEAD);
	txItem.buffer 	= safeMalloc((uint32_t)(newMessage->paramSize + AC_AUX_DMA_PROTOCOL_OVERHEAD));
	if(0 == txItem.buffer)
		goto err;

	//assemble the DMA buffer frame
	txItem.buffer[0] = 'B';										//start bit
	txItem.buffer[1] = (uint8_t)(newMessage->paramSize + 1); 	//length = params + command
	txItem.buffer[2] = (uint8_t)newMessage->command;			//command

	memcpy(&txItem.buffer[3], newMessage->paramPtr, newMessage->paramSize);	//parameters

	//calculate CRC value
	crcVal = 0;
	for(i=0; i<txItem.size-1; i++)
	{
		crcVal = (uint8_t)(crcVal ^ txItem.buffer[i]);
	}

	txItem.buffer[txItem.size-1] = crcVal;						//CRC value

	//push the TX item into queue
	if(pdFALSE == xQueueSend(acDriver.txQueue, &txItem, ticksToWait))
		goto err;

	return true;


err:
	if(0 != txItem.buffer)
		safeFree(txItem.buffer);

	return false;
}


/**
 * @brief Hardware initialisation that has been moved from st32l4xx_hal_msp.c
 * 		  file for the code clarity. This function gets called by HAL layer
 * 		  during peripheral de-/initialisation.
 */
void acAuxDma_HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	DMA_HandleTypeDef*  hdma_usart1_rx;
	DMA_HandleTypeDef*  hdma_usart1_tx;


	if(huart->Instance != USART1)
		return;

	/* Peripheral clock enable */
	__HAL_RCC_USART1_CLK_ENABLE();

	/**USART1 GPIO Configuration
	PB6     ------> USART1_TX
	PB7     ------> USART1_RX
	*/
	GPIO_InitStruct.Pin 	= GPIO_PIN_6|GPIO_PIN_7;
	GPIO_InitStruct.Mode 	= GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull 	= GPIO_PULLUP;
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USART1 DMA Init */
	/* USART1_RX Init */
	hdma_usart1_rx = &acDriver.hdma_usart1_rx;
	hdma_usart1_rx->Instance 				 = DMA1_Channel5;
	hdma_usart1_rx->Init.Request 			 = DMA_REQUEST_2;
	hdma_usart1_rx->Init.Direction 			 = DMA_PERIPH_TO_MEMORY;
	hdma_usart1_rx->Init.PeriphInc 			 = DMA_PINC_DISABLE;
	hdma_usart1_rx->Init.MemInc 			 = DMA_MINC_ENABLE;
	hdma_usart1_rx->Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_usart1_rx->Init.MemDataAlignment 	 = DMA_MDATAALIGN_BYTE;
	hdma_usart1_rx->Init.Mode 				 = DMA_NORMAL;
	hdma_usart1_rx->Init.Priority 			 = DMA_PRIORITY_LOW;
	if (HAL_DMA_Init(hdma_usart1_rx) != HAL_OK)
	 	errorLoop();

	__HAL_LINKDMA(huart,hdmarx,*hdma_usart1_rx);

	/* USART1_TX Init */
	hdma_usart1_tx = &acDriver.hdma_usart1_tx;
	hdma_usart1_tx->Instance 				 = DMA1_Channel4;
	hdma_usart1_tx->Init.Request 			 = DMA_REQUEST_2;
	hdma_usart1_tx->Init.Direction 			 = DMA_MEMORY_TO_PERIPH;
	hdma_usart1_tx->Init.PeriphInc 			 = DMA_PINC_DISABLE;
	hdma_usart1_tx->Init.MemInc 			 = DMA_MINC_ENABLE;
	hdma_usart1_tx->Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_usart1_tx->Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
	hdma_usart1_tx->Init.Mode 				 = DMA_NORMAL;
	hdma_usart1_tx->Init.Priority 			 = DMA_PRIORITY_LOW;
	if (HAL_DMA_Init(hdma_usart1_tx) != HAL_OK)
	  	errorLoop();

	__HAL_LINKDMA(huart,hdmatx,*hdma_usart1_tx);

	/* USART1 interrupt Init */
	HAL_NVIC_SetPriority(USART1_IRQn, UART1_ISR_PRIORITY, 0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);

	/* DMA1_Channel4_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, UART1_ISR_PRIORITY, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);

	/* DMA1_Channel5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, UART1_ISR_PRIORITY, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
}


/**
 * @brief Hardware initialisation that has been moved from st32l4xx_hal_msp.c
 * 		  file for the code clarity. This function gets called by HAL layer
 * 		  during peripheral de-/initialisation.
 */
void acAuxDma_HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{
	if(huart->Instance != USART1)
		return;

    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();

    /**USART1 GPIO Configuration
    PB6     ------> USART1_TX
    PB7     ------> USART1_RX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6|GPIO_PIN_7);

    /* USART1 DMA DeInit */
    HAL_DMA_DeInit(huart->hdmarx);
    HAL_DMA_DeInit(huart->hdmatx);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
}


/* ----------------------- Private Functions ------------------------------*/
STATIC void sendCommandUnknownMsg(void)
{
	acMessage_t Item;

	Item.command  = COMM_UNKNOWN_COMMAND;
	Item.paramPtr = 0;
	Item.paramSize = 0;

	addMessageToQueue(&Item, 0);
}


STATIC void sendTransmissionErrorMsg(void)
{
	acMessage_t Item;

	Item.command  = COMM_TRANSMISSION_ERROR;
	Item.paramPtr = 0;
	Item.paramSize = 0;

	addMessageToQueue(&Item, 0);
}


STATIC void acAuxDmaHardwareInit(void)
{
	UART_HandleTypeDef* huart = &acDriver.huart;

	huart->Instance 					= USART1;
	huart->Init.BaudRate 				= 256000; //115200;
	huart->Init.WordLength 				= UART_WORDLENGTH_8B;
	huart->Init.StopBits 				= UART_STOPBITS_1;
	huart->Init.Parity 					= UART_PARITY_NONE;
	huart->Init.Mode 					= UART_MODE_TX_RX;
	huart->Init.HwFlowCtl 				= UART_HWCONTROL_NONE;
	huart->Init.OverSampling 			= UART_OVERSAMPLING_16;
	huart->Init.OneBitSampling 			= UART_ONE_BIT_SAMPLE_DISABLE;
	huart->AdvancedInit.AdvFeatureInit 	= UART_ADVFEATURE_NO_INIT;

	if (HAL_UART_Init(huart) != HAL_OK)
		errorLoop();

	//enable IDLE interrupt
	SET_BIT(huart->Instance->CR1, USART_CR1_IDLEIE);
	__HAL_UART_CLEAR_FLAG(huart, USART_ICR_IDLECF);
}


STATIC void acTransmitMessageOverDma(void)
{
	if(HAL_OK == HAL_UART_Transmit_DMA(&acDriver.huart, acDriver.activeTxItem.buffer, acDriver.activeTxItem.size))
	{
		//wait for DMA to transfer all the data
		xSemaphoreTake(acDriver.txFinishedSemaphore, MAX_DELAY);

		//release buffer's memory
		safeFree(acDriver.activeTxItem.buffer);
	}
}


STATIC void acProcessFinishedDmaRx(void)
{
	uint8_t* rxBuffer;

	//stop the RX DMA
	HAL_UART_AbortReceive(&acDriver.huart);

	//get handle to the buffer with received data
	rxBuffer = acDriver.activeDmaBuffer;

	//change the buffer to another one
	if(acDriver.activeDmaBuffer == &acDriver.dmaRxBuffer1[0])
		acDriver.activeDmaBuffer = &acDriver.dmaRxBuffer2[0];
	else
		acDriver.activeDmaBuffer = &acDriver.dmaRxBuffer1[0];

	//set up the RX DMA for next transaction
	HAL_UART_Receive_DMA(&acDriver.huart, acDriver.activeDmaBuffer, AC_AUX_DMA_RX_BUF_SIZE);

	//process received data
	acDmaAuxProcessRxBuffer(rxBuffer, AC_AUX_DMA_RX_BUF_SIZE);
}


STATIC bool acDmaAuxProcessRxBuffer(uint8_t* buffer, uint16_t bufferSize)
{
	int i;
	uint8_t length;
	uint8_t* params;
	uint8_t cmd;
	uint8_t crcVal;
	uint8_t temp;

	if(0 == buffer)		return false;
	if(0 == bufferSize)	return false;

	//check starting byte
	if('B' != buffer[0]) goto err;

	//get other fields of the message
	length 	= buffer[1];
	cmd 	= buffer[2];
	params 	= &buffer[3];
	crcVal	= buffer[length+2];

	//if message too long, then discard
	if((length+3) > AC_AUX_DMA_MAX_MESSAGE_LENGTH) goto err;
	if((length+3) > bufferSize) goto err;

	//calculate CRC value
	temp = 0;
	for(i=0; i<length+2; i++)
	{
		temp = (uint8_t)(temp ^ buffer[i]);
	}

	//check if CRC value is correct
	if(temp != crcVal)
		goto err;

	//try to execute the command
	if(false == accTriggerCommand(cmd, params))
	{
		//unrecognised command ID
		sendCommandUnknownMsg();

		return false;
	}

	return true;

err:
	sendTransmissionErrorMsg();
	return false;
}


STATIC void acDmaAuxConfirmRx(void)
{
	portBASE_TYPE   xHigherPriorityTaskWoken;

	xSemaphoreGiveFromISR(acDriver.rxFinishedSemaphore, &xHigherPriorityTaskWoken);

#ifdef CEEDLING_TEST
	xHigherPriorityTaskWoken = 0;
#endif

	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}
