/**
  ******************************************************************************
  * @file      	timestamps.h
  * @author    	Michal Kalbarczyk
  * @date      	11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	Build around TIM2 peripheral. TIM2 gets reset with module
  * 	initialisation which can be triggered from any point in the system.
  * 	Time stamps start getting collected by triggering timestampCollectionStart()
  * 	function. From that point onwards time stamps are getting collected
  * 	with each collectTimestamp() function until collection buffer get
  * 	full. On each collection time stamp name and TIM2 counter readings
  * 	are being taken.
  *
  *		options:
  * 	DELTA	    - captures TIM2 ticks between time stamps
  * 	NAMES_ONLY - captures only names of the stamp, without time signature
  * 	USE_FILTER - captures only time stamps with names within
  * 				  FILTER_LOW - FILTER_HIGH range
*/


#ifndef _TIMESTAMPS_H_
#define _TIMESTAMPS_H_


/* ----------------------- Includes ---------------------------------------*/
#include <systemDefinitions.h>
#include "stm32l4xx_hal.h"


/* ----------------------- Public Defines ---------------------------------*/
#define STAMPS_ENABLE

//#define NAMES_ONLY
#define DELTA
//#define USE_FILTER

#define TIME_STAMPS_AMOUNT 	100

#define FILTER_LOW			10
#define FILTER_HIGH			11


/* ----------------------- Public Type Definitions ------------------------*/
/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void startCollectingTimestamps(unsigned int);
void collectTimestamp(unsigned int);
void initTimestamps(void);


#endif /* _TIMESTAMPS_H_ */
