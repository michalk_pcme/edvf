/**
  ******************************************************************************
  * @file      	timeoutTimers.h
  * @author    	Michal Kalbarczyk
  * @date      	11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	HOW TO USE:
  * 		1. Add new timersNames enum and increase NUMBER_OF_TIMEOUT_TIMERS in timeoutTimers.h
  * 		2. Set and start timer with setTimeoutTimer() function
  * 		3. Check for elapsed time flag with hasTimeoutTimerElapsed() function
*/


#ifndef TIMEOUTTIMERS_H_
#define TIMEOUTTIMERS_H_


/* ----------------------- Includes ---------------------------------------*/
#include <systemDefinitions.h>


/* ----------------------- Public Defines ---------------------------------*/
#define NUMBER_OF_TIMEOUT_TIMERS 		1


/* ----------------------- Public Type Definitions ------------------------*/
typedef enum{
	TT_RM_ZERO_CROSSING					= 0
}timersNames;


typedef struct{
	bool enabled;
	bool elapsed;
	uint32_t period;
	uint32_t counter;
}timeoutTimerStc;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void initTimeoutTimers(void);
void deInitTimeoutTimers(void);
void setTimeoutTimer(timersNames, uint32_t);
bool hasTimeoutTimerElapsed(timersNames);
void resetTimeoutTimer(timersNames);

void updateTimeoutTimers(void);


#endif /* TIMEOUTTIMERS_H_ */
