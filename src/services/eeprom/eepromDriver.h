/**
  ******************************************************************************
  * @file      	eepromDriver.h
  * @author    	Michal Kalbarczyk
  * @date      	17-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


#ifndef EEPROMDRIVER_H_
#define EEPROMDRIVER_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <queue.h>

#include "eepromComms.h"


/* ----------------------- Public Defines ---------------------------------*/
#define ED_TASK_SIZE						1024
#define ED_INQUIRY_QUEUE_SIZE				4


/* ----------------------- Public Type Definitions ------------------------*/
typedef enum
{
	ED_RELEASE_SPI_PERIPHERAL,
	ED_READ_MEMORY,
	ED_WRITE_MEMORY,
	ED_ERASE_MEMORY
}edInqTypes_t;


typedef struct{
	uint16_t none;
}releaseSpiPeriphPar_t;


typedef struct{
	uint16_t address;		/*!< starting register address from which data should be read*/
	uint16_t dataSize;		/*!< number of bytes to be read */
	uint8_t* data;			/*!< data buffer */
}readMemoryPar_t;


typedef struct{
	uint16_t address;		/*!< starting register address towards which data should be written*/
	uint16_t dataSize;		/*!< number of bytes to be written */
	uint8_t* data;			/*!< data buffer */
}writeMemoryPar_t;


typedef struct{
	uint16_t none;
}eraseMemoryPar_t;


typedef struct
{
	edInqTypes_t inqType;

	union{
		releaseSpiPeriphPar_t 	releaseSpiPeriph;
		readMemoryPar_t 		readMemory;
		writeMemoryPar_t		writeMemory;
		eraseMemoryPar_t		eraseMemory;
	}params;
}edInquiry_t;


typedef struct
{
	bool spiReady;

	SPI_TypeDef* spiInstance;
	ecDriver_t commDriver;

	QueueHandle_t inquiryQueue;
}edDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void edInitEepromDriver(void);

void edTask(void);

BaseType_t edReleaseSpiCallback(uint32_t ticksToWait);
BaseType_t edEraseMemory(uint32_t ticksToWait);
BaseType_t edReadMemory(uint16_t address, uint16_t dataSize, uint8_t* data, uint32_t ticksToWait);
BaseType_t edWriteMemory(uint16_t address, uint16_t dataSize, uint8_t* data, uint32_t ticksToWait);


#endif /* EEPROMDRIVER_H_ */
