/**
  ******************************************************************************
  * @file      	eepromDriver.c
  * @author    	Michal Kalbarczyk
  * @date      	17-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "spiPeripheral.h"
#include "eepromDriver.h"
#include "eepromComms.h"
#include "eepromMap.h"
#include "cryptography.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
static edDriver_t edDriver;


/* ----------------------- Private Prototypes -----------------------------*/
static void edProcessInquiry(edInquiry_t* inq);
static bool edAcquireConfigureSpi(void);

static bool eepromMemWriteCrcCheck(uint16_t, uint8_t*, uint16_t);
static bool eepromMemReadCrcCheck(uint16_t, uint8_t*, uint16_t);

static bool eepromMemRead(uint16_t, uint8_t*, uint16_t);

static void eepromExample(void);
static bool eepromTest(void);


/* ----------------------- Public Functions -------------------------------*/

/**
  * @brief  ???
  * @note   Should be called at the beginning of the program.
  */
void edInitEepromDriver(void){

    //initialise task
    xTaskCreate((TaskFunction_t) edTask,
                "ADC driver",
				ED_TASK_SIZE, NULL, EEPROM_DRIVER_TASK_PRIORITY, NULL);

    //initialise messaging queue
    edDriver.inquiryQueue = xQueueCreate(ED_INQUIRY_QUEUE_SIZE, sizeof(edInquiry_t));

    //initialise ADC driver
    edDriver.spiInstance = 0;
    edDriver.spiReady = false;
}


void edTask(void)
{
	edInquiry_t inq;

    while(1)
    {
        //process inquiry queue
        if(pdPASS == xQueueReceive(edDriver.inquiryQueue, &inq, 0))
        {
            edProcessInquiry(&inq);
        }

        vTaskDelay(1000);

        //edEraseMemory(0);

        uint8_t wrData = 0xAB;
        edWriteMemory(1, 1, &wrData, 0);

        //uint8_t mtx[4];
        uint8_t rdData = 0;
        edReadMemory(1, 1, &rdData, 0);

    }
}


BaseType_t edReleaseSpiCallback(uint32_t ticksToWait)
{
	edInquiry_t inq;

    inq.inqType = ED_RELEASE_SPI_PERIPHERAL;

    //add inquiry to the queue for further processing
    return xQueueSend(edDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t edEraseMemory(uint32_t ticksToWait)
{
	edInquiry_t inq;

    inq.inqType = ED_ERASE_MEMORY;

    //add inquiry to the queue for further processing
    return xQueueSend(edDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t edReadMemory(uint16_t address, uint16_t dataSize, uint8_t* data, uint32_t ticksToWait)
{
	readMemoryPar_t* par;
	edInquiry_t inq;

    inq.inqType = ED_READ_MEMORY;

    par = &inq.params.readMemory;
    par->address 	= address;
    par->dataSize 	= dataSize;
    par->data 		= data;

    //add inquiry to the queue for further processing
    return xQueueSend(edDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t edWriteMemory(uint16_t address, uint16_t dataSize, uint8_t* data, uint32_t ticksToWait)
{
	writeMemoryPar_t* par;
	edInquiry_t inq;

    inq.inqType = ED_WRITE_MEMORY;

    par = &inq.params.writeMemory;
    par->address 	= address;
    par->dataSize 	= dataSize;
    par->data 		= data;

    //add inquiry to the queue for further processing
    return xQueueSend(edDriver.inquiryQueue, &inq, ticksToWait);
}


/* ----------------------- Private Functions ------------------------------*/

/**
 * @brief ???
 */
static void edProcessInquiry(edInquiry_t* inq)
{
	switch(inq->inqType){

	    case ED_RELEASE_SPI_PERIPHERAL:

	    	//check if SPI is in possession
	       	if(0 == edDriver.spiInstance) break;

	       	//de-initialise SPI and release the instance
	       	ecDeInitEepromCommsDriver(&edDriver.commDriver);
	       	spReleaseMasterOwnership(&edDriver);

	       	//clean-up EEPROM driver
	       	edDriver.spiInstance = 0;
	       	edDriver.spiReady = false;
	        break;


	    case ED_READ_MEMORY:
	    	if(false == edAcquireConfigureSpi())
	    		break;

	    	ecReadInq_t ecReadInq;

	    	ecReadInq.address 	= inq->params.readMemory.address;
	    	ecReadInq.data 		= inq->params.readMemory.data;
	    	ecReadInq.dataSize 	= inq->params.readMemory.dataSize;

	    	ecReadMemory(&edDriver.commDriver, &ecReadInq);
	    	break;


	    case ED_WRITE_MEMORY:
	    	if(false == edAcquireConfigureSpi())
	    		break;

	    	ecWriteInq_t ecWriteInq;

	    	ecWriteInq.address 	= inq->params.writeMemory.address;
	    	ecWriteInq.data 	= inq->params.writeMemory.data;
	    	ecWriteInq.dataSize = inq->params.writeMemory.dataSize;

	        ecWriteMemory(&edDriver.commDriver, &ecWriteInq);

	    	break;


	    case ED_ERASE_MEMORY:
	    	if(false == edAcquireConfigureSpi())
	    		break;

	    	ecEraseAll(&edDriver.commDriver);
	    	break;

	    default:
	    	break;
	 }
}


static bool edAcquireConfigureSpi(void)
{
	//check if SPI driver requires configuring
	if(false == edDriver.spiReady)
	{
		//yep, it does...
		//check if SPI instance is in possession
		if(0 == edDriver.spiInstance)
		{
			if(false == spGetMasterOwnership(&edDriver, &edReleaseSpiCallback, MAX_DELAY))
				goto err;

			//nope, get SPI instance
			if(0 == (edDriver.spiInstance = spGetSpiInstance(&edDriver)))
				goto err; //not successful in acquiring SPI instance
		}

		//initialise EEPROM Comms driver
		if(false == ecInitEepromCommsDriver(&edDriver.commDriver, edDriver.spiInstance))
			goto err;

		edDriver.spiReady = true;
	}

	return true;


err:
	//return SPI instance if in posession
	if(0 != edDriver.spiInstance)
		spReleaseMasterOwnership(&edDriver);

	//reset other fields of the driver
	edDriver.spiInstance = 0;
	edDriver.spiReady = false;

	return false;
}


/**
  * @brief   Writes data to EEPROM along with CRC value check.
  *
  * @param   memStartAddr: starting address in EEPROM
  * @param   pData:		   pointer to the data
  * @param   size:		   size of the data that should be written
  *
  * @retval  B_TRUE - success
  */
static bool eepromMemWriteCrcCheck(uint16_t memStartAddr, uint8_t *pData, uint16_t size){

	uint32_t crc;
	uint8_t * checkData;
    
    //suspend RTOS scheduler
    vTaskSuspendAll();
    
	checkData = safeMalloc(sizeof(uint8_t)*size);

	if(0 == checkData)
	{
		//enable scheduler and return failure due to memory allocation problem
		xTaskResumeAll();
		return false;
	}

	//calculate CRC value of the data
	crc = softCrc(pData, size);
    
	//write data to EEPROM
    //if(false == eepromMemWriteAcrossPages(memStartAddr, pData, (uint8_t)size))
   //     goto err;

	//write CRC value
	//if(false == eepromMemWriteAcrossPages((uint16_t)(memStartAddr + size), (uint8_t*)&crc, sizeof(uint32_t)))
	//	goto err;

	//check if data got written correctly
	//if(false == eepromMemReadCrcCheck(memStartAddr, checkData, size)){
	//	safeFree(checkData);
	//	goto err;
	//}

	safeFree(checkData);
    
    //enable scheduler
    xTaskResumeAll();
    
	return true;
    
//error handler    
err:
    //enable scheduler
    xTaskResumeAll();
    //ssReportEepromFail();
    return false;
}


/**
  * @brief   Reads data from EEPROM along with CRC value check.
  *
  * @param   memStartAddr: starting address in EEPROM
  * @param   pData:		   pointer to the structure that will be filled with data
  * @param   size:		   size of the data that should be written to the structure
  *
  * @retval  B_TRUE - success
  */
static bool eepromMemReadCrcCheck(uint16_t memStartAddr, uint8_t *pData, uint16_t size){

	uint32_t crc;
	uint32_t crcEeprom;
    
    //suspend RTOS scheduler
    vTaskSuspendAll();
    
    //make sure that EEPROM has finished its previous operations
    //if(false == drvI2CAckPolling(EPPROM_ADDRESS))
    //    goto err;

	//extract data
	//if(false == drvI2CReadRegisters(memStartAddr, pData, size, EPPROM_ADDRESS))
	//	goto err;

	//extract CRC
	//if(false == drvI2CReadRegisters((uint16_t)(memStartAddr + size), (uint8_t*)&crcEeprom, (uint16_t)sizeof(uint32_t), EPPROM_ADDRESS))
	//	goto err;

	//calculate CRC
	crc = softCrc(pData, size);

	//check CRCs
	if(crc != crcEeprom)
        goto err;

    //enable scheduler
    xTaskResumeAll();
    
	return true;
    
//error handler    
err:
    //enable scheduler
    xTaskResumeAll();
    //ssReportEepromFail();
    return false;
}


/**
  * @brief   Reads data from EEPROM.
  *
  * @param   memStartAddr: starting address in EEPROM
  * @param   pData:		   pointer to the structure that will be filled with data
  * @param   size:		   size of the data that should be written to the structure
  *
  * @retval  B_TRUE - success
  */
static bool eepromMemRead(uint16_t memStartAddr, uint8_t *pData, uint16_t size){
    
    //suspend RTOS scheduler
    vTaskSuspendAll();
    
    //make sure that EEPROM has finished its previous operations
    //if(false == drvI2CAckPolling(EPPROM_ADDRESS))
    //    goto err;

	//extract data
	//if(false == drvI2CReadRegisters(memStartAddr, pData, size, EPPROM_ADDRESS))
	//	goto err;

    //enable scheduler
    xTaskResumeAll();
    
	return true;
    
//error handler    
err:
    //enable scheduler
    xTaskResumeAll();
    //ssReportEepromFail();
    return false;
}


/**
  * @brief   Example code of module usage.
  */
static void eepromExample(void)
{
    uint32_t temp = 0xDEADBEEF;
    
    if(false == eepromMemWriteCrcCheck(EEPROM_ADDR_MAGIC_NBR, (uint8_t*)&temp, sizeof(uint32_t))){
        asm("nop");
    }
    
    temp = 0;
    if(false == eepromMemReadCrcCheck(EEPROM_ADDR_MAGIC_NBR, (uint8_t*)&temp, sizeof(uint32_t))){
        asm("nop");
    }
    
    if(0xDEADBEEF == temp){
        asm("nop"); //success
    }
    else{
        asm("nop"); //failure
    }
}


static bool eepromTest(void)
{
    volatile uint16_t address;
    volatile uint16_t value;
    uint8_t  tempVal;
    uint8_t  read;
    
    asm("nop");
    
    //test every possible byte value 0 - 0xFF
    for(value = 0; value < 0x100; value++){
        
        tempVal = (uint8_t)(value & 0xFF);
        
        //at any EEPROM writable address
        for(address = 0; address < EEPROM_ADDR_LAST; address++){
            
            //if(false == eepromMemWriteAcrossPages(address, &tempVal,   1)){
            //    asm("nop");
            //}
            if(false == eepromMemRead( address, &read, 1)){
                asm("nop");
            }
            
            //check if value is the same one
            if(tempVal != read){
                return false;
            }
        }
        
    }
        
    return true;
}


