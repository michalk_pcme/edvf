/**
  ******************************************************************************
  * @file      	
  * @author    	
  * @date      	
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


/* ----------------------- Includes ---------------------------------------*/
#include "eepromComms.h"
#include "systemDefinitions.h"
#include "systemInit.h"
#include "FreeRTOS.h"
#include "task.h"



/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
static uint16_t spiReadWrite(ecDriver_t* driver, uint16_t txData);
static inline void spiWait(SPI_TypeDef* spix);
static void senseDOpin(void);


/* ----------------------- Public Functions -------------------------------*/

/**
 * @brief Function initialises fields of the ADC Comms driver.
 * @note  Should be called only after creation of the driver
 * 	 	  structure.
 */
void ecPresetEepromCommsDriver(ecDriver_t* driver)
{
	driver->spiConfigured = false;
	driver->hspi.Instance = 0;
}


/**
 * @brief Function prepares ADC Comms driver for communication using
 * 		  specified SPI instance.
 */
bool ecInitEepromCommsDriver(ecDriver_t* driver, SPI_TypeDef* spiInstance)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_HandleTypeDef* hspi;

	//safety checks
	assertPtr(spiInstance);
	assertPtr(driver);

	//init structures to known values
	ecPresetEepromCommsDriver(driver);

	//set chip select pin low
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);


	/**SPI2 GPIO Configuration
	PB13     ------> SPI2_SCK
	PB14     ------> SPI2_MISO
	PB15     ------> SPI2_MOSI
	*/
	GPIO_InitStruct.Pin 		= GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull 		= GPIO_NOPULL;
	GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate 	= GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	//SPI configuration
	//peripheral clock enable
	__HAL_RCC_SPI2_CLK_ENABLE();

	//configure SPI HAL handle and SPI peripheral
	hspi = &driver->hspi;
	hspi->Instance 					= spiInstance;
	hspi->Init.Mode 				= SPI_MODE_MASTER;
	hspi->Init.Direction 			= SPI_DIRECTION_2LINES;
	hspi->Init.DataSize 			= SPI_DATASIZE_14BIT; //SPI_DATASIZE_16BIT; //SPI_DATASIZE_8BIT;
	hspi->Init.CLKPolarity 			= SPI_POLARITY_LOW; //SPI_POLARITY_LOW;
	hspi->Init.CLKPhase 			= SPI_PHASE_1EDGE;
	hspi->Init.NSS 					= SPI_NSS_SOFT;
	hspi->Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_32; //SPI_BAUDRATEPRESCALER_2;
	hspi->Init.FirstBit 			= SPI_FIRSTBIT_MSB;
	hspi->Init.TIMode 				= SPI_TIMODE_DISABLE; //SPI_TIMODE_DISABLE;
	hspi->Init.CRCCalculation 		= SPI_CRCCALCULATION_DISABLE;
	hspi->Init.CRCPolynomial 		= 7;
	hspi->Init.CRCLength 			= SPI_CRC_LENGTH_DATASIZE;
	hspi->Init.NSSPMode 			= SPI_NSS_PULSE_DISABLE;

	if(HAL_SPI_Init(hspi) != HAL_OK)
		errorLoop();

	//configure adcc driver
	driver->spiConfigured = true;


	//enable SPI
	__HAL_SPI_ENABLE(hspi);

	return true;
}


bool ecDeInitEepromCommsDriver(ecDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//turn /CS pin low
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);

	//disable SPI
	__HAL_SPI_DISABLE(&driver->hspi);

	//de-init SPI peripheral
	//Peripheral and DMA clocks disable
	__HAL_RCC_SPI2_CLK_DISABLE();

	/**SPI2 GPIO Configuration
	PB13     ------> SPI2_SCK
	PB14     ------> SPI2_MISO
	PB15     ------> SPI2_MOSI
	*/
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15);

	HAL_SPI_DeInit(&driver->hspi);

	//reset ADC comms driver
	ecPresetEepromCommsDriver(driver);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool
 */
bool ecErase(ecDriver_t* driver, uint16_t address)
{
	uint16_t temp;

	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//turn /CS high
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_SET);

	//assemble a command
	temp = EEPROM_CMD_ERASE | (EEPROM_ADDR_MASK  & address);

	//send the command
	spiReadWrite(driver, temp);

	//toggle CS pin
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);

	senseDOpin();

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool
 */
bool ecEraseAll(ecDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//turn /CS high
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_SET);

	//send the command
	spiReadWrite(driver, EEPROM_CMD_ERAL);

	//toggle CS pin
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);

	senseDOpin();
	//vTaskDelay(500);

	return true;
}


bool ecReadMemory(ecDriver_t* driver, ecReadInq_t* inq)
{
	uint16_t cmd;
	uint16_t temp;
	uint16_t i;
	uint8_t* ptr;

	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_SET);

#if 0
	cmd = (uint16_t)(EEPROM_CMD_READ | (inq->address & EEPROM_ADDR_MASK));

	HAL_SPI_TransmitReceive(&driver->hspi, (uint8_t*)&cmd, (uint8_t*)&temp, 2, HAL_MAX_DELAY);

	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);

	cmd = EEPROM_CMD_NOP;

	HAL_SPI_TransmitReceive(&driver->hspi, (uint8_t*)&cmd, (uint8_t*)&temp, 1, HAL_MAX_DELAY);

	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_14BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);
#endif

#if 1
	cmd = (uint16_t)(EEPROM_CMD_READ | (inq->address & EEPROM_ADDR_MASK));
	spiReadWrite(driver, cmd);

#if 0
	//todo reconfigure SPI for 8bits transmission
	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);
#endif

	cmd = (uint16_t)(0x3FFF);
	temp = spiReadWrite(driver, cmd);
	ptr = inq->data;
	*ptr = (uint8_t)temp;
	ptr++;

#if 0
	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_14BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);
#endif

/*

	ptr = inq->data;
	for(i=0; i<inq->dataSize; i++)
	{
		temp = spiReadWrite(driver, EEPROM_CMD_NOP);
		*ptr = (uint8_t)temp;
		ptr++;
	}
*/
#endif
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);

	asm("nop");

	return true;
}


bool ecWriteMemory(ecDriver_t* driver, ecWriteInq_t* inq)
{
	uint16_t cmd;
	uint16_t temp;
	uint16_t i;
	uint8_t* ptr;

	GPIO_InitTypeDef GPIO_InitStruct;

	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//set the DO pin to input
	GPIO_InitStruct.Pin 	= GPIO_PIN_14;
	GPIO_InitStruct.Mode 	= GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_11BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);

	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_SET);

	ptr = inq->data;
	for(i=0; i<inq->dataSize; i++)
	{
		cmd = (uint16_t)((EEPROM_CMD_WRITE | (inq->address & EEPROM_ADDR_MASK)) >> 3);
		spiReadWrite(driver, cmd);

		cmd = ((inq->address & 0x7) << 8) | (0xFF & *ptr);
		//temp = *ptr << 6;
		spiReadWrite(driver, cmd);

		ptr++;

		HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);

		senseDOpin();


#if 0
	//reconfigure SPI for 8bits transmission
	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);

	spiReadWrite(driver, *inq->data);

	HAL_SPI_DeInit(&driver->hspi);

	driver->hspi.Init.DataSize = SPI_DATASIZE_14BIT;
	if(HAL_SPI_Init(&driver->hspi) != HAL_OK)
		errorLoop();

	__HAL_SPI_ENABLE(&driver->hspi);
#endif

	}

	asm("nop");

	return true;
}


/* ----------------------- Private Functions ------------------------------*/

static uint16_t spiReadWrite(ecDriver_t* driver, uint16_t txData)
{
	uint16_t temp16u;
	uint8_t temp8u;

	if(driver->hspi.Init.DataSize == SPI_DATASIZE_8BIT)
	{
		spiWait(driver->hspi.Instance);

		//It needs to be arranged with *(__IO uint8_t *), otherwise it will make 16bit
		//transfers of data regardless of bit per transmission selected at the
		//peripheral.
		temp8u = (uint8_t)txData;
		*(__IO uint8_t *)&driver->hspi.Instance->DR = temp8u;

		spiWait(driver->hspi.Instance);

		temp8u = *(__IO uint8_t *)&driver->hspi.Instance->DR;

		return temp8u;
	}
	else
	{
		spiWait(driver->hspi.Instance);

		//It needs to be arranged with *(__IO uint8_t *), otherwise it will make 16bit
		//transfers of data regardless of bit per transmission selected at the
		//peripheral.
		*(__IO uint16_t *)&driver->hspi.Instance->DR = txData;

		spiWait(driver->hspi.Instance);

		temp16u = *(__IO uint16_t *)&driver->hspi.Instance->DR;

		return temp16u;
	}

}


static inline void spiWait(SPI_TypeDef* spix)
{
	while(((spix->SR & (SPI_SR_TXE | SPI_SR_RXNE)) == 0) || (spix->SR & SPI_SR_BSY))
	{
		asm("nop");
	}
}


static void senseDOpin(void)
{
	volatile int i=0;
	GPIO_InitTypeDef GPIO_InitStruct;

	//set the DO pin to input
	//GPIO_InitStruct.Pin 	= GPIO_PIN_14;
	//GPIO_InitStruct.Mode 	= GPIO_MODE_INPUT;
	//GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	//HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	//toggle CS pin
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_SET);

	//wait for the DO pin
	while(GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14))
	{
		i++;
		asm("nop");
	}

	//toggle CS
	HAL_GPIO_WritePin(E2PROM_CS_GPIO_Port, E2PROM_CS_Pin, GPIO_PIN_RESET);

	//turn DO to spi function
	GPIO_InitStruct.Pin 		= GPIO_PIN_14;
	GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull 		= GPIO_NOPULL;
	GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate 	= GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	asm("nop");
}
