/**
  ******************************************************************************
  * @file      	
  * @author    	
  * @date      	
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _EEPROM_COMMS_H_
#define _EEPROM_COMMS_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"


/* ----------------------- Public Defines ---------------------------------*/
#define EEPROM_ADDR_MASK				(uint16_t)(0x07FF)

#define EEPROM_CMD_NOP					(uint8_t)(0xFF)

#define EEPROM_CMD_READ					(uint16_t)(0x3000)
#define EEPROM_CMD_EWEN					(uint16_t)(0x2600)
#define EEPROM_CMD_ERASE				(uint16_t)(0x3800)
#define EEPROM_CMD_ERAL					(uint16_t)(0x2400)
#define EEPROM_CMD_WRITE				(uint16_t)(0x2800)
#define EEPROM_CMD_WRAL					(uint16_t)(0x2200)
#define EEPROM_CMD_EWDS					(uint16_t)(0x2000)



/* ----------------------- Public Type Definitions ------------------------*/

typedef struct
{
	uint16_t address;		/*!< starting register address from which data should be read*/
	uint16_t dataSize;		/*!< number of bytes to be read */
	uint8_t* data;			/*!< data buffer */
}ecReadInq_t;


typedef struct
{
	uint16_t address;		/*!< starting register address towards which data should be written*/
	uint16_t dataSize;		/*!< number of bytes to be written */
	uint8_t* data;			/*!< data buffer */
}ecWriteInq_t;


typedef struct
{
	bool spiConfigured;
	SPI_HandleTypeDef hspi;
}ecDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void ecPresetEepromCommsDriver(ecDriver_t* driver);
bool ecInitEepromCommsDriver(ecDriver_t* driver, SPI_TypeDef* spiInstance);
bool ecDeInitEepromCommsDriver(ecDriver_t* driver);

bool ecErase(ecDriver_t* driver, uint16_t address);
bool ecEraseAll(ecDriver_t* driver);
bool ecReadMemory(ecDriver_t* driver, ecReadInq_t* inq);
bool ecWriteMemory(ecDriver_t* driver, ecWriteInq_t* inq);
//bool ecReadMemory(ecDriver_t* driver, ecWriteInq_t* inq);

#endif  /*_EEPROM_COMMS_H_*/
