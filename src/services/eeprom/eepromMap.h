/**
  ******************************************************************************
  * @file      	eepromMap.h
  * @author    	Michal Kalbarczyk
  * @date      	17-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


#ifndef _EEPROMMAP_H_
#define _EEPROMMAP_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Public Type Definitions ------------------------*/

typedef struct{
    uint32_t none;
}__attribute__ ((packed)) emFactoryVars_t;


typedef struct{
	uint32_t none;
}__attribute__ ((packed)) emUserVars_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
/* ----------------------- Public Defines ---------------------------------*/

/**
 * @brief EEPROM size is of 16kbits = 2kB
 */
#define EEPROM_SIZE							(uint16_t)(0x07D0)

/**
 * @brief At the end of each address there is always placed its 32bit CRC value.
 * 		  newAddress = prevAddress + size of prev variable/struct + size of prev variable CRC
 */
#define EEPROM_CRC_SIZE_BYTES               (sizeof(uint32_t))

#define EEPROM_BASE							0


/**
 * Writing algorithm supports writing across pages, so do not worry about
 * EEPROM mapping.
 */

#define EEPROM_ADDR_MAGIC_NBR               (uint16_t)(EEPROM_BASE)

#define EEPROM_ADDR_FACTORY_VARIABLES       (uint16_t)(EEPROM_ADDR_MAGIC_NBR         + sizeof(uint32_t)        + EEPROM_CRC_SIZE_BYTES)
#define EEPROM_ADDR_USER_VARIABLES          (uint16_t)(EEPROM_ADDR_FACTORY_VARIABLES + sizeof(emFactoryVars_t) + EEPROM_CRC_SIZE_BYTES)
#define EEPROM_ADDR_UNLOCK_CODES            (uint16_t)(EEPROM_ADDR_USER_VARIABLES    + sizeof(emUserVars_t)    + EEPROM_CRC_SIZE_BYTES)

#define EEPROM_ADDR_NEXT					(uint16_t)(EEPROM_ADDR_UNLOCK_CODES      + sizeof(eepromUnlockCodes_t) + EEPROM_CRC_SIZE_BYTES)


/**
 * @brief The last writable address on the EEPROM. 
 */
#define EEPROM_ADDR_LAST                    (uint16_t)(EEPROM_SIZE-1)


//hard-coded fields in EEPROM chip           



#endif /* _EEPROMMAP_H_ */
