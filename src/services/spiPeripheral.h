/**
  ******************************************************************************
  * @file      	spiPeripheral.h
  * @author    	Michal Kalbarczyk
  * @date      	12-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _SPI_PERIPHERAL_H_
#define _SPI_PERIPHERAL_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"

#include "resourceGuard.h"


/* ----------------------- Public Defines ---------------------------------*/
/* ----------------------- Public Type Definitions ------------------------*/
typedef struct
{
	SPI_TypeDef *instance;
	rgDriver_t spiGuard;
}spDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void spInitSpiInstance(void);
bool spGetMasterOwnership(void* newMasterHandle, rgReleaseClb_t releaseCallback, TickType_t ticksToWait);
void spReleaseMasterOwnership(void* masterHandle);
bool spCheckMasterOwnership(void* masterHandle);
SPI_TypeDef* spGetSpiInstance(void* masterHandle);


#ifdef CEEDLING_TEST
extern STATIC spDriver_t spDriver;
#endif


#endif  /*_SPI_PERIPHERAL_H_*/
