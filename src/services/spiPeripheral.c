/**
  ******************************************************************************
  * @file      	spiPeripheral.c
  * @author    	Michal Kalbarczyk
  * @date      	12-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"

#include "spiPeripheral.h"

#include "resourceGuard.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
STATIC spDriver_t spDriver;


/* ----------------------- Private Prototypes -----------------------------*/
/* ----------------------- Public Functions -------------------------------*/
void spInitSpiInstance(void)
{
	spDriver.instance = SPI2;

	rgDriverConstructor(&spDriver.spiGuard);
}


bool spGetMasterOwnership(void* newMasterHandle, rgReleaseClb_t releaseCallback, TickType_t ticksToWait)
{
	return rgRegisterNewMaster(
			&spDriver.spiGuard,
			newMasterHandle,
			releaseCallback,
			ticksToWait);
}


void spReleaseMasterOwnership(void* masterHandle)
{
	rgUnregisterMaster(&spDriver.spiGuard, masterHandle);
}


bool spCheckMasterOwnership(void* masterHandle)
{
	return rgCheckMasterHandle(&spDriver.spiGuard, masterHandle);
}


SPI_TypeDef* spGetSpiInstance(void* masterHandle)
{
	if(false == rgCheckMasterHandle(&spDriver.spiGuard, masterHandle))
		return 0;

	return spDriver.instance;
}


/* ----------------------- Private Functions ------------------------------*/

