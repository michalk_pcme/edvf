/**
  ******************************************************************************
  * @file      	adcDriver.h
  * @author    	Michal Kalbarczyk
  * @date      	15-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _ADC_DRIVER_H_
#define _ADC_DRIVER_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <semphr.h>
#include <queue.h>

#include "adcComms.h"
#include "adcRegisters.h"


/* ----------------------- Public Defines ---------------------------------*/
#define ADCD_TASK_SIZE						1024
#define ADCD_INQUIRY_QUEUE_SIZE				4

#define ADCD_SEMAPH_CHECK_INTERVAL			(SERVICES_REFRESH_PERIOD)


/* ----------------------- Public Type Definitions ------------------------*/

typedef enum
{
	ADCD_RELEASE_SPI_PERIPHERAL,
	ADCD_READ_ADC_REGISTERS,
	ADCD_WRITE_ADC_REGISTERS,
	ADCD_READ_ADC_REGISTERS_TO_AUX,
	ADCD_WRITE_ADC_REGISTERS_FROM_AUX,
	ADCD_ENABLE_SENDING_ADC_SAMPLES_TO_AUX,
	ADCD_SEND_ADC_COMMAND,
	ADCD_REGISTER_SAMPLE_READY_CALLBACK,
	ADCD_UNREGISTER_SAMPLE_READY_CALLBACK
}adcdInqTypes_t;


typedef struct
{
	uint8_t command;
}__attribute__ ((packed)) adcrAdcCmdPar_t;


/**
 * @brief Callback notifying that inquiry has been processed.
 */
typedef BaseType_t(*adcdInqClb_t)(bool status, uint32_t ticksToWait);


/**
 * @brief Callback returning new ADC sample.
 */
typedef BaseType_t(*adcdSampRdyClb_t)(float sample, uint32_t ticksToWait);


/**
 * @brief Callback returning read ADC registers.
 */
typedef BaseType_t(*adcdReadAdcRegClb_t)(adcrRegisters_t* adcRegisters, uint32_t ticksToWait);


/**
 * @brief Callback called whenever module wants master to release
 * 		  its control over the module.
 */
typedef BaseType_t(*adcdUnregMstrClb_t)(uint32_t ticksToWait);


typedef struct
{
	adcdInqTypes_t inqType;

	adcdInqClb_t inqProcessedCallback;

	union{
		struct{
			uint16_t none;
	    }releaseSpiPeripheral;

	    struct{
	    	adcdReadAdcRegClb_t readAdcRegCallback;
	    }readAdcRegisters;

	    struct{
	    	adcrRegisters_t registers;	// <<-- this is bit big, but... it makes code clean
	    }writeAdcRegisters;

	    struct{
	      	uint16_t none;
	    }readAdcRegistersToAux;

	    struct{
	    	bool enable;
	    }enableSendingAdcSamplesToAux;

	    struct{
	    	uint8_t command;
	    }sendAdcCommand;

	    struct{
	    	adcdSampRdyClb_t adcSampleReadyCallback;
	    }registerSampleReadyCallback;

	    struct{
	    	adcdSampRdyClb_t adcSampleReadyCallback;
	    }unregisterSampleReadyCallback;
	}params;

}adcdInquiry_t;


typedef struct
{
	bool spiReady;				/*!< flag - SPI instance acquired AND commDriver configured*/

	bool sendAdcSamplesToAuxComms;
	uint32_t latestRawAdcData;
	float latestConvertedAdcData;

	SPI_TypeDef* spiInstance;
	adccDriver_t commDriver;

	QueueHandle_t inquiryQueue;
	SemaphoreHandle_t dataReadySemaphore;
	adcdSampRdyClb_t adcSampleReadyCallback;
}adcDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void adcdInitAdcDriver(void);
void adcdTask(void);
void adcdDrdyGpioTriggeredLow(void);

BaseType_t adcdReleaseSpiCallback(uint32_t ticksToWait);

BaseType_t adcdRegisterSampleReadyCallback(  void* masterHandle, adcdInqClb_t inqProcessedCallback, adcdSampRdyClb_t adcSampleReadyCallback, uint32_t ticksToWait);
BaseType_t adcdUnregisterSampleReadyCallback(void* masterHandle, adcdInqClb_t inqProcessedCallback, adcdSampRdyClb_t adcSampleReadyCallback, uint32_t ticksToWait);
BaseType_t adcdReadAdcRegisters(             void* masterHandle, adcdInqClb_t inqProcessedCallback, adcdReadAdcRegClb_t readAdcRegCallback, uint32_t ticksToWait);
BaseType_t adcdWriteAdcRegisters(            void* masterHandle, adcdInqClb_t inqProcessedCallback, adcrRegisters_t* registers, uint32_t ticksToWait);
BaseType_t adcdSendAdcCommand(               void* masterHandle, adcdInqClb_t inqProcessedCallback, uint8_t command, uint32_t ticksToWait);

BaseType_t adcdReadAdcRegistersToAux(        adcdInqClb_t inqProcessedCallback, uint32_t ticksToWait);
BaseType_t adcdWriteAdcRegistersFromAux(     adcdInqClb_t inqProcessedCallback, adcrRegisters_t* registers, uint32_t ticksToWait);
BaseType_t adcdEnableSendingAdcSamplesToAux( adcdInqClb_t inqProcessedCallback, bool enable, uint32_t ticksToWait);
BaseType_t adcdSendAdcCommandFromAux(        adcdInqClb_t inqProcessedCallback, uint8_t command, uint32_t ticksToWait);


#endif  /*_ADC_DRIVER_H_*/
