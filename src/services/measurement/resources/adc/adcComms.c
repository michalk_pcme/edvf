/**
  ******************************************************************************
  * @file      	adcComms.c
  * @author    	Michal Kalbarczyk
  * @date      	12-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "systemInit.h"

#include "adcComms.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
#define NBR_OF_CONVERSION_BYTES				3


/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
static uint8_t spiReadWrite(adccDriver_t* driver, uint8_t txData);
static inline void spiWait(SPI_TypeDef* spix);


/* ----------------------- Public Functions -------------------------------*/

/**
 * @brief Function initialises fields of the ADC Comms driver.
 * @note  Should be called only after creation of the driver
 * 	 	  structure.
 */
void adccPresetAdcCommsDriver(adccDriver_t* driver)
{
	driver->spiConfigured = false;
	driver->hspi.Instance = 0;
}


/**
 * @brief Function prepares ADC Comms driver for communication using
 * 		  specified SPI instance.
 */
bool adccInitAdcCommsDriver(adccDriver_t* driver, SPI_TypeDef* spiInstance)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_HandleTypeDef* hspi;

	//safety checks
	assertPtr(spiInstance);
	assertPtr(driver);

	//init structures to known values
	adccPresetAdcCommsDriver(driver);

	//set chip select pin high (disables SPI interface on ADC chip)
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);


	/**SPI2 GPIO Configuration
	PB13     ------> SPI2_SCK
	PB14     ------> SPI2_MISO
	PB15     ------> SPI2_MOSI
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	//SPI configuration
	//peripheral clock enable
	__HAL_RCC_SPI2_CLK_ENABLE();

	//SPI2 interrupt Init
	//HAL_NVIC_SetPriority(SPI2_IRQn, 5, 0);
	//HAL_NVIC_EnableIRQ(SPI2_IRQn);

	//configure SPI HAL handle and SPI peripheral
	hspi = &driver->hspi;
	hspi->Instance 					= spiInstance;
	hspi->Init.Mode 				= SPI_MODE_MASTER;
	hspi->Init.Direction 			= SPI_DIRECTION_2LINES;
	hspi->Init.DataSize 			= SPI_DATASIZE_8BIT; //SPI_DATASIZE_16BIT; //SPI_DATASIZE_8BIT;
	hspi->Init.CLKPolarity 			= SPI_POLARITY_LOW;
	hspi->Init.CLKPhase 			= SPI_PHASE_2EDGE;
	hspi->Init.NSS 					= SPI_NSS_SOFT;
#ifdef SYSTEM_CLOCK_8MHZ
	hspi->Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_2; //SPI_BAUDRATEPRESCALER_2;
#endif
#ifdef SYSTEM_CLOCK_8_192MHZ
	hspi->Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_2; //SPI_BAUDRATEPRESCALER_2;
#endif
#ifdef SYSTEM_CLOCK_16MHZ
	hspi->Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_4; //SPI_BAUDRATEPRESCALER_2;
#endif
#ifdef SYSTEM_CLOCK_80MHZ
	hspi->Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_16; //SPI_BAUDRATEPRESCALER_2;
#endif
	hspi->Init.FirstBit 			= SPI_FIRSTBIT_MSB;
	hspi->Init.TIMode 				= SPI_TIMODE_DISABLE;
	hspi->Init.CRCCalculation 		= SPI_CRCCALCULATION_DISABLE;
	hspi->Init.CRCPolynomial 		= 7;
	hspi->Init.CRCLength 			= SPI_CRC_LENGTH_DATASIZE;
	hspi->Init.NSSPMode 			= SPI_NSS_PULSE_DISABLE;

	if(HAL_SPI_Init(hspi) != HAL_OK)
		errorLoop();

	//configure adcc driver
	driver->spiConfigured = true;


	//enable SPI
	__HAL_SPI_ENABLE(hspi);

	return true;
}


bool adccDeInitAdcCommsDriver(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//turn /CS pin high
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	//disable SPI
	__HAL_SPI_DISABLE(&driver->hspi);

	//de-init SPI peripheral
	//Peripheral and DMA clocks disable
	__HAL_RCC_SPI2_CLK_DISABLE();

	/**SPI2 GPIO Configuration
	PB13     ------> SPI2_SCK
	PB14     ------> SPI2_MISO
	PB15     ------> SPI2_MOSI
	*/
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15);

	//SPI2 and DMA interrupts DeInit
	//HAL_NVIC_DisableIRQ(SPI2_IRQn);

	HAL_SPI_DeInit(&driver->hspi);

	//reset ADC comms driver
	adccPresetAdcCommsDriver(driver);

	return true;
}


bool adccIsDriverConfigured(adccDriver_t* driver)
{
	return driver->spiConfigured;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccWakeup(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//send the WAKEUP command
	spiReadWrite(driver, ADC_CMD_WAKEUP);

	//turn /CS pin high
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccSleep(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//turn /CS pin low
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	//send the SLEEP command
	spiReadWrite(driver, ADC_CMD_SLEEP);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccSync(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	//turn /CS low
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	//send the SYNC command
	spiReadWrite(driver, ADC_CMD_SYNC_1);
	spiReadWrite(driver, ADC_CMD_SYNC_2);

	//turn /CS high
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccReset(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	//send the command
	spiReadWrite(driver, ADC_CMD_RESET);

	//wait for 0.6ms for reset to finish (dependent on ADC input CLK)
	vTaskDelay(1/portTICK_PERIOD_MS);

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  Function performs RDATA command.
 * @param  driver:  [IN]  adcComms handle
 * 		   txData:  [OUT] ADC conversion result
 * @retval bool - SPI transaction successful
 */
bool adccRdata(adccDriver_t* driver, uint32_t* txData)
{
	uint8_t* ptr;
	uint16_t i;

	//safety checks
	assertPtr(driver);
	assertPtr(txData);

	if(false == driver->spiConfigured) return false;

	//clear the storage
	*txData = 0;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	//send the RDATA command
	spiReadWrite(driver, ADC_CMD_RDATA);

	//collect conversion result byte by byte and assemble them into single value
	//assign pointer to the variable field
	ptr = (uint8_t*)txData;

	//move the pointer to the MSB position of the 24bit variable
	ptr += 2;
	for(i=0; i<NBR_OF_CONVERSION_BYTES; i++)
	{
		//data reception is from MSB to LSB
		*ptr = spiReadWrite(driver, ADC_CMD_NOP);
		ptr--;
	}

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccRdataC(adccDriver_t* driver)
{
	//safety checks
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	//send the command
	spiReadWrite(driver, ADC_CMD_RDATAC);

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccSdataC(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	//send the command
	spiReadWrite(driver, ADC_CMD_SDATAC);

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}



/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccRReg(adccDriver_t* driver, adccRRegInq_t* inq)
{
	uint8_t cmd;
	uint8_t i;
	uint8_t* ptr;

	assertPtr(driver);
	assertPtr(inq);
	assertPtr(inq->data);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	cmd = (uint8_t)(ADC_CMD_RREG_1 | (inq->address & 0xF));
	spiReadWrite(driver, cmd);

	cmd = (uint8_t)(ADC_CMD_RREG_2 | ((inq->dataSize-1) & 0xF));
	spiReadWrite(driver, cmd);

	ptr = (uint8_t*)inq->data;
	for(i=0; i<inq->dataSize; i++)
	{
		*ptr = spiReadWrite(driver, ADC_CMD_NOP);
		ptr++;
	}

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccWReg(adccDriver_t* driver, adccWRegInq_t* inq)
{
	uint8_t cmd;
	uint8_t i;
	uint8_t* ptr;

	assertPtr(driver);
	assertPtr(inq);
	assertPtr(inq->data);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	cmd = (uint8_t)(ADC_CMD_WREG_1 | (inq->address & 0xF));
	spiReadWrite(driver, cmd);

	cmd = (uint8_t)(ADC_CMD_WREG_2 | ((inq->dataSize-1) & 0xF));
	spiReadWrite(driver, cmd);

	ptr = (uint8_t*)inq->data;
	for(i=0; i<inq->dataSize; i++)
	{
		spiReadWrite(driver, *ptr);
		ptr++;
	}

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccSysOCal(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	spiReadWrite(driver, ADC_CMD_SYSOCAL);

	//wait for DRDY to get low
	//(can take around 30ms -> refer to manual Table 17. Calibration Time Versus Data Rate)
	while(GPIO_PIN_SET == HAL_GPIO_ReadPin(DRDY_GPIO_Port, DRDY_Pin))
	{}

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccSysGCal(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	spiReadWrite(driver, ADC_CMD_SYSGCAL);

	//wait for DRDY to get low
	//(can take around 30ms -> refer to manual Table 17. Calibration Time Versus Data Rate)
	while(GPIO_PIN_SET == HAL_GPIO_ReadPin(DRDY_GPIO_Port, DRDY_Pin))
	{}

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccSelfOCal(adccDriver_t* driver)
{
	assertPtr(driver);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	spiReadWrite(driver, ADC_CMD_SELFOCAL);

	//wait for DRDY to get low
	//(can take around 30ms -> refer to manual Table 17. Calibration Time Versus Data Rate)
	while(GPIO_PIN_SET == HAL_GPIO_ReadPin(DRDY_GPIO_Port, DRDY_Pin))
	{}

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/**
 * @brief  ???
 * @param  driver:  [IN]  adcComms handle
 * @retval bool - SPI transaction successful
 */
bool adccNop(adccDriver_t* driver, uint8_t* txData)
{
	assertPtr(driver);
	assertPtr(txData);

	if(false == driver->spiConfigured) return false;

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_RESET);

	*txData = spiReadWrite(driver, ADC_CMD_NOP);

	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

	return true;
}


/* ----------------------- Private Functions ------------------------------*/

static uint8_t spiReadWrite(adccDriver_t* driver, uint8_t txData)
{
	uint8_t temp;

	spiWait(driver->hspi.Instance);

	//It needs to be arranged with *(__IO uint8_t *), otherwise it will make 16bit
	//transfers of data regardless of bit per transmission selected at the
	//peripheral.
	*(__IO uint8_t *)&driver->hspi.Instance->DR = txData;

	spiWait(driver->hspi.Instance);

	temp = *(__IO uint8_t *)&driver->hspi.Instance->DR;

	return (uint8_t)temp;

}


static inline void spiWait(SPI_TypeDef* spix)
{
	while(((spix->SR & (SPI_SR_TXE | SPI_SR_RXNE)) == 0) || (spix->SR & SPI_SR_BSY))
	{
		asm("nop");
	}
}



