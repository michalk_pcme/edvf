/**
  ******************************************************************************
  * @file      	
  * @author    	
  * @date      	
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _ADC_REGISTERS_H_
#define _ADC_REGISTERS_H_


/* ----------------------- Includes ---------------------------------------*/
/* ----------------------- Public Defines ---------------------------------*/
#define ADS_NBR_OF_REGISTERS		11


//registers addresses
#define ADS_REG_BCS					((uint8_t) 0x0)
#define ADS_REG_VBIAS				((uint8_t) 0x1)
#define ADS_REG_MUX1				((uint8_t) 0x2)
#define ADS_REG_SYS0				((uint8_t) 0x3)
#define ADS_REG_OFC0				((uint8_t) 0x4)
#define ADS_REG_OFC1				((uint8_t) 0x5)
#define ADS_REG_OFC2				((uint8_t) 0x6)
#define ADS_REG_FSC0				((uint8_t) 0x7)
#define ADS_REG_FSC1				((uint8_t) 0x8)
#define ADS_REG_FSC2				((uint8_t) 0x9)
#define ADS_REG_ID					((uint8_t) 0xA)


//bit definition for BCS register
#define ADS_BCS_RES_Pos				(0U)
#define ADS_BCS_RES_Msk				(0x1U << ADS_BCS_RES_Pos)
#define ADS_BCS_BCS_Pos				(6U)
#define ADS_BCS_BCS_Msk				(0x3U << ADS_BCS_BCS_Pos)

//bit definitions for VBIAS register
#define ADS_VBIAS_VBIAS_Pos			(0U)
#define ADS_VBIAS_VBIAS_Msk			(0x3U << ADS_BCS_BCS_Pos)

//bit definitions for MUX1 register
#define ADS_MUX1_MUXCAL_Pos			(0U)
#define ADS_MUX1_MUXCAL_Msk			(0x7U << ADS_MUX1_MUXCAL_Pos)
#define ADS_MUX1_CLKSTAT_Pos		(7U)
#define ADS_MUX1_CLKSTAT_Msk		(0x1U << ADS_MUX1_CLKSTAT_Pos)

//bit definitions for SYS0 register
#define ADS_SYS0_DR_Pos				(0U)
#define ADS_SYS0_DR_Msk				(0xFU << ADS_SYS0_DR_Pos)
#define ADS_SYS0_PGA_Pos			(4U)
#define ADS_SYS0_PGA_Msk			(0x7U << ADS_SYS0_PGA_Pos)

//bit definitions for OFC0 register
#define ADS_OFC0_OFC_Pos			(0U)
#define ADS_OFC0_OFC_Msk			(0xFFU << ADS_OFC0_OFC_Pos)

//bit definitions for OFC1 register
#define ADS_OFC1_OFC_Pos			(0U)
#define ADS_OFC1_OFC_Msk			(0xFFU << ADS_OFC1_OFC_Pos)

//bit definitions for OFC2 register
#define ADS_OFC2_OFC_Pos			(0U)
#define ADS_OFC2_OFC_Msk			(0xFFU << ADS_OFC2_OFC_Pos)

//bit definitions for FSC0 register
#define ADS_FSC0_OFC_Pos			(0U)
#define ADS_FSC0_OFC_Msk			(0xFFU << ADS_FSC0_OFC_Pos)

//bit definitions for FSC1 register
#define ADS_FSC1_OFC_Pos			(0U)
#define ADS_FSC1_OFC_Msk			(0xFFU << ADS_FSC1_OFC_Pos)

//bit definitions for FSC2 register
#define ADS_FSC2_OFC_Pos			(0U)
#define ADS_FSC2_OFC_Msk			(0xFFU << ADS_FSC2_OFC_Pos)

//bit definitions for ID register
#define ADS_ID_DRDYMODE_Pos			(3U)
#define ADS_ID_DRDYMODE_Msk			(0x1U << ADS_ID_DRDYMODE_Pos)
#define ADS_ID_ID_Pos				(4U)
#define ADS_ID_ID_Msk				(0xFU << ADS_ID_ID_Pos)


/* ----------------------- Public Type Definitions ------------------------*/
typedef union
{
	uint8_t mtx[ADS_NBR_OF_REGISTERS];

	struct{
		uint8_t BCS;
		uint8_t VBIAS;
		uint8_t MUX1;
		uint8_t SYS0;
		uint8_t OFC0;
		uint8_t OFC1;
		uint8_t OFC2;
		uint8_t FSC0;
		uint8_t FSC1;
		uint8_t FSC2;
		uint8_t ID;
	}regs;
}__attribute__ ((packed)) adcrRegisters_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/


#endif  /*_ADC_REGISTERS_H_*/
