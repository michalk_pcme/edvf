/**
  ******************************************************************************
  * @file      	adcComms.h
  * @author    	Michal Kalbarczyk
  * @date      	12-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _ADC_COMMS_H_
#define _ADC_COMMS_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"


/* ----------------------- Public Defines ---------------------------------*/
#define ADC_CMD_WAKEUP				((uint8_t) 0x00)
#define ADC_CMD_SLEEP				((uint8_t) 0x02)
#define ADC_CMD_SYNC_1				((uint8_t) 0x04)
#define ADC_CMD_SYNC_2				((uint8_t) 0x04)
#define ADC_CMD_RESET				((uint8_t) 0x06)
#define ADC_CMD_NOP					((uint8_t) 0xFF)
#define ADC_CMD_RDATA				((uint8_t) 0x12)
#define ADC_CMD_RDATAC				((uint8_t) 0x14)
#define ADC_CMD_SDATAC				((uint8_t) 0x16)
#define ADC_CMD_RREG_1				((uint8_t) 0x20)
#define ADC_CMD_RREG_2				((uint8_t) 0x00)
#define ADC_CMD_WREG_1				((uint8_t) 0x40)
#define ADC_CMD_WREG_2				((uint8_t) 0x00)
#define ADC_CMD_SYSOCAL				((uint8_t) 0x60)
#define ADC_CMD_SYSGCAL				((uint8_t) 0x61)
#define ADC_CMD_SELFOCAL			((uint8_t) 0x62)
#define ADC_CMD_RESTRICTED			((uint8_t) 0xF1)


/* ----------------------- Public Type Definitions ------------------------*/
typedef struct
{
	uint8_t address;		/*!< starting register address from which data should be read*/
	uint8_t dataSize;		/*!< number of bytes to be read */
	uint8_t* data;			/*!< data buffer */
}adccRRegInq_t;


typedef struct
{
	uint8_t address;		/*!< */
	uint8_t dataSize;		/*!< */
	uint8_t* data;			/*!< */
}adccWRegInq_t;


typedef struct
{
	bool spiConfigured;
	SPI_HandleTypeDef hspi;
}adccDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void adccPresetAdcCommsDriver(adccDriver_t* driver);
bool adccInitAdcCommsDriver(adccDriver_t* driver, SPI_TypeDef* spiInstance);
bool adccDeInitAdcCommsDriver(adccDriver_t* driver);
bool adccIsDriverConfigured(adccDriver_t* driver);

bool adccWakeup(adccDriver_t* driver);
bool adccSleep(adccDriver_t* driver);
bool adccSync(adccDriver_t* driver);
bool adccReset(adccDriver_t* driver);
bool adccRdata(adccDriver_t* driver, uint32_t* txData);
bool adccRdataC(adccDriver_t* driver);
bool adccSdataC(adccDriver_t* driver);
bool adccRReg(adccDriver_t* driver, adccRRegInq_t* inq);
bool adccWReg(adccDriver_t* driver, adccWRegInq_t* inq);
bool adccSysOCal(adccDriver_t* driver);
bool adccSysGCal(adccDriver_t* driver);
bool adccSelfOCal(adccDriver_t* driver);
bool adccNop(adccDriver_t* driver, uint8_t* txData);


#endif  /*_ADC_COMMS_H_*/
