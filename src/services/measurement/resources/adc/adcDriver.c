/**
  ******************************************************************************
  * @file      	adcDriver.c
  * @author    	Michal Kalbarczyk
  * @date      	15-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "systemInit.h"
#include <FreeRTOS.h>
#include <task.h>

#include <string.h>

#include "adcDriver.h"
#include "adcComms.h"
#include "adcRegisters.h"
#include "spiPeripheral.h"

//#include "acEngine.h"
#include "acDmaAux.h"
#include "acCommands.h"

#include "measResGuard.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
static adcDriver_t adcdDriver;


/* ----------------------- Private Prototypes -----------------------------*/
static void adcdProcessInquiry(adcdInquiry_t* inq);
static bool adcdAcquireConfigureSpi(void);
static void adcdCollectAdcData(void);
static void adcdSetRegister(uint8_t* mtx, uint8_t address, uint8_t pos, uint8_t mask, uint8_t val);
static void adcdInitAdcChip(void);
static void adcdSendCommand(uint8_t cmd);

static bool adcdReleaseSpiPeripheralInq(adcdInquiry_t* inq);
static bool adcdReadAdcRegistersInq(adcdInquiry_t* inq);
static bool adcdWriteAdcRegistersInq(adcdInquiry_t* inq);
static bool adcdReadRegistersToAuxInq(adcdInquiry_t* inq);
static bool adcdWriteAdcRegistersFromAuxInq(adcdInquiry_t* inq);

float adcdConvertRawSample(uint32_t sample);


/* ----------------------- Public Functions -------------------------------*/
void adcdInitAdcDriver(void)
{

    //initialise task
    xTaskCreate((TaskFunction_t) adcdTask,
                "ADC driver",
				ADCD_TASK_SIZE, NULL, ADC_DRIVER_TASK_PRIORITY, NULL);

    //initialise messaging queue
    adcdDriver.inquiryQueue = xQueueCreate(ADCD_INQUIRY_QUEUE_SIZE, sizeof(adcdInquiry_t));

    //initialise semaphores
    vSemaphoreCreateBinary(adcdDriver.dataReadySemaphore);
    xSemaphoreTake(adcdDriver.dataReadySemaphore, MAX_DELAY);

    //initialise ADC driver
    adcdDriver.adcSampleReadyCallback = 0;
    adcdDriver.spiInstance = 0;
    adcdDriver.spiReady = false;
    adcdDriver.sendAdcSamplesToAuxComms = false;
}


void adcdTask(void)
{
	adcdInquiry_t inq;

	adcdInitAdcChip();

    while(1)
    {
        //process inquiry queue
        if(pdPASS == xQueueReceive(adcdDriver.inquiryQueue, &inq, 0))
        {
            adcdProcessInquiry(&inq);
        }

        //poll for notification from the DRDY pin (TODO this needs reworking as it takes too much time to wait between consecutive inquiry processing)
        if(pdTRUE == xSemaphoreTake(adcdDriver.dataReadySemaphore, ADCD_SEMAPH_CHECK_INTERVAL / portTICK_PERIOD_MS))
        {
        	adcdCollectAdcData();
        }
    }
}


void adcdDrdyGpioTriggeredLow(void)
{
	portBASE_TYPE   xHigherPriorityTaskWoken;

	xSemaphoreGiveFromISR(adcdDriver.dataReadySemaphore, &xHigherPriorityTaskWoken);

	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}


BaseType_t adcdReleaseSpiCallback(uint32_t ticksToWait)
{
	adcdInquiry_t inq;

    inq.inqType = ADCD_RELEASE_SPI_PERIPHERAL;
    inq.inqProcessedCallback = 0;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdRegisterSampleReadyCallback(void* masterHandle, adcdInqClb_t inqProcessedCallback, adcdSampRdyClb_t adcSampleReadyCallback, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

	//check if caller has a right to control ADC driver
	if(false == mrgCheckMasterOwnership(masterHandle))
		return pdFALSE;

    inq.inqType = ADCD_REGISTER_SAMPLE_READY_CALLBACK;
    inq.inqProcessedCallback = inqProcessedCallback;
    inq.params.registerSampleReadyCallback.adcSampleReadyCallback = adcSampleReadyCallback;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdUnregisterSampleReadyCallback(void* masterHandle, adcdInqClb_t inqProcessedCallback, adcdSampRdyClb_t adcSampleReadyCallback, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

	//check if caller has a right to control ADC driver
	if(false == mrgCheckMasterOwnership(masterHandle))
		return pdFALSE;

    inq.inqType = ADCD_UNREGISTER_SAMPLE_READY_CALLBACK;
    inq.inqProcessedCallback = inqProcessedCallback;
    inq.params.unregisterSampleReadyCallback.adcSampleReadyCallback = adcSampleReadyCallback;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


/**
 * @brief ???
 * @param masterHandle			- (optional) handle to the master module that holds ownership over ADC Driver interface
 * @param inqProcessedCallback  - (optional) pointer to callback that will be triggered after inquiry gets processed
 * @param readAdcRegCallback	- pointer to callback where ADC registers data should be returned to
 * @param ticksToWait			- time to wait to put inquiry into the ADC Driver module's internal queue.
 */
BaseType_t adcdReadAdcRegisters(void* masterHandle, adcdInqClb_t inqProcessedCallback, adcdReadAdcRegClb_t readAdcRegCallback, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

	//safety check
	if(0 == readAdcRegCallback)
		return pdFALSE;

	//check if caller has a right to control ADC driver
	if(false == mrgCheckMasterOwnership(masterHandle))
		return pdFALSE;

    inq.inqType = ADCD_READ_ADC_REGISTERS;
    inq.inqProcessedCallback = inqProcessedCallback;
    inq.params.readAdcRegisters.readAdcRegCallback = readAdcRegCallback;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdWriteAdcRegisters(void* masterHandle, adcdInqClb_t inqProcessedCallback, adcrRegisters_t* registers, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

	//check if caller has a right to control ADC driver
	if(false == mrgCheckMasterOwnership(masterHandle))
		return pdFALSE;

    inq.inqType = ADCD_WRITE_ADC_REGISTERS;
    inq.inqProcessedCallback = inqProcessedCallback;
    inq.params.writeAdcRegisters.registers = *registers;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdSendAdcCommand(void* masterHandle, adcdInqClb_t inqProcessedCallback, uint8_t command, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

	//check if caller has a right to control ADC driver
	if(false == mrgCheckMasterOwnership(masterHandle))
		return pdFALSE;

	inq.inqType = ADCD_SEND_ADC_COMMAND;
	inq.inqProcessedCallback = inqProcessedCallback;
	inq.params.sendAdcCommand.command = command;

	//add inquiry to the queue for further processing
	return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdReadAdcRegistersToAux(adcdInqClb_t inqProcessedCallback, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

    inq.inqType = ADCD_READ_ADC_REGISTERS_TO_AUX;
    inq.inqProcessedCallback = inqProcessedCallback;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdWriteAdcRegistersFromAux(adcdInqClb_t inqProcessedCallback, adcrRegisters_t* registers, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

    inq.inqType = ADCD_WRITE_ADC_REGISTERS_FROM_AUX;
    inq.inqProcessedCallback = inqProcessedCallback;
    inq.params.writeAdcRegisters.registers = *registers;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdEnableSendingAdcSamplesToAux(adcdInqClb_t inqProcessedCallback, bool enable, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

    inq.inqType = ADCD_ENABLE_SENDING_ADC_SAMPLES_TO_AUX;
    inq.inqProcessedCallback = inqProcessedCallback;
    inq.params.enableSendingAdcSamplesToAux.enable = enable;

    //add inquiry to the queue for further processing
    return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t adcdSendAdcCommandFromAux(adcdInqClb_t inqProcessedCallback, uint8_t command, uint32_t ticksToWait)
{
	adcdInquiry_t inq;

	inq.inqType = ADCD_SEND_ADC_COMMAND;
	inq.inqProcessedCallback = inqProcessedCallback;
	inq.params.sendAdcCommand.command = command;

	//add inquiry to the queue for further processing
	return xQueueSend(adcdDriver.inquiryQueue, &inq, ticksToWait);
}


/* ----------------------- Private Functions ------------------------------*/

static void adcdProcessInquiry(adcdInquiry_t* inq)
{
	bool inqStatus = false;

    switch(inq->inqType){
        case ADCD_RELEASE_SPI_PERIPHERAL:
        	inqStatus = adcdReleaseSpiPeripheralInq(inq);
            break;

        case ADCD_READ_ADC_REGISTERS:
        	inqStatus = adcdReadAdcRegistersInq(inq);
        	break;

        case ADCD_WRITE_ADC_REGISTERS:
        	inqStatus = adcdWriteAdcRegistersInq(inq);
        	break;

        case ADCD_READ_ADC_REGISTERS_TO_AUX:
        	inqStatus = adcdReadRegistersToAuxInq(inq);
        	break;


        case ADCD_WRITE_ADC_REGISTERS_FROM_AUX:
        	inqStatus = adcdWriteAdcRegistersFromAuxInq(inq);
        	break;


        case ADCD_ENABLE_SENDING_ADC_SAMPLES_TO_AUX:
        	adcdDriver.sendAdcSamplesToAuxComms = inq->params.enableSendingAdcSamplesToAux.enable;
        	inqStatus = true;
        	break;


        case ADCD_SEND_ADC_COMMAND:
        	if(false == adcdAcquireConfigureSpi())
        		break;

        	adcdSendCommand(inq->params.sendAdcCommand.command);
        	inqStatus = true;
        	break;


        case ADCD_REGISTER_SAMPLE_READY_CALLBACK:
        	adcdDriver.adcSampleReadyCallback = inq->params.registerSampleReadyCallback.adcSampleReadyCallback;
        	inqStatus = true;
        	break;


        case ADCD_UNREGISTER_SAMPLE_READY_CALLBACK:
        	adcdDriver.adcSampleReadyCallback = 0;
        	inqStatus = true;
        	break;

        default:
            break;
    }

    //call confirmation callback to the sender of particular inquiry that message has been processed
    if(0 != inq->inqProcessedCallback)
    {
    	inq->inqProcessedCallback(inqStatus, 0);
    }
}


static bool adcdAcquireConfigureSpi(void)
{
	//check if SPI driver requires configuring
	if(false == adcdDriver.spiReady)
	{
		//yep, it does...
		//check if SPI instance is in possession
		if(0 == adcdDriver.spiInstance)
		{
			//nope, get SPI instance
			//get ownership over SPI
			if(false == spGetMasterOwnership(&adcdDriver, &adcdReleaseSpiCallback, MAX_DELAY))
				goto err;

			//retrieve SPI handle
			if(0 == (adcdDriver.spiInstance = spGetSpiInstance(&adcdDriver)))
				goto err; //not successful in acquiring SPI instance
		}

		//initialise ADC Comms driver
		if(false == adccInitAdcCommsDriver(&adcdDriver.commDriver, adcdDriver.spiInstance))
			goto err;

		adcdDriver.spiReady = true;
	}

	return true;


err:
	//return SPI instance if in posession
	if(0 != adcdDriver.spiInstance)
		spReleaseMasterOwnership(&adcdDriver);

	//reset other fields of the driver
	adcdDriver.spiInstance = 0;
	adcdDriver.spiReady = false;

	return false;
}


static void adcdCollectAdcData(void)
{
	if(false == adcdAcquireConfigureSpi())
		return;

	//read raw ADC sample
	adccRdata(&adcdDriver.commDriver, &adcdDriver.latestRawAdcData);

	//convert raw ADC sample into value
	adcdDriver.latestConvertedAdcData = adcdConvertRawSample(adcdDriver.latestRawAdcData);

	//send data to connected module
	if(0 != adcdDriver.adcSampleReadyCallback)
		adcdDriver.adcSampleReadyCallback(adcdDriver.latestConvertedAdcData, MAX_DELAY);

	//send data to AUX serial port
	if(true == adcdDriver.sendAdcSamplesToAuxComms)
	{
		//send the ADC sample
		acMessage_t msg;
		msg.command   = COMM_ADC_SAMPLE_DATA;
		msg.paramPtr  = (uint8_t*)&adcdDriver.latestRawAdcData;
		msg.paramSize = sizeof(uint32_t);

		addMessageToQueue(&msg, 0);
	}

	return;
}


static void adcdSetRegister(uint8_t* mtx, uint8_t address, uint8_t pos, uint8_t mask, uint8_t val)
{
	mtx[address] = (uint8_t)(mtx[address] | ((val << pos) & mask));
}


static void adcdInitAdcChip(void)
{
	//take ADC chip away from reset condition
	HAL_GPIO_WritePin(ADC_RESET_GPIO_Port, ADC_RESET_Pin, GPIO_PIN_SET);

	//wait for the ADC chip power up
	vTaskDelay(16/portTICK_PERIOD_MS);

	//get SPI instance
	if(false == spGetMasterOwnership(&adcdDriver, &adcdReleaseSpiCallback, MAX_DELAY))
		errorLoop();

	//retrieve SPI handle
	if(0 == (adcdDriver.spiInstance = spGetSpiInstance(&adcdDriver)))
		errorLoop(); //not successful in acquiring SPI instance

	//initialise ADC Comms driver and SPI peripheral
	if(false == adccInitAdcCommsDriver(&adcdDriver.commDriver, adcdDriver.spiInstance))
		errorLoop();

	//adjust ADC driver field
	adcdDriver.spiReady = true;

	//delay for tCSSC (10ns)
	vTaskDelay(1/portTICK_PERIOD_MS);

	//reset ADC chip
	adccReset(&adcdDriver.commDriver);

	//disable continuous data read
	adccSdataC(&adcdDriver.commDriver);


	//prepare ADC configuration registers
	adcrRegisters_t wRegs;
	memset(&wRegs, 0, sizeof(adcrRegisters_t));

	adcdSetRegister(wRegs.mtx, ADS_REG_BCS, 	ADS_BCS_RES_Pos, 		ADS_BCS_RES_Msk, 		1);
	adcdSetRegister(wRegs.mtx, ADS_REG_BCS, 	ADS_BCS_BCS_Pos, 		ADS_BCS_BCS_Msk, 		0);

	adcdSetRegister(wRegs.mtx, ADS_REG_VBIAS, 	ADS_VBIAS_VBIAS_Pos, 	ADS_VBIAS_VBIAS_Msk, 	0);	//??

	adcdSetRegister(wRegs.mtx, ADS_REG_MUX1, 	ADS_MUX1_MUXCAL_Pos, 	ADS_MUX1_MUXCAL_Msk, 	0);	//??
	//adcdSetRegister(wData, ADS_REG_MUX1, 	ADS_MUX1_CLKSTAT_Pos, 	ADS_MUX1_CLKSTAT_Msk, 	0);

	adcdSetRegister(wRegs.mtx, ADS_REG_SYS0, 	ADS_SYS0_DR_Pos, 		ADS_SYS0_DR_Msk, 		0x6); //??
	adcdSetRegister(wRegs.mtx, ADS_REG_SYS0, 	ADS_SYS0_PGA_Pos, 		ADS_SYS0_PGA_Msk, 		1);

	adcdSetRegister(wRegs.mtx, ADS_REG_OFC0, 	ADS_OFC0_OFC_Pos, 		ADS_OFC0_OFC_Msk, 		0);
	adcdSetRegister(wRegs.mtx, ADS_REG_OFC1, 	ADS_OFC1_OFC_Pos, 		ADS_OFC1_OFC_Msk, 		0);
	adcdSetRegister(wRegs.mtx, ADS_REG_OFC2, 	ADS_OFC2_OFC_Pos, 		ADS_OFC2_OFC_Msk, 		0);

	adcdSetRegister(wRegs.mtx, ADS_REG_FSC0, 	ADS_FSC0_OFC_Pos, 		ADS_FSC0_OFC_Msk, 		0);
	adcdSetRegister(wRegs.mtx, ADS_REG_FSC1, 	ADS_FSC1_OFC_Pos, 		ADS_FSC1_OFC_Msk, 		0);
	adcdSetRegister(wRegs.mtx, ADS_REG_FSC2, 	ADS_FSC2_OFC_Pos, 		ADS_FSC2_OFC_Msk, 		0x40);

	adcdSetRegister(wRegs.mtx, ADS_REG_ID, 		ADS_ID_DRDYMODE_Pos, 	ADS_ID_DRDYMODE_Msk, 	0);
	//adcdSetRegister(wData, ADS_REG_ID, 		ADS_ID_ID_Pos, 			ADS_ID_ID_Msk, 			0);

#if 0
	//set the ADC registers
	adccWRegInq_t wregInq;
	wregInq.address		= 0;
	wregInq.dataSize 	= sizeof(adcrRegisters_t);
	wregInq.data 		= (uint8_t*)&wRegs;
	adccWReg(&adcdDriver.commDriver, &wregInq);


	//read the ADC registers
	adcrRegisters_t rRegs;
	memset(&rRegs, 0, sizeof(adcrRegisters_t));

	adccRRegInq_t rregInq;
	rregInq.address 	= 0;
	rregInq.dataSize 	= sizeof(adcrRegisters_t);
	rregInq.data 		= (uint8_t*)&rRegs;
	adccRReg(&adcdDriver.commDriver, &rregInq);


	//compare read with write
	int i;
	for(i=0; i<4; i++)
		if(rRegs.mtx[i] != wRegs.mtx[i])
		{
			//errorLoop();
			asm("nop");
		}
#endif

	//reset ADC filter and start new conversion
	adccSync(&adcdDriver.commDriver);

	//enable DRDY interrupt
	HAL_NVIC_EnableIRQ(DRDY_EXTI_IRQn);

	//delay for tSCCS (?? ns)
	vTaskDelay(1/portTICK_PERIOD_MS);
}


static void adcdSendCommand(uint8_t cmd)
{
	switch(cmd)
	{
	case ADC_CMD_WAKEUP:
		adccWakeup(&adcdDriver.commDriver);
		break;

	case ADC_CMD_SLEEP:
		adccSleep(&adcdDriver.commDriver);
		break;

	case ADC_CMD_SYNC_1:
		adccSync(&adcdDriver.commDriver);
		break;

	case ADC_CMD_RESET:
		adccReset(&adcdDriver.commDriver);
		break;

	case ADC_CMD_SYSOCAL:
		adccSysOCal(&adcdDriver.commDriver);
		break;

	case ADC_CMD_SYSGCAL:
		adccSysGCal(&adcdDriver.commDriver);
		break;

	case ADC_CMD_SELFOCAL:
		adccSelfOCal(&adcdDriver.commDriver);
		break;

	default:
		addShortMessageToQueue(COMM_ADC_COMMAND_NOT_RECOGNISED, MAX_DELAY);
		return;
		break;
	}

	addShortMessageToQueue(COMM_ADC_COMMAND_SENT, MAX_DELAY);
}


static bool adcdReleaseSpiPeripheralInq(adcdInquiry_t* inq)
{
	(void)(inq);

	//check if SPI is in possession
	if(0 == adcdDriver.spiInstance)
		return false;

	//deinitialise SPI and release the instance
	adccDeInitAdcCommsDriver(&adcdDriver.commDriver);
	spReleaseMasterOwnership(&adcdDriver);

	//clean-up ADC driver
	adcdDriver.spiInstance = 0;
	adcdDriver.spiReady = false;

	return true;
}


static bool adcdReadAdcRegistersInq(adcdInquiry_t* inq)
{
	if(false == adcdAcquireConfigureSpi())
		return false;

	//read registers from ADC
	adcrRegisters_t registers;
	adccRRegInq_t rregInq;

	rregInq.address 	= 0;
	rregInq.dataSize	= sizeof(adcrRegisters_t);
	rregInq.data		= (uint8_t*)&registers;

	if(false == adccRReg(&adcdDriver.commDriver, &rregInq))
		return false;

	if(0 != inq->params.readAdcRegisters.readAdcRegCallback)
		inq->params.readAdcRegisters.readAdcRegCallback(&registers, MAX_DELAY);

	return true;
}


static bool adcdWriteAdcRegistersInq(adcdInquiry_t* inq)
{
	if(false == adcdAcquireConfigureSpi())
		return false;

	adccWRegInq_t wregInq;
	wregInq.address		= 0;
	wregInq.dataSize 	= sizeof(adcrRegisters_t);
	wregInq.data 		= (uint8_t*)&inq->params.writeAdcRegisters.registers;

	if(false == adccWReg(&adcdDriver.commDriver, &wregInq))
		return false;

	return true;
}


static bool adcdReadRegistersToAuxInq(adcdInquiry_t* inq)
{
	(void)(inq);

	//check if SPI is configured
    if(false == adcdAcquireConfigureSpi())
    {
    	addShortMessageToQueue(COMM_ADC_REGISTERS_READ_ERROR, MAX_DELAY);
        return false;
    }

    //read registers from ADC
    adcrRegisters_t registers;
    adccRRegInq_t rregInq;

    rregInq.address 	= 0;
    rregInq.dataSize	= sizeof(adcrRegisters_t);
    rregInq.data		= (uint8_t*)&registers;

    if(false == adccRReg(&adcdDriver.commDriver, &rregInq))
    {
    	addShortMessageToQueue(COMM_ADC_REGISTERS_READ_ERROR, MAX_DELAY);
    	return false;
    }

    //prepare AUX comm message with registers
    acMessage_t msg;

    msg.command   = COMM_ADC_REGISTERS_DATA;
    msg.paramPtr  = (uint8_t*)&registers;
    msg.paramSize = sizeof(adcrRegisters_t);

    addMessageToQueue(&msg, 0);

    return true;
}


static bool adcdWriteAdcRegistersFromAuxInq(adcdInquiry_t* inq)
{
	if(false == adcdAcquireConfigureSpi())
		return false;

	adccWRegInq_t wregInq;
	wregInq.address		= 0;
	wregInq.dataSize 	= sizeof(adcrRegisters_t);
	wregInq.data 		= (uint8_t*)&inq->params.writeAdcRegisters.registers;

	if(false == adccWReg(&adcdDriver.commDriver, &wregInq))
	{
		addShortMessageToQueue(COMM_ADC_REGISTERS_NOT_SET, MAX_DELAY);
		return false;
	}

	addShortMessageToQueue(COMM_ADC_REGISTERS_SET, MAX_DELAY);
	return true;
}


float adcdConvertRawSample(uint32_t sample)
{
	int32_t val1, val2, res;
	val1 = sample & 0x7FFFFF;
	val2 = sample & (0x1<<23);

	res = val1-val2;

	return (float)res;
}



