/**
  ******************************************************************************
  * @file      	measResGuard.h
  * @author    	Michal Kalbarczyk
  * @date      	28-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/

#ifndef MEAS_RES_GUARD_H_
#define MEAS_RES_GUARD_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"

#include "resourceGuard.h"



/* ----------------------- Public Defines ---------------------------------*/
/* ----------------------- Public Type Definitions ------------------------*/
/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void mrgInitMeasurementResourceGuard(void);
bool mrgGetMasterOwnership(void* newMasterHandle, rgReleaseClb_t releaseCallback, TickType_t ticksToWait);
void mrgReleaseMasterOwnership(void* masterHandle);
bool mrgCheckMasterOwnership(void* masterHandle);


#endif /* MEAS_RES_GUARD_H_ */
