/**
  ******************************************************************************
  * @file      	dacDriver.h
  * @author    	Michal Kalbarczyk
  * @date      	31-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _DAC_DRIVER_H_
#define _DAC_DRIVER_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"


/* ----------------------- Public Defines ---------------------------------*/
#define DD_MIN_SINE_FREQ				5 		//[Hz]
#define DD_MAX_SINE_FREQ				1000 	//[Hz]

#define DD_PI 							3.14159265
#define DD_DAC_MIDDLE_OFFSET			(0xFFF >> 1)

#define DD_MAX_DAC_UPDATE_PERIOD		((float)0.00004) //[s] look into TIM4 initialisation routine description for more details


/* ----------------------- Public Type Definitions ------------------------*/

typedef enum
{
	DD_MODE_NONE,
	DD_MODE_DC_OUTPUT,
	DD_MODE_SINE_WAVE
}ddMode_t;

#if 0
typedef struct
{
	uint32_t outputValue;
}__attribute__ ((packed)) ddDcParams_t;


typedef struct
{
	float frequency;
	uint32_t p_pAmplitude;
	int32_t  dcOffset;
}__attribute__ ((packed)) ddSineParams_t;
#endif


typedef struct
{
	ddMode_t mode;

	union
	{
		struct{
			uint32_t outputValue;
		}dcOutput;

		struct{
			float frequency;
			uint32_t p_pAmplitude;
			int32_t  dcOffset;
		}sineWave;
	}params;
}__attribute__ ((packed)) ddConfig_t;


typedef struct
{
	ddMode_t mode;

	uint32_t dcOutput;

	float     frequency;
	float     p_pAmplitude;
	float     dcOffset;
	uint16_t* sineBuffer;
	uint32_t  sineBufferSize;		/*!< size in number of elements not bytes*/

	DAC_HandleTypeDef hdac1;
	DMA_HandleTypeDef hdma_dac_ch1;
	TIM_HandleTypeDef htim4;

	SemaphoreHandle_t accessSemaphore;
}ddDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void ddInitDacDriver(void);

void ddDacIrqHandler(void);
void ddDmaChannel3IrqHandler(void);

void ddDacDriver_HAL_DAC_MspInit(DAC_HandleTypeDef* hdac);
void ddDacDriver_HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac);

bool ddStartDac(void* masterHandle, ddConfig_t* config);
void ddStopDac(void* masterHandle);

bool ddStartDacAux(ddConfig_t* config);
void ddStopDacAux(void);
void ddSendSettingsToAux(void);


#ifdef CEEDLING_TEST
extern STATIC ddDriver_t ddDriver;
#endif


#endif  /*_DAC_DRIVER_H_*/
