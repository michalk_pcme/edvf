/**
  ******************************************************************************
  * @file      	gainSwitches.h
  * @author    	Michal Kalbarczyk
  * @date      	25-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


#ifndef _GAIN_SWITCHES_H_
#define _GAIN_SWITCHES_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"


/* ----------------------- Public Defines ---------------------------------*/
#define GS_SW_GAIN0_MSK					((uint16_t) 1<<0)
#define GS_SW_GAIN1_MSK					((uint16_t) 1<<1)
#define GS_SW_ROD_STATE_MSK				((uint16_t) 1<<2)
#define GS_SW_OP_SELECT_MSK				((uint16_t) 1<<3)
#define GS_SW_GAIN5L_MSK				((uint16_t) 1<<4)
#define GS_SW_GAIN5H_MSK				((uint16_t) 1<<5)


/* ----------------------- Public Type Definitions ------------------------*/

typedef struct
{
	uint16_t data;
}__attribute__ ((packed)) gsDataParams_t;


typedef struct
{
	SemaphoreHandle_t accessSemaphore;
	uint16_t switchesState;
}gsDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void gsInitGainSwitches(void);

void gsSetSwitches(void* masterHandle, uint16_t switchesState);
void gsSetSwitchesAux(uint16_t switchesState);
void gsGetSwitches(uint16_t* switchesState);
void gsGetSwitchState(uint16_t switchMask, bool* state);


#ifdef CEEDLING_TEST
extern STATIC gsDriver_t gsDriver;
#endif

#endif  /*_GAIN_SWITCHES_H_*/
