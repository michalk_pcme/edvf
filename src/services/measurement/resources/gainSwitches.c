/**
  ******************************************************************************
  * @file      	gainSwitches.c
  * @author    	Michal Kalbarczyk
  * @date      	25-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"

#include "systemInit.h"
#include "gainSwitches.h"
#include "measResGuard.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
STATIC gsDriver_t gsDriver;


/* ----------------------- Private Prototypes -----------------------------*/
STATIC void gsSetSwitchesPrivate(uint16_t switchesState);
STATIC void gsUpdateSwitchesLevels(void);
STATIC GPIO_PinState gsPinState(uint16_t state);


/* ----------------------- Public Functions -------------------------------*/
void gsInitGainSwitches(void)
{
	//create semaphores
	vSemaphoreCreateBinary(gsDriver.accessSemaphore);

	gsDriver.switchesState = 0;

	gsUpdateSwitchesLevels();
}


void gsSetSwitches(void* masterHandle, uint16_t switchesState)
{
	if(false == mrgCheckMasterOwnership(masterHandle))
		return;

	gsSetSwitchesPrivate(switchesState);
}


void gsSetSwitchesAux(uint16_t switchesState)
{
	gsSetSwitchesPrivate(switchesState);
}


void gsGetSwitches(uint16_t* switchesState)
{
	xSemaphoreTake(gsDriver.accessSemaphore, MAX_DELAY);

	*switchesState = gsDriver.switchesState;

	xSemaphoreGive(gsDriver.accessSemaphore);
}


void gsGetSwitchState(uint16_t switchMask, bool* state)
{
	xSemaphoreTake(gsDriver.accessSemaphore, MAX_DELAY);

	if(0 != (gsDriver.switchesState & switchMask))
		*state = true;
	else
		*state = false;

	xSemaphoreGive(gsDriver.accessSemaphore);
}


/* ----------------------- Private Functions ------------------------------*/

STATIC void gsSetSwitchesPrivate(uint16_t switchesState)
{
	xSemaphoreTake(gsDriver.accessSemaphore, MAX_DELAY);

	gsDriver.switchesState = switchesState;

	gsUpdateSwitchesLevels();

	xSemaphoreGive(gsDriver.accessSemaphore);
}


STATIC void gsUpdateSwitchesLevels(void)
{
	uint16_t temp;

	temp = gsDriver.switchesState;

	HAL_GPIO_WritePin(GAIN_0_GPIO_Port, 	GAIN_0_Pin, 	gsPinState(temp & GS_SW_GAIN0_MSK));
	HAL_GPIO_WritePin(GAIN_1_GPIO_Port, 	GAIN_1_Pin, 	gsPinState(temp & GS_SW_GAIN1_MSK));
	HAL_GPIO_WritePin(ROD_STATE_GPIO_Port, 	ROD_STATE_Pin, 	gsPinState(temp & GS_SW_ROD_STATE_MSK));
	HAL_GPIO_WritePin(OP_SELECT_GPIO_Port, 	OP_SELECT_Pin, 	gsPinState(temp & GS_SW_OP_SELECT_MSK));
	HAL_GPIO_WritePin(GAIN5L_GPIO_Port, 	GAIN5L_Pin, 	gsPinState(temp & GS_SW_GAIN5L_MSK));
	HAL_GPIO_WritePin(GAIN5H_GPIO_Port, 	GAIN5H_Pin, 	gsPinState(temp & GS_SW_GAIN5H_MSK));
}


STATIC inline GPIO_PinState gsPinState(uint16_t state)
{
	if(0 != state)
		return GPIO_PIN_SET;
	else
		return GPIO_PIN_RESET;
}

