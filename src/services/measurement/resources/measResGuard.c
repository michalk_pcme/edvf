/**
  ******************************************************************************
  * @file      	measResGuard.c
  * @author    	Michal Kalbarczyk
  * @date      	28-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"

#include "measResGuard.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
rgDriver_t mrgGuard;


/* ----------------------- Public Functions -------------------------------*/
/* ----------------------- Private Functions ------------------------------*/

void mrgInitMeasurementResourceGuard(void)
{
	rgDriverConstructor(&mrgGuard);
}


bool mrgGetMasterOwnership(void* newMasterHandle, rgReleaseClb_t releaseCallback, TickType_t ticksToWait)
{
	return rgRegisterNewMaster(
			&mrgGuard,
			newMasterHandle,
			releaseCallback,
			ticksToWait
			);
}


void mrgReleaseMasterOwnership(void* masterHandle)
{
	rgUnregisterMaster(&mrgGuard, masterHandle);
}


bool mrgCheckMasterOwnership(void* masterHandle)
{
	return rgCheckMasterHandle(&mrgGuard, masterHandle);
}



