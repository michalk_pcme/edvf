/**
  ******************************************************************************
  * @file      	dacDriver.c
  * @author    	Michal Kalbarczyk
  * @date      	31-Jan-2018
  * @copyright 	
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details	
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"

#include "systemInit.h"
#include "dacDriver.h"

#include "acDmaAux.h"
#include "acCommands.h"

#include <math.h>
#include <string.h>

#include "measResGuard.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/



/* ----------------------- Private Variables ------------------------------*/
STATIC ddDriver_t ddDriver;


/* ----------------------- Private Prototypes -----------------------------*/
STATIC bool ddStartDacPrivate(ddConfig_t* config);
STATIC void ddStopDacPrivate(void);

STATIC bool ddStartDcOutputMode(ddConfig_t* config);
STATIC void ddInitHardwareDcOutput(void);
STATIC void ddDeInitHardwareDcOutput(void);

STATIC bool ddStartSineWaveMode(ddConfig_t* config);
STATIC void ddInitHardwareSineOutput(void);
STATIC void ddDeInitHardwareSineOutput(void);


/* ----------------------- Public Functions -------------------------------*/
void ddInitDacDriver(void)
{
	//create semaphores
	vSemaphoreCreateBinary(ddDriver.accessSemaphore);

	ddDriver.mode 				= DD_MODE_NONE;

	ddDriver.dcOutput 			= 0;

	ddDriver.frequency        	= 0;
	ddDriver.p_pAmplitude 		= 0;
	ddDriver.dcOffset 			= 0;

	ddDriver.sineBuffer 		= 0;
	ddDriver.sineBufferSize 	= 0;
}


/**
 * @brief Interrupt handler.
 */
void ddDacIrqHandler(void)
{
	HAL_DAC_IRQHandler(&ddDriver.hdac1);
}


/**
 * @brief Interrupt handler.
 * @note  In regular operation this interrupt will be most likely
 * 		  triggered only by DMA underrun error. It would mean that
 * 		  DAC is being driven too fast by TIM4.
 */
void ddDmaChannel3IrqHandler(void)
{
	HAL_DMA_IRQHandler(&ddDriver.hdma_dac_ch1);
}


/**
 * @brief Hardware initialisation that has been moved from st32l4xx_hal_msp.c
 * 		  file for the code clarity. This function gets called by HAL layer
 * 		  during peripheral de-/initialisation.
 */
void ddDacDriver_HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	DMA_HandleTypeDef* hdma_dac_ch1;

	if(hdac->Instance != DAC1)
		return;

    /* Peripheral clock enable */
#ifndef CEEDLING_TEST
	//direct calls to the registers should be avoided with Ceedling - segmentation faults
    __HAL_RCC_DAC1_CLK_ENABLE();
#endif

    /**DAC1 GPIO Configuration
    PA4     ------> DAC1_OUT1
    */
    GPIO_InitStruct.Pin = GPIO_PIN_4;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* DAC1 DMA Init */
    /* DAC_CH1 Init */
    hdma_dac_ch1 = &ddDriver.hdma_dac_ch1;
    hdma_dac_ch1->Instance 					= DMA1_Channel3;
    hdma_dac_ch1->Init.Request 				= DMA_REQUEST_6;
    hdma_dac_ch1->Init.Direction 			= DMA_MEMORY_TO_PERIPH;
    hdma_dac_ch1->Init.PeriphInc 			= DMA_PINC_DISABLE;
    hdma_dac_ch1->Init.MemInc 				= DMA_MINC_ENABLE;
    hdma_dac_ch1->Init.PeriphDataAlignment 	= DMA_PDATAALIGN_HALFWORD;
    hdma_dac_ch1->Init.MemDataAlignment 	= DMA_MDATAALIGN_HALFWORD;
    hdma_dac_ch1->Init.Mode 				= DMA_CIRCULAR;
    hdma_dac_ch1->Init.Priority 			= DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(hdma_dac_ch1) != HAL_OK)
    	errorLoop();

    __HAL_LINKDMA(hdac, DMA_Handle1, *hdma_dac_ch1);

    /* DAC1 interrupt Init */
    HAL_NVIC_SetPriority(TIM6_DAC_IRQn, DAC_ISR_PRIORITY, 0);
    HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
}


/**
 * @brief Hardware initialisation that has been moved from st32l4xx_hal_msp.c
 * 		  file for the code clarity. This function gets called by HAL layer
 * 		  during peripheral de-/initialisation.
 */
void ddDacDriver_HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac)
{
	if(hdac->Instance != DAC1)
		return;

    /* Peripheral clock disable */
    __HAL_RCC_DAC1_CLK_DISABLE();

    /**DAC1 GPIO Configuration
    PA4     ------> DAC1_OUT1
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_4);

    /* DAC1 DMA DeInit */
    HAL_DMA_DeInit(hdac->DMA_Handle1);

    /* DAC1 interrupt DeInit */
    HAL_NVIC_DisableIRQ(TIM6_DAC_IRQn);
}


bool ddStartDac(void* masterHandle, ddConfig_t* config)
{
	bool status;

	if(false == mrgCheckMasterOwnership(masterHandle))
		return false;

	xSemaphoreTake(ddDriver.accessSemaphore, MAX_DELAY);

	status = ddStartDacPrivate(config);

	xSemaphoreGive(ddDriver.accessSemaphore);

	return status;
}


void ddStopDac(void* masterHandle)
{
	if(false == mrgCheckMasterOwnership(masterHandle))
		return;

	xSemaphoreTake(ddDriver.accessSemaphore, MAX_DELAY);

	ddStopDacPrivate();

	xSemaphoreGive(ddDriver.accessSemaphore);
}


bool ddStartDacAux(ddConfig_t* config)
{
	bool status;

	xSemaphoreTake(ddDriver.accessSemaphore, MAX_DELAY);

	status = ddStartDacPrivate(config);

	xSemaphoreGive(ddDriver.accessSemaphore);

	return status;
}


void ddStopDacAux(void)
{
	xSemaphoreTake(ddDriver.accessSemaphore, MAX_DELAY);

	ddStopDacPrivate();

	xSemaphoreGive(ddDriver.accessSemaphore);
}


void ddSendSettingsToAux(void)
{
	ddConfig_t config;

	//clear the config structure
	memset(&config, 0, sizeof(ddConfig_t));

	//gain exclusive access to the DAC ddDriver
	xSemaphoreTake(ddDriver.accessSemaphore, MAX_DELAY);

	//assemble settings into structure accordingly to the operating mode
	config.mode = ddDriver.mode;

	switch(ddDriver.mode)
	{
	case DD_MODE_DC_OUTPUT:
		config.params.dcOutput.outputValue = ddDriver.dcOutput;
		break;

	case DD_MODE_SINE_WAVE:
		config.params.sineWave.dcOffset 	= (int32_t) ddDriver.dcOffset;
		config.params.sineWave.frequency 	= (float)   ddDriver.frequency;
		config.params.sineWave.p_pAmplitude = (uint32_t)ddDriver.p_pAmplitude;
		break;

	case DD_MODE_NONE:
	default:
		break;
	}

	//release access to the DAC ddDriver
	xSemaphoreGive(ddDriver.accessSemaphore);

	//send the message over AUX
	acMessage_t msg;
	msg.command = COMM_DD_SETTINGS_DATA;
	msg.paramPtr = (uint8_t*)&config;
	msg.paramSize = sizeof(ddConfig_t);

	addMessageToQueue(&msg, 0);
}


/* ----------------------- Private Functions ------------------------------*/

STATIC bool ddStartDacPrivate(ddConfig_t* config)
{
	bool status = false;

	//check if DAC is already in use
	if(DD_MODE_NONE != ddDriver.mode)
		ddStopDacPrivate();

	switch(config->mode)
	{
	case DD_MODE_DC_OUTPUT:
		if(true == ddStartDcOutputMode(config))
		{
			addShortMessageToQueue(COMM_DD_DAC_STARTED, 0);
			status = true;
		}
		break;

	case DD_MODE_SINE_WAVE:
		if(true == ddStartSineWaveMode(config))
		{
			addShortMessageToQueue(COMM_DD_DAC_STARTED, 0);
			status = true;
		}
		break;

	case DD_MODE_NONE:
	default:
		break;
	}

	return status;
}


STATIC void ddStopDacPrivate(void)
{
	switch(ddDriver.mode)
	{
	case DD_MODE_DC_OUTPUT:
		ddDeInitHardwareDcOutput();
		ddDriver.mode = DD_MODE_NONE;

		addShortMessageToQueue(COMM_DD_DAC_STOPPED, 0);
		break;

	case DD_MODE_SINE_WAVE:
		ddDeInitHardwareSineOutput();

		//clear the sinusoidal buffer if allocated
		if(0 != ddDriver.sineBuffer)
		{
			safeFree(ddDriver.sineBuffer);
			ddDriver.sineBuffer 		= 0;
			ddDriver.sineBufferSize 	= 0;
		}

		ddDriver.mode = DD_MODE_NONE;

		addShortMessageToQueue(COMM_DD_DAC_STOPPED, 0);
		break;

	case DD_MODE_NONE:
	default:
		ddDriver.mode = DD_MODE_NONE;
		break;
	}
}


STATIC bool ddStartDcOutputMode(ddConfig_t* config)
{
	//initialise hardware for DC mode
	ddInitHardwareDcOutput();

	//set the output value to the DAC
	HAL_DAC_SetValue(&ddDriver.hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, config->params.dcOutput.outputValue);

	//set ddDriver fields
	ddDriver.dcOutput = config->params.dcOutput.outputValue;
	ddDriver.mode 	= DD_MODE_DC_OUTPUT;

	return true;
}


STATIC void ddInitHardwareDcOutput(void)
{
	DAC_ChannelConfTypeDef sConfig;

	ddDriver.hdac1.Instance = DAC1;
	if (HAL_DAC_Init(&ddDriver.hdac1) != HAL_OK)
		errorLoop();

	//DAC channel OUT1 config
	sConfig.DAC_SampleAndHold 			= DAC_SAMPLEANDHOLD_ENABLE;
	sConfig.DAC_Trigger 				= DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer 			= DAC_OUTPUTBUFFER_ENABLE;
	sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_ENABLE;
	sConfig.DAC_UserTrimming		 	= DAC_TRIMMING_FACTORY;

	if (HAL_DAC_ConfigChannel(&ddDriver.hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
		errorLoop();

	if (HAL_DAC_Start(&ddDriver.hdac1, DAC_CHANNEL_1) != HAL_OK)
		errorLoop();
}


STATIC void ddDeInitHardwareDcOutput(void)
{
	HAL_DAC_Stop(&ddDriver.hdac1, DAC_CHANNEL_1);

	HAL_DAC_DeInit(&ddDriver.hdac1);
}


/**
 * @brief This function creates a new buffer to store samples of a generated
 * 		  sine wave signal. After signal calculations buffer gets sent to HAL
 * 		  to start sending it over DMA to DAC output.
 */
STATIC bool ddStartSineWaveMode(ddConfig_t* config)
{
	uint32_t i;
	uint32_t bufSize;

	//check if frequency is within operating range
	if(config->params.sineWave.frequency < DD_MIN_SINE_FREQ) goto err;
	if(config->params.sineWave.frequency > DD_MAX_SINE_FREQ) goto err;

	//calculate number of sine wave samples
	bufSize = (uint32_t)(1/(config->params.sineWave.frequency * DD_MAX_DAC_UPDATE_PERIOD));

	//check if number of samples is within operating range of DMA
	if(bufSize > 0xFFFF)
		goto err;

	//check if sinusoidal buffer has been already allocated (should not!)
	if(0 != ddDriver.sineBuffer)
		goto err;

	//assign a buffer for sine wave samples
	if(0 == (ddDriver.sineBuffer = safeMalloc( sizeof(uint16_t) * bufSize )))
		goto err;

	ddDriver.sineBufferSize = bufSize;

	//prepare the sine wave
	float deg;
	float dcOffset 	= (float)config->params.sineWave.dcOffset + (float)DD_DAC_MIDDLE_OFFSET;
	float amplitude = (float)config->params.sineWave.p_pAmplitude/2;
	float temp 		= (float)bufSize;

	for(i=0; i<ddDriver.sineBufferSize; i++)
	{
		deg = (float)i * (float)(360.0/temp);

		ddDriver.sineBuffer[i] = (uint16_t)(sin(deg * DD_PI/180) * amplitude + dcOffset);
	}

	//initialise hardware for sine wave mode
	ddInitHardwareSineOutput();

	//start the DAC
	if(HAL_OK != HAL_TIM_Base_Start(&ddDriver.htim4))
		errorLoop();

	if(HAL_OK != HAL_DAC_Start_DMA(&ddDriver.hdac1, DAC_CHANNEL_1, (uint32_t*)&ddDriver.sineBuffer[0], ddDriver.sineBufferSize, DAC_ALIGN_12B_R))
		errorLoop();

	//fill up ddDriver's fields
	ddDriver.frequency    = (float)config->params.sineWave.frequency;
	ddDriver.dcOffset 	= (float)config->params.sineWave.dcOffset;
	ddDriver.p_pAmplitude = (float)config->params.sineWave.p_pAmplitude;
	ddDriver.mode 		= DD_MODE_SINE_WAVE;

	return true;


err:
	if(0 != ddDriver.sineBuffer)
		safeFree(ddDriver.sineBuffer);

	ddDriver.sineBuffer 		= 0;
	ddDriver.sineBufferSize 	= 0;

	return false;
}


/**
 * @brief Hardware for sinusoidal waveform consists of DAC, DMA and Timer4.
 * 		  Timer 4 update event is used for trigger for DAC to process new
 * 		  data that is provided via DMA from a buffer placed in RAM.
 * 		  Data is provided in a circular manner to generate continuous sine
 * 		  wave.
 */
STATIC void ddInitHardwareSineOutput(void)
{
	DAC_ChannelConfTypeDef sConfig;

	//DAC Initialization
	ddDriver.hdac1.Instance = DAC1;
	if (HAL_DAC_Init(&ddDriver.hdac1) != HAL_OK)
		errorLoop();

	//DAC channel OUT1 config
	sConfig.DAC_SampleAndHold 			= DAC_SAMPLEANDHOLD_DISABLE;
	sConfig.DAC_Trigger 				= DAC_TRIGGER_T4_TRGO;
	sConfig.DAC_OutputBuffer 			= DAC_OUTPUTBUFFER_ENABLE;
	sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_DISABLE;
	sConfig.DAC_UserTrimming 			= DAC_TRIMMING_FACTORY;
	if (HAL_DAC_ConfigChannel(&ddDriver.hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
		errorLoop();



	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;
	TIM_HandleTypeDef* htim4;

#ifdef SYSTEM_CLOCK_8MHZ
#error "Create a proper initalisation for this requency"
#endif

#ifdef SYSTEM_CLOCK_8_192MHZ
	//DAC triggering timer initalisation
	// max_DAC_update_freq = 25kHz -> max_DAC_update_period = 40us
	// timer tick [us] = 1 / (APB1_CLK_period/(prescaler + 1)) = 1 / ( 8.192MHz/(0 + 1)) = 0.122us
	// TIM_PERIOD = max_DAC_update_period / timer tick [us] = 40us/0.122us = 327

	htim4 = &ddDriver.htim4;
	htim4->Instance 			= TIM4;
	htim4->Init.Prescaler 		= 0;
	htim4->Init.CounterMode 	= TIM_COUNTERMODE_UP;
	htim4->Init.Period 			= 327;
	htim4->Init.ClockDivision 	= TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init(htim4);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(htim4, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(htim4, &sMasterConfig);
#endif

#ifdef SYSTEM_CLOCK_16MHZ
#error "Create a proper initalisation for this requency"
#endif

#ifdef SYSTEM_CLOCK_80MHZ
#error "Create a proper initalisation for this requency"
#endif


}


STATIC void ddDeInitHardwareSineOutput(void)
{
	HAL_TIM_Base_Stop(&ddDriver.htim4);

	HAL_TIM_Base_DeInit(&ddDriver.htim4);

	HAL_DAC_Stop_DMA(&ddDriver.hdac1, DAC_CHANNEL_1);

	HAL_DAC_DeInit(&ddDriver.hdac1);
}



