/**
  ******************************************************************************
  * @file      	measProgMgr.c
  * @author    	Michal Kalbarczyk
  * @date      	5-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "systemInit.h"
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>

#include "regularMeasurements.h"
#include "adcDriver.h"
#include "gainSwitches.h"

#include "rmsAlgorithm.h"
#include "firFilter.h"

#include "acDmaAux.h"
#include "acCommands.h"

#include "timestamps.h"
#include "timeoutTimers.h"

#include "measResGuard.h"


/* ----------------------- Private Defines --------------------------------*/
#define RM_NBR_OF_FIR_COEFS				32
#define RM_ZERO_CROSSING_PERIOD			1000


/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/

/**
 * Pragmas used to suppress warnings of double to float conversion.
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-conversion"
static float firCoefs[RM_NBR_OF_FIR_COEFS] = {
		 0.001533491022156642, -0.001888311544501797,  0.001713449997280088, -0.0001583074464945825,
		-0.003444993345788122,  0.008445884576026748, -0.01220721839146596,   0.0107046978629928,
		-0.0005492194527706845,-0.01831467822231439,   0.0405865063829215,   -0.05543661403965807,
		 0.04815927677380113,  -0.0009188852171992643,-0.1211665881917967,    0.6029415092368108,
		 0.6029415092368108,   -0.1211665881917967,   -0.0009188852171992646, 0.04815927677380113,
		-0.05543661403965807,   0.04058650638292149,  -0.0183146782223144,   -0.0005492194527706849,
		 0.0107046978629928,   -0.01220721839146596,   0.008445884576026748, -0.003444993345788122,
		-0.0001583074464945825, 0.001713449997280087, -0.001888311544501799,  0.001533491022156641
};
#pragma GCC diagnostic pop


static rmDriver_t driver;


/* ----------------------- Private Prototypes -----------------------------*/
static void rmProcessInquiry(rmInquiry_t*);

static bool rmWaitForAdcInqProcessedCallback(void);
static BaseType_t rmUnregisterMasterCallback(uint32_t ticksToWait);
static BaseType_t rmAdcSampleReadyCallback(float adcSample, uint32_t ticksToWait);
static BaseType_t rmInqProcessedCallback(bool inqResult, uint32_t ticksToWait);

static void rmVelocityCalculations(float sample);

static void rmNewAdcSampleInq(rmInquiry_t* inq);
static void rmStopModuleInq(rmInquiry_t* inq);
static void rmStartModuleInq(rmInquiry_t* inq);
static void rmSetRmsParametersInq(rmInquiry_t* inq);


/* ----------------------- Public Functions -------------------------------*/

void rmInitRegularMeasurementsDriver(void)
{
    //initialise task
    xTaskCreate((TaskFunction_t) rmTask,
                "Regular measurements",
				RM_TASK_SIZE, NULL, REGULAR_MEASUREMENTS_TASK_PRIORITY, NULL);

    //initialise messaging queue
    driver.inquiryQueue = xQueueCreate(RM_INQUIRY_QUEUE_SIZE, sizeof(rmInquiry_t));

    //initialise semaphores
    vSemaphoreCreateBinary(driver.adcInquiryProcessedSemaphore);
    xSemaphoreTake(driver.adcInquiryProcessedSemaphore, MAX_DELAY);

    //initialise driver
    driver.configured 			= false;
    driver.waitingForNewSample	= false;
    driver.masterOfAdc 			= false;
    driver.sendMeasDataOverAux 	= false;
    driver.rmsValue 			= 0;
    driver.adcInquiryResult 	= false;
    driver.windowSize			= 1;
    driver.rmsValUpdatePrescaler = 1;
    driver.rmsValUpdateCtr		= 0;

    rmsPreSetDriver(&driver.rmsDriver);
    firPreSetDriver(&driver.firDriver);
}


void rmTask(void)
{
	rmInquiry_t inq;

    while(1)
    {
        //process inquiry queue
        if(pdPASS == xQueueReceive(driver.inquiryQueue, &inq, MAX_DELAY))
        {
        	rmProcessInquiry(&inq);
        }
    }
}


BaseType_t rmStartModule(uint32_t ticksToWait)
{
	rmInquiry_t inq;

    inq.inqType = RM_INQ_START_MODULE;

    //add inquiry to the queue for further processing
    return xQueueSend(driver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t rmStopModule(uint32_t ticksToWait)
{
	rmInquiry_t inq;

    inq.inqType = RM_INQ_STOP_MODULE;

    //add inquiry to the queue for further processing
    return xQueueSend(driver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t rmStartSendingMeasData(uint32_t ticksToWait)
{
	rmInquiry_t inq;

    inq.inqType = RM_START_SENDING_MEAS_DATA;

    //add inquiry to the queue for further processing
    return xQueueSend(driver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t rmStopSendingMeasData(uint32_t ticksToWait)
{
	rmInquiry_t inq;

    inq.inqType = RM_STOP_SENDING_MEAS_DATA;

    //add inquiry to the queue for further processing
    return xQueueSend(driver.inquiryQueue, &inq, ticksToWait);
}


BaseType_t rmSetRmsParameters(rmRmsParams_t* parameters, uint32_t ticksToWait)
{
	rmInquiry_t inq;

	if(0 == parameters)
		return pdFALSE;

    inq.inqType = RM_SET_RMS_PARAMETERS;
    inq.params.setRmsParameters.params = *parameters;

    //add inquiry to the queue for further processing
    return xQueueSend(driver.inquiryQueue, &inq, ticksToWait);
}


/* ----------------------- Private Functions ------------------------------*/

static void rmProcessInquiry(rmInquiry_t* inq)
{
	switch(inq->inqType)
	{
	case RM_INQ_NEW_ADC_SAMPLE:
		rmNewAdcSampleInq(inq);
		break;

	case RM_INQ_START_MODULE:
		rmStartModuleInq(inq);
		break;

	case RM_INQ_STOP_MODULE:
		rmStopModuleInq(inq);
		break;

	case RM_START_SENDING_MEAS_DATA:
		driver.sendMeasDataOverAux = true;
		break;

	case RM_STOP_SENDING_MEAS_DATA:
		driver.sendMeasDataOverAux = false;
		break;

	case RM_SET_RMS_PARAMETERS:
		rmSetRmsParametersInq(inq);
		break;

	default:
		break;
	}
}


static bool rmWaitForAdcInqProcessedCallback(void)
{
	volatile bool temp;

	xSemaphoreTake(driver.adcInquiryProcessedSemaphore, MAX_DELAY);
	temp = driver.adcInquiryResult;
	if(false == temp)
		asm("nop");
	return temp;
}


/**
 * @brief Called by ADC Driver whenever inquiry has been processed.
 */
static BaseType_t rmInqProcessedCallback(bool inqResult, uint32_t ticksToWait)
{
	(void)(ticksToWait);

	driver.adcInquiryResult = inqResult;
	xSemaphoreGive(driver.adcInquiryProcessedSemaphore);

	return pdTRUE;
}


/**
 * @brief This function should be called by ADC Driver whenever it wants to shake off this module's
 * 		  ownership over it.
 */
static BaseType_t rmUnregisterMasterCallback(uint32_t ticksToWait)
{
    return rmStopModule(ticksToWait);
}


/**
 * @brief This function should be registered with ADC Driver. ADC Driver is going to call it whenever
 * 		  new ADC sample get collected.
 */
static BaseType_t rmAdcSampleReadyCallback(float adcSample, uint32_t ticksToWait)
{
	rmInquiry_t inq;

    inq.inqType = RM_INQ_NEW_ADC_SAMPLE;
    inq.params.newAdcSample.sample = adcSample;

    //add inquiry to the queue for further processing
    return xQueueSend(driver.inquiryQueue, &inq, ticksToWait);
}


static void rmVelocityCalculations(float sample)
{
	bool sampleSign;

	//get sample value sign
	sampleSign = true;
	if(sample < 0)
		sampleSign = false;

	//establish if zero has been crossed
	if(driver.prevSampSign != sampleSign)
		driver.zeroCrossingCtr++;

	//copy last sample sign information
	driver.prevSampSign = sampleSign;

	//check if enough time has passed to establish number of zero crossings per second
	if(true == hasTimeoutTimerElapsed(TT_RM_ZERO_CROSSING))
	{
		setTimeoutTimer(TT_RM_ZERO_CROSSING, RM_ZERO_CROSSING_PERIOD);

		//send data with zero crossing counter
		if(true == driver.sendMeasDataOverAux)
		{
			rmVelocityParams_t params;
			params.zeroCrossingCtr = driver.zeroCrossingCtr;

			acMessage_t msg;
			msg.command 	= COMM_RM_ZERO_CROSSINGS_DATA;
			msg.paramPtr 	= (uint8_t*)&params;
			msg.paramSize 	= sizeof(rmVelocityParams_t);

			addMessageToQueue(&msg, 0);
		}

		//reset the counter
		driver.zeroCrossingCtr = 0;
	}
}


static void rmNewAdcSampleInq(rmInquiry_t* inq)
{
	rmMeasData_t* measData;
	float temp;

	if(false == driver.configured)
		return;

	if(false == driver.waitingForNewSample)
		return;

	collectTimestamp(1);

	measData = &driver.measData;

	//get ADC sample
	measData->rawSample = inq->params.newAdcSample.sample;

	//push it through FIR filter
	firAddNewSample(&driver.firDriver, inq->params.newAdcSample.sample);
	temp = firGetFIRValue(&driver.firDriver);
	measData->firOut = temp;

	//calculate velocity
	rmVelocityCalculations(temp);

	//push the filtered value through RMS calculations
	rmsAddNewSampleOptimised(&driver.rmsDriver, temp);
	temp = rmsGetRMSValue(&driver.rmsDriver);
	measData->rmsOut = temp;

	//store locally RMS value
	driver.rmsValue = temp;

	//check if it is time to send data on the output
	driver.rmsValUpdateCtr++;
	if(driver.rmsValUpdateCtr == driver.rmsValUpdatePrescaler)
	{
		if(true == driver.sendMeasDataOverAux)
		{
			acMessage_t msg;
			msg.command 	= COMM_RM_MEAS_DATA;
			msg.paramPtr 	= (uint8_t*)measData;
			msg.paramSize 	= sizeof(rmMeasData_t);

			addMessageToQueue(&msg, 0);
		}

		driver.rmsValUpdateCtr = 0;
	}

	//collectTimestamp(2);
}


static void rmStartModuleInq(rmInquiry_t* inq)
{
	(void)(inq);

	if(true == driver.configured)
		return;

	//configure RMS driver
	rmsConfig_t rmsConfig;
	rmsConfig.nbrOfSamplesPerBlock 	= 1;
	rmsConfig.nbrOfBlocks 			= driver.windowSize;
	if(false == rmsConfigureDriver(&driver.rmsDriver, &rmsConfig))
		errorLoop();

	//configure FIR driver
	firConfig_t firConfig;
	firConfig.firCoefs 		= &firCoefs[0];
	firConfig.nbrOfCoefs 	= RM_NBR_OF_FIR_COEFS;
	if(false == firConfigureDriver(&driver.firDriver, &firConfig))
		errorLoop();

	//configure ADC Driver
	if(pdFALSE == mrgGetMasterOwnership(&driver, &rmUnregisterMasterCallback, MAX_DELAY))
		errorLoop();

	driver.masterOfAdc = true;

	if(pdFALSE == adcdRegisterSampleReadyCallback(&driver, &rmInqProcessedCallback, &rmAdcSampleReadyCallback, MAX_DELAY))
		errorLoop();
	if(false == rmWaitForAdcInqProcessedCallback())
		errorLoop();

	//configure gain switches
	//gsSetSwitches(GS_SW_GAIN0_MSK | GS_SW_ROD_STATE_MSK | GS_SW_OP_SELECT_MSK | GS_SW_GAIN5L_MSK | GS_SW_GAIN5H_MSK);

	setTimeoutTimer(TT_RM_ZERO_CROSSING, RM_ZERO_CROSSING_PERIOD);
	driver.prevSampSign = true;
	driver.zeroCrossingCtr = 0;

	driver.rmsValUpdateCtr = 0;
	driver.waitingForNewSample = true;
	driver.configured = true;

	addShortMessageToQueue(COMM_RM_MODULE_STARTED, MAX_DELAY);
}


static void rmStopModuleInq(rmInquiry_t* inq)
{
	(void)(inq);

	if(false == driver.configured)
		return;

	//unregister module from ADC Driver
	adcdUnregisterSampleReadyCallback(&driver, &rmInqProcessedCallback, &rmAdcSampleReadyCallback, MAX_DELAY);
	if(false == rmWaitForAdcInqProcessedCallback()) return;

	if(true == driver.masterOfAdc){
		mrgReleaseMasterOwnership(&driver);
		driver.masterOfAdc = false;
	}

	//configure gain switches
	//gsSetSwitches(0);

	//reset FIR, RMS drivers
	rmsReSetDriver(&driver.rmsDriver);
	firReSetDriver(&driver.firDriver);

	driver.waitingForNewSample = false;
	driver.configured = false;

	addShortMessageToQueue(COMM_RM_MODULE_STOPPED, MAX_DELAY);
}


static void rmSetRmsParametersInq(rmInquiry_t* inq)
{
	//copy parameters locally
	driver.windowSize 			 = inq->params.setRmsParameters.params.windowSize;
	driver.rmsValUpdatePrescaler = inq->params.setRmsParameters.params.rmsValUpdatePrescaler;

	//make sure that prescaler is at least 1
	if(0 == driver.rmsValUpdatePrescaler)
		driver.rmsValUpdatePrescaler = 1;

	driver.rmsValUpdateCtr = 0;

	//send confirmation message
	addShortMessageToQueue(COMM_RM_RMS_PARAMETERS_SET, MAX_DELAY);

	//check if driver is configured
	if(false == driver.configured)
		return;

	//reset RMS driver
	rmsReSetDriver(&driver.rmsDriver);

	//configure RMS driver with new settings
	rmsConfig_t rmsConfig;
	rmsConfig.nbrOfSamplesPerBlock 	= 1;
	rmsConfig.nbrOfBlocks 			= driver.windowSize;
	if(false == rmsConfigureDriver(&driver.rmsDriver, &rmsConfig))
		errorLoop();

	resetTimeoutTimer(TT_RM_ZERO_CROSSING);
}


