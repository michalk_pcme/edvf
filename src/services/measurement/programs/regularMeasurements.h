/**
  ******************************************************************************
  * @file      	measProgMgr.h
  * @author    	Michal Kalbarczyk
  * @date      	5-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/

#ifndef REGULAR_MEASUREMENTS_H_
#define REGULAR_MEASUREMENTS_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <queue.h>
#include <semphr.h>

#include "rmsAlgorithm.h"
#include "firFilter.h"


/* ----------------------- Public Defines ---------------------------------*/
#define RM_TASK_SIZE						1024
#define RM_INQUIRY_QUEUE_SIZE				4


/* ----------------------- Public Type Definitions ------------------------*/

typedef struct
{
	uint32_t windowSize;
	uint32_t rmsValUpdatePrescaler;
}__attribute__ ((packed)) rmRmsParams_t;


typedef struct
{
	uint32_t zeroCrossingCtr;
}__attribute__ ((packed)) rmVelocityParams_t;


typedef struct
{
	float rawSample;
	float firOut;
	float rmsOut;
}__attribute__ ((packed)) rmMeasData_t;


typedef enum
{
	RM_INQ_NEW_ADC_SAMPLE,
	RM_INQ_START_MODULE,
	RM_INQ_STOP_MODULE,
	RM_START_SENDING_MEAS_DATA,
	RM_STOP_SENDING_MEAS_DATA,
	RM_SET_RMS_PARAMETERS
}rmInqTypes_t;


typedef struct
{
	rmInqTypes_t inqType;

	union
	{
	    struct
	    {
	    	float sample;
	    }newAdcSample;

	    struct
		{
	    	rmRmsParams_t params;
	    }setRmsParameters;

	}params;
}rmInquiry_t;


typedef struct
{
	bool configured;
	bool waitingForNewSample;
	bool masterOfAdc;
	bool sendMeasDataOverAux;

	//RMS calculations
	uint32_t windowSize;				/*!< number of samples in RMS calculation window*/
	uint32_t rmsValUpdatePrescaler;
	uint32_t rmsValUpdateCtr;

	firDriver_t firDriver;
	rmsDriver_t rmsDriver;

	float rmsValue;

	//velocity calculation
	bool 	 prevSampSign;
	uint32_t zeroCrossingCtr;

	QueueHandle_t inquiryQueue;
	SemaphoreHandle_t adcInquiryProcessedSemaphore;
	bool adcInquiryResult;

	rmMeasData_t measData;
}rmDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void rmInitRegularMeasurementsDriver(void);
void rmTask(void);

BaseType_t rmStartModule(uint32_t ticksToWait);
BaseType_t rmStopModule(uint32_t ticksToWait);
BaseType_t rmStartSendingMeasData(uint32_t ticksToWait);
BaseType_t rmStopSendingMeasData(uint32_t ticksToWait);
BaseType_t rmSetRmsParameters(rmRmsParams_t* parameters, uint32_t ticksToWait);


#endif /* SERVICES_MEASPROGRAMS_MEASPROGMGR_H_ */
