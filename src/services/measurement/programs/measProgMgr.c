/**
  ******************************************************************************
  * @file      	measProgMgr.c
  * @author    	Michal Kalbarczyk
  * @date      	5-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "systemInit.h"
#include <FreeRTOS.h>
#include <task.h>

#include "measProgMgr.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
/* ----------------------- Private Variables ------------------------------*/
/* ----------------------- Private Prototypes -----------------------------*/
/* ----------------------- Public Functions -------------------------------*/

/* ----------------------- Private Functions ------------------------------*/


