/**
  ******************************************************************************
  * @file      	measProgMgr.h
  * @author    	Michal Kalbarczyk
  * @date      	5-Feb-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
*/

#ifndef SERVICES_MEASPROGRAMS_MEASPROGMGR_H_
#define SERVICES_MEASPROGRAMS_MEASPROGMGR_H_


/* ----------------------- Includes ---------------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include <FreeRTOS.h>
#include <queue.h>


/* ----------------------- Public Defines ---------------------------------*/
#define MPM_TASK_SIZE						1024
#define MPM_INQUIRY_QUEUE_SIZE				4


/* ----------------------- Public Type Definitions ------------------------*/

typedef enum
{
	MPM_PROG_NONE,
	MPM_PROG_RMS_SAMPLING,
	MPM_PROG_ZERO_CHECK,
	MPM_PROG_SPAN_CHECK,
	MPM_PROG_CONTAMINATION_CHECK
}mpmProgNames_t;


typedef struct
{
	mpmProgNames_t programName;
	bool (*initProgram)(void);
	bool (*programProcess)(void);
	bool (*deinitProgram)(void);
}mpmProg_t;


typedef enum
{
	MPM_INQ_CHANGE_PROGRAM
}mpmInqTypes_t;


typedef struct
{
	mpmInqTypes_t inqType;

	union
	{
		struct
		{
			uint16_t none;
	    }changeProgram;
	}params;
}mpmInquiry_t;


typedef struct
{
	mpmProgNames_t activeProgramName;

	QueueHandle_t inquiryQueue;
}mpmDriver_t;


/* ----------------------- Public Macros ----------------------------------*/
/* ----------------------- Public Prototypes ------------------------------*/
void mpmInitMeasProgDriver(void);
void mpmTask(void);



#endif /* SERVICES_MEASPROGRAMS_MEASPROGMGR_H_ */
