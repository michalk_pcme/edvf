/**
  ******************************************************************************
  * @file      	timestamps.c
  * @author    	Michal Kalbarczyk
  * @date      	11-Jan-2018
  * @copyright
  *
  *		PCME PROPRIETARY AND CONFIDENTIAL
  *     SOFTWARE FILE/MODULE HEADER
  *     PCME (c) 2016
  *     Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
  *     All Rights Reserved
  *
  * @details
  * 	Build around TIM2 peripheral. TIM2 gets reset with module
  * 	initialisation which can be triggered from any point in the system.
  * 	Time stamps start getting collected by triggering timestampCollectionStart()
  * 	function. From that point onwards time stamps are getting collected
  * 	with each collectTimestamp() function until collection buffer get
  * 	full. On each collection time stamp name and TIM2 counter readings
  * 	are being taken.
  *
  *		options:
  * 	DELTA	    - captures TIM2 ticks between time stamps
  * 	NAMES_ONLY - captures only names of the stamp, without time signature
  * 	USE_FILTER - captures only time stamps with names within
  * 				  FILTER_LOW - FILTER_HIGH range
*/


/* ----------------------- Includes ---------------------------------------*/
#include "timestamps.h"
#include <systemDefinitions.h>
#include "stm32l4xx_hal.h"


/* ----------------------- Private Defines --------------------------------*/
/* ----------------------- Private Type Definitions -----------------------*/
typedef struct{
	unsigned long stamp;
	unsigned int name;
} Stamp_stc;


/* ----------------------- Private Variables ------------------------------*/
static TIM_HandleTypeDef htim2;

#ifdef STAMPS_ENABLE
#ifndef NAMES_ONLY
	static volatile Stamp_stc timing[TIME_STAMPS_AMOUNT];
#else
	static volatile unsigned long timing[TIME_STAMPS_AMOUNT];
#endif


static volatile int counter  = 0;
static volatile bool collect = false;
static volatile bool col_EN  = false;

static volatile unsigned long Last_stamp = 0;
#endif


/* ----------------------- Private Prototypes -----------------------------*/
static void configureTimer(void);


/* ----------------------- Public Functions -------------------------------*/

/**
  * @brief  This function starts collection of time stamps.
  * @note	Should be called at the execution point where time stamps tracking
  * 		should start.
  * @param  time_name: time stamp name number
  * @retval None
  */
void startCollectingTimestamps(unsigned int time_name){

#ifdef STAMPS_ENABLE

	if (true == col_EN){
#ifndef NAMES_ONLY
		TIM2->CNT = 0;
		timing[0].stamp = Last_stamp = TIM2->CNT;
		timing[0].name = time_name;
#else
		timing[0] = time_name;
#endif
		counter++;
		collect = true;
		col_EN = false;
	}
#else
	(void)(time_name);
#endif
}


/**
  * @brief  This function collects time stamp.
  * @param  time_name: time stamp name number
  * @retval None
  */
void collectTimestamp(unsigned int time_name){
#ifdef STAMPS_ENABLE

#ifdef USE_FILTER
	if(time_name < FILTER_LOW) return;
	if(time_name > FILTER_HIGH) return;
#endif

	if(true == collect){
#ifndef NAMES_ONLY
		timing[counter].stamp = TIM2->CNT;
		timing[counter].name  = time_name;
#ifdef DELTA
		TIM2->CNT = 0;
#endif
#else
		timing[counter] = time_name;
#endif
		counter++;
		if(counter>TIME_STAMPS_AMOUNT-1)
			collect = false;
	}

#else
	(void)(time_name);
#endif
}


/**
  * @brief  Initialises time stamp module.
  * @note	Should be called at the beginning of the system or from serial
  * 		communication command.
  * @param  time_name: time stamp name number
  * @retval None
  */
void initTimestamps(void){
#ifdef STAMPS_ENABLE

	unsigned int i;

	for(i=0;i<TIME_STAMPS_AMOUNT;i++){
#ifndef NAMES_ONLY
		timing[i].name=timing[i].stamp=0;
#else
		timing[i] = 0;
#endif
	}

	counter=0;

	configureTimer();

	HAL_TIM_Base_Start(&htim2);

	col_EN = true;

#endif
}


/* ----------------------- Private Functions ------------------------------*/

/**
  * @brief  Configures module's base hardware timer.
  * @param  None
  * @retval None
  */
static void configureTimer(void){
#ifdef STAMPS_ENABLE
	//The rest of configuration is in HAL_TIM_Base_MspInit().

	  TIM_ClockConfigTypeDef sClockSourceConfig;
	  TIM_MasterConfigTypeDef sMasterConfig;

	  // timer tick [ns] = 1 / (APB1_CLK_period/(prescaler + 1)) = 1 / (80MHz/(0 + 1)) = 12.5ns
	  // timer tick [ns] = 1 / (APB1_CLK_period/(prescaler + 1)) = 1 / (16MHz/(0 + 1)) = 62.5ns
	  // timer tick [ns] = 1 / (APB1_CLK_period/(prescaler + 1)) = 1 / ( 8MHz/(0 + 1)) = 125ns

	  htim2.Instance = TIM2;
	  htim2.Init.Prescaler = 0;
	  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	  htim2.Init.Period = 0xFFFFFFFF;
	  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  HAL_TIM_Base_Init(&htim2);

	  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	  HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig);

	  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	  HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);
#endif
}




